/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.controller;

import org.manager.LiveMatchManager;
import org.manager.LiveMatchUpdateManager;
import org.manager.LogManager;
import org.utility.Constants;

/**
 *
 * @author superx
 */
public class LiveMatchEventController {

    private static final LogManager log = new LogManager();
    
    public static void excuiteLiveMatchEvent() {
        
       log.write( Constants.LOG_LEVEL_INFO, "in  public void excuiteLiveMatchEvent(){" );
        
        LiveMatchUpdateManager liveMatchUpdateManager = new LiveMatchUpdateManager();
        
        // Get livematch updated filename
        String liveMatchEventFileName = liveMatchUpdateManager.getLiveMatchEventXMLFileName();
        
        String fileName = Constants.LIVE_MATCH_XML_PATH +"/"+liveMatchEventFileName;
        log.write( Constants.LOG_LEVEL_INFO, "liveMatchEventFileName->" + fileName);
        
        liveMatchUpdateManager.excuiteLiveMatchEvent(fileName);
        
        log.write( Constants.LOG_LEVEL_INFO, "out  public void excuiteLiveMatchEvent(){" );    
    }
    
}
