/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.manager.FileManager;
import org.manager.LiveMatchManager;
import org.manager.LogManager;
import org.manager.spider.Ball24Manager;
import org.model.CompetitionM;
import org.model.EventM;
import org.model.LiveMatchM;
import org.model.TeamM;
import org.model.db.CompetitionDAO;
import org.model.db.CompetitionImpl;
import org.model.db.LastUpdateDAO;
import org.model.db.LastUpdateImpl;
import org.model.db.LiveMatchDAO;
import org.model.db.LiveMatchImpl;
import org.model.db.TeamDAO;
import org.model.db.TeamImpl;
import org.utility.Constants;

/**
 *
 * @author superx
 */
public class LiveMatchController {

    private static final LogManager log = new LogManager();

    public void excuiteLiveMatch() {

        log.write(Constants.LOG_LEVEL_INFO, "in  public void excuiteLiveMatch(){");

        String strXMLPath = "";
        String filename = "";
        LiveMatchManager liveMatchManager = new LiveMatchManager();
        filename = liveMatchManager.getLiveMatchXMLFileName();
        strXMLPath = Constants.LIVE_MATCH_NEW_ENTRY + "/" + filename;

        log.write(Constants.LOG_LEVEL_INFO, " strXMLPath : " + strXMLPath);

        liveMatchManager.excuiteLMXML2LMData(strXMLPath);
        FileManager source = new FileManager();
        source.setPath(Constants.LIVE_MATCH_NEW_ENTRY);
        source.setFilename(filename);
        if (FileManager.isFile(source)) {
            FileManager target = new FileManager();
            target.setPath(Constants.LIVE_MATCH_EXCUITED);
            target.setFilename(filename);
            if (FileManager.move(source, target)) {
                log.write(Constants.LOG_LEVEL_INFO, " Move : " + source.getFullpath() + ">>>" + target.getFullpath());
            } else {
                log.write(Constants.LOG_LEVEL_INFO, "Fail to  move : " + source.getFullpath());
            }
        }

    }

    public void runLiveMatchUpdateInfinity() {

        log.write(Constants.LOG_LEVEL_INFO, "in  public void excuiteLiveMatchUpdated(){");

        LiveMatchManager liveMatchManager = new LiveMatchManager();

        // Get livematch updated filename
        String liveMatchUpdatedFileName = liveMatchManager.getLiveMatchUpdatedXMLFileName();

        String fileName = Constants.LIVE_MATCH_EXCUITED + "/" + liveMatchUpdatedFileName;
        log.write(Constants.LOG_LEVEL_INFO, "liveMatchUpdatedFileName->" + fileName);

        liveMatchManager.excuiteLMUpdatedXML2LMData(fileName);
        LastUpdateDAO lastUpdateDAO = new LastUpdateImpl();
        if (lastUpdateDAO.updateByType("update")) {
            log.write(Constants.LOG_LEVEL_INFO, "update last_update table completed");
        } else {
            log.write(Constants.LOG_LEVEL_INFO, "update last_update table fail");
        }

        log.write(Constants.LOG_LEVEL_INFO, "out  public void excuiteLiveMatchUpdated(){");

        FileManager fileManager = new FileManager();
        fileManager.setPath(Constants.LIVE_MATCH_EXCUITED);
        fileManager.setFilename(liveMatchUpdatedFileName);
        if (fileManager.delete()) {
            log.write(Constants.LOG_LEVEL_INFO, fileName + " has been deleted.");
        }

    }

    public void runLiveYesterday() {

        log.write(Constants.LOG_LEVEL_INFO, "in  public void runLiveYesterday(){");

        LiveMatchManager liveMatchManager = new LiveMatchManager();

        // Get livematch updated filename
        String liveMatchUpdatedFileName = liveMatchManager.getOldestFile(Constants.LIVE_MATCH_YESTERDAY);

        String fileName = Constants.LIVE_MATCH_YESTERDAY + "/" + liveMatchUpdatedFileName;
        log.write(Constants.LOG_LEVEL_INFO, "Oldest file->" + fileName);

        liveMatchManager.excuiteYesterdayLMXML2LMData(fileName);
//        LastUpdateDAO lastUpdateDAO = new LastUpdateImpl();
//        if (lastUpdateDAO.updateByType("update")) {
//            log.write(Constants.LOG_LEVEL_INFO, "update last_update table completed");
//        } else {
//            log.write(Constants.LOG_LEVEL_INFO, "update last_update table fail");
//        }

        log.write(Constants.LOG_LEVEL_INFO, "out  public void runLiveYesterday(){");

        FileManager fileManager = new FileManager();
        fileManager.setPath(Constants.LIVE_MATCH_YESTERDAY);
        fileManager.setFilename(liveMatchUpdatedFileName);
        if (fileManager.delete()) {
            log.write(Constants.LOG_LEVEL_INFO, fileName + " has been deleted.");
        }

    }

    public void liveMatchEventInfinity() {

        log.write(Constants.LOG_LEVEL_INFO, "in  public void liveMatchEventInfinity() {");

        LiveMatchManager liveMatchManager = new LiveMatchManager();

        // Get livematch updated filename
//        String liveMatchUpdatedFileName = liveMatchManager.getLiveMatchUpdatedXMLFileName();
//
//        String fileName = Constants.LIVE_MATCH_NEW_ENTRY + "/" + liveMatchUpdatedFileName;
//        log.write(Constants.LOG_LEVEL_INFO, "liveMatchUpdatedFileName->" + fileName);
        //liveMatchManager.excuiteLMUpdatedXML2LMData(fileName);
        FileManager fileManager = new FileManager();
        fileManager.writeFileLiveMatchOnly();
        LastUpdateDAO lastUpdateDAO = new LastUpdateImpl();
        if (lastUpdateDAO.updateByType("event")) {
            log.write(Constants.LOG_LEVEL_INFO, "update last_update table completed");
        } else {
            log.write(Constants.LOG_LEVEL_INFO, "update last_update table fail");
        }

        log.write(Constants.LOG_LEVEL_INFO, "out  public void liveMatchEventInfinity() {");

    }

    public void liveMatchEventInfinity2() {

        log.write(Constants.LOG_LEVEL_INFO, "in  public void liveMatchEventInfinity2() {");

        Vector<LiveMatchM> liveMatchMvt = new Vector();
        LiveMatchDAO liveMatchDAO = new LiveMatchImpl();
        FileManager fileManager = new FileManager();
        Ball24Manager ball24man = new Ball24Manager();
        Vector<EventM> eventMvt = new Vector();

        liveMatchMvt = liveMatchDAO.getLiveMatchWithout11();
        for (LiveMatchM liveMatchM : liveMatchMvt) {
            ball24man.getLiveEventAction(eventMvt, liveMatchM.getMid());
            fileManager.writeLiveEventJson(eventMvt, liveMatchM.getMid(), liveMatchM.getEvent_id());
        }
        //System.out.println(eventMvt.size());

        LastUpdateDAO lastUpdateDAO = new LastUpdateImpl();
        if (lastUpdateDAO.updateByType("event2")) {
            log.write(Constants.LOG_LEVEL_INFO, "update last_update table completed");
        } else {
            log.write(Constants.LOG_LEVEL_INFO, "update last_update table fail");
        }

        log.write(Constants.LOG_LEVEL_INFO, "out  public void liveMatchEventInfinity2() {");

    }

    public void liveMatchLastEvent() {

        log.write(Constants.LOG_LEVEL_INFO, "in  public void liveMatchLastEvent() {");

        Vector<LiveMatchM> liveMatchMvt = new Vector();
        LiveMatchDAO liveMatchDAO = new LiveMatchImpl();
        FileManager fileManager = new FileManager();
        Ball24Manager ball24man = new Ball24Manager();
        Vector<EventM> eventMvt = new Vector();

        liveMatchMvt = liveMatchDAO.getLiveMatchLastEvent();
        for (LiveMatchM liveMatchM : liveMatchMvt) {
            ball24man.getLiveEventAction(eventMvt, liveMatchM.getMid());
            fileManager.writeLiveEventJson(eventMvt, liveMatchM.getMid(), liveMatchM.getEvent_id());
        }
        System.out.println(eventMvt.size());

        LastUpdateDAO lastUpdateDAO = new LastUpdateImpl();
        if (lastUpdateDAO.updateByType("event3")) {
            log.write(Constants.LOG_LEVEL_INFO, "update last_update table completed");
        } else {
            log.write(Constants.LOG_LEVEL_INFO, "update last_update table fail");
        }

        log.write(Constants.LOG_LEVEL_INFO, "out  public void liveMatchLastEvent() {");

    }

    public void liveMatchResultEvent() {

        log.write(Constants.LOG_LEVEL_INFO, "in  public void liveMatchResultEvent() {");

        Vector<LiveMatchM> liveMatchMvt = new Vector();
        LiveMatchDAO liveMatchDAO = new LiveMatchImpl();
        FileManager fileManager = new FileManager();
        Ball24Manager ball24man = new Ball24Manager();
        Vector<EventM> eventMvt = new Vector();

        liveMatchMvt = liveMatchDAO.getLiveMatchResultEvent();
        for (LiveMatchM liveMatchM : liveMatchMvt) {
            ball24man.getLiveEventAction(eventMvt, liveMatchM.getMid());
            fileManager.writeLiveResultEventJson(eventMvt, liveMatchM.getMid());
        }
        System.out.println(eventMvt.size());

//        LastUpdateDAO lastUpdateDAO = new LastUpdateImpl();
//        if (lastUpdateDAO.updateByType("event3")) {
//            log.write(Constants.LOG_LEVEL_INFO, "update last_update table completed");
//        } else {
//            log.write(Constants.LOG_LEVEL_INFO, "update last_update table fail");
//        }
        log.write(Constants.LOG_LEVEL_INFO, "out  public void liveMatchResultEvent() {");

    }

    public void runLiveVS() {
        LiveMatchDAO liveMatchDAO = new LiveMatchImpl();
        TeamDAO teamDAO = new TeamImpl();
        CompetitionDAO competitionDAO = new CompetitionImpl();
        Vector<LiveMatchM> liveMatchVtr = new Vector<LiveMatchM>();
        liveMatchVtr = liveMatchDAO.retrieveByLiveMatchChecked();
        TeamM home = new TeamM();
        TeamM guest = new TeamM();
        TeamController teamController = new TeamController();
        ResultController resultCon = new ResultController();
        CompetitionM competition = new CompetitionM();

        for (LiveMatchM liveMatch : liveMatchVtr) {
            boolean matchAllSuccess = true;

            home = new TeamM();
            guest = new TeamM();
            home = teamDAO.getBytid(liveMatch.getHid());
            guest = teamDAO.getBytid(liveMatch.getGid());
            competition = competitionDAO.get(liveMatch.getKid());
            if (0 == competition.getCompetitionId()) {
                competitionDAO.getCompetitionById("" + liveMatch.getKid());
                competition = competitionDAO.get(liveMatch.getKid());
            }
            System.out.println(liveMatch.getMid() + ":" + home.getTn() + "-" + guest.getTn());
            ///liveMatchDAO.getInfo(1373815, 0);

            if (0 == home.getTid()) {
                String info[] = liveMatchDAO.getInfo(liveMatch.getMid(), 0);
                if (info[0] != null) {
//                    TeamM t = new TeamM();
//                    //Vector<Team> vt = new Vector<Team>();
//                    CompetitionM com = new CompetitionM();
//                    String cnPk = competitionDAO.getCnPk(info[0]);
//                    com = competitionDAO.getComByPk(cnPk);
//
//                    t.setCid(com.getCompetitionId());
//                    t.setComPk(info[0]);
//                    t.setComType(com.getCompetitionType());
//                    t.setTid(liveMatch.getHid());
//                    t.setTn(liveMatch.getHn());
//                    t.setTnPk(info[1]);
//                    t.setTnTh(liveMatch.getHn());
//                    //vt.add(t);
//                    boolean result = teamDAO.addSingle(t);
//                    //h = tDb.get(l.getHid());
//                    home = teamDAO.getBytid(liveMatch.getHid());
//                    System.out.println("add new team " + liveMatch.getHn() + " " + result);  
                    home = teamController.addTeam(liveMatch, info);

                } else {
                    matchAllSuccess = true;
                }

            }

            if (home.getTnPk() != null && home.getComPk() != null) {
                //if (teamController.getTeamDetailV2(home) == false) {
                if (false == teamController.getDetail(home.getTid())) {
                    //matchAllSuccess = false;
                    System.out.println("[H]Get Team Detail Unsuccess");
                    System.out.println("Team Home Not Found !!!!!");
                    //String info[] = tDb.getInfo(l.getMid(), 1, l.getAo());
                    String info[] = liveMatchDAO.getInfo(liveMatch.getMid(), 0);
                    if (info[0] != null) {
                        home = teamController.addTeam(liveMatch, info);
                    } else {
                        matchAllSuccess = false;
                    }

                } else {

                }

            } else {
                System.out.println("[H] Team Home Not Found !!!!!");
                String info[] = liveMatchDAO.getInfo(liveMatch.getMid(), 0);
                if (info[0] != null) {
                    home = teamController.addTeam(liveMatch, info);
                } else {

                }
                matchAllSuccess = false;

            }

            if (guest.getTid() == 0) {
                System.out.println("[G] Team Away Not Found !!!!!");

                //String info[] = tDb.getInfo(l.getMid(), 2, l.getAo());
                String info[] = liveMatchDAO.getInfo(liveMatch.getMid(), 1);
                if (info[0] != null) {
                    guest = teamController.addTeam(liveMatch, info);
                } else {
                    matchAllSuccess = false;
                    System.out.println("GET  Team dataa not success");
                }

            } else {

            }

            if (guest.getTnPk() != null && guest.getComPk() != null) {
                //if (tc.getTeamDetailV2(g) == false) {
                if (false == teamController.getDetail(guest.getTid())) {
                    String info[] = liveMatchDAO.getInfo(liveMatch.getMid(), 1);
                    if (info[0] != null) {
                        guest = teamController.addTeam(liveMatch, info);
                    } else {

                    }
                    matchAllSuccess = false;

                } else {

                }

            } else {
                System.out.println("[G] Team away not found !!!!");
                String info[] = liveMatchDAO.getInfo(liveMatch.getMid(), 1);
                if (info[0] != null) {
                    guest = teamController.addTeam(liveMatch, info);
                } else {

                }
                matchAllSuccess = false;
            }

            if (matchAllSuccess) {
                System.out.println("State 1st Success");
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
                String path = "spiderbotdoc/compare/" + ft.format(dNow).toString();
                FileManager fileman = new FileManager();
                fileman.setPath(path);
                String filename = home.getComPk() + "_" + home.getTnPk() + "-" + guest.getComPk() + "_" + guest.getTnPk() + ".txt";
                fileman.setFilename(filename);
                if (FileManager.isFile(fileman)) {

                } else {
                    matchAllSuccess = false;
                    System.out.println("State 2 Unsuccess");

                }

                if (matchAllSuccess) {
                    System.out.println("State 2 Success");
                    if (resultCon.haveResultsCompare(home) == false || resultCon.haveResultsCompare(guest) == false) {
                        matchAllSuccess = false;
                        System.out.println("State 3 Unsuccess");
                    } else {
                        System.out.println("State 3 Success");
                    }

                }

            } else {
                System.out.println("State 1 Unsuccess");
            }
            //System.out.println("x35");
            //count++;

            if (matchAllSuccess) {
//                LiveLeague ll =llDb.getLiveLeague(l.getLid()+"");
//                System.out.println("leagueId:"+ll.getLeagueId());
//                boolean check=lcon.updateV2(ll);

                //tDb.writeFile(h, g, l.getMid());
                teamDAO.writeFile(home, guest, liveMatch.getMid());
                //System.out.println("Match All Success !!!");
            }

        }

    }
    
}
