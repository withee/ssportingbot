/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.controller;

import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Tingly
 */
public class RE {

    public static Vector<String> getRegularExpression(String rexp, String str) {
        Pattern p = Pattern.compile(rexp);
        Matcher m = p.matcher(str);
        Vector<String> vs = new Vector<String>();
        while (m.find()) {
            vs.addElement(m.group());

        }
        return vs;

    }

    public static Vector<String> getRegularExpressionDev(String rexp, String str) {
        Pattern p = Pattern.compile(rexp);
        Matcher m = p.matcher(str);
        Vector<String> vs = new Vector<String>();
        while (m.find()) {
            vs.addElement(m.group());
            System.out.println(m.group());

        }
        return vs;

    }

    public static String getRE(String rexp, String str) {
        Pattern p = Pattern.compile(rexp);
        Matcher m = p.matcher(str);
        String value = "";
        while (m.find()) {
            value = (m.group());

        }
        return value;

    }

    public static String getLastRegularExpression(String rexp, String str) {
        Pattern p = Pattern.compile(rexp);
        Matcher m = p.matcher(str);
        String value = "";
        while (m.find()) {
            value = m.group();

        }
        return value;

    }

    public static String getFirstRegularExpression(String rexp, String str) {
        Pattern p = Pattern.compile(rexp);
        Matcher m = p.matcher(str);
        String value = "";
        while (m.find()) {
            value = m.group();
            break;

        }
        return value;

    }
}
