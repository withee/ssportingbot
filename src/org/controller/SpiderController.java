/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import org.manager.FileManager;
import org.manager.LogManager;
import org.manager.spider.Ball24Manager;
import org.model.LiveLeagueM;
import org.model.LiveMatchM;
import org.model.RequestHeaderM;
import org.model.ResponseHeaderM;
import org.model.TeamM;
import org.model.db.LiveLeagueDAO;
import org.model.db.LiveLeagueImpl;
import org.model.db.LiveMatchDAO;
import org.model.db.LiveMatchImpl;
import org.model.db.SubLeagueDAO;
import org.model.db.SubLeagueImpl;
import org.model.db.SubLeagueM;
import org.model.db.TeamDAO;
import org.model.db.TeamImpl;

/**
 *
 * @author superx
 */
public class SpiderController {

    private static final LogManager log = new LogManager();

    public void getLeagueStatisticContent() {

        FileManager fileman = new FileManager();
        LiveLeagueDAO liveLeagueDAO = new LiveLeagueImpl();
        SubLeagueDAO subDB = new SubLeagueImpl();
        Vector<LiveLeagueM> liveLeagueVt = new Vector<LiveLeagueM>();
        RequestHeaderM req = new RequestHeaderM();
        ResponseHeaderM res;
        Ball24Manager ball24manager = new Ball24Manager();
        //LiveLeagueM liveLeage = new LiveLeagueM();
        liveLeagueVt = liveLeagueDAO.retrieveByChecked();
        String array[] = {null, "h", "g"};
        String urlString = "";
        String urlStringO = "";

        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        String path = "spiderbotdoc/league/statistic/" + ft.format(dNow).toString();
        String filename = "";
        fileman.setPath(path);
        for (LiveLeagueM l : liveLeagueVt) {
            int page = 0;
            res = new ResponseHeaderM();

            if ((l.getLnPk().equals("Serie-A") || l.getLnPk().equals("Serie-B") || l.getLnPk().equals("Serie-C") || l.getLnPk().equals("Copa-do-Brasil-Sub-20")) && l.getCnPk().equals("Brasil")) {
                l.setCnPk("Brazil");
            }

            //req.setUrl("http://www.futbol24.com/" + l.getCt() + "/" + l.getCnPk() + "/" + l.getLnPk() + "/" + l.getLs() + "/fixtures/?Ajax=1&statLF-Page=" + page);
            //System.out.println(req.getUrl());
            for (int k = 0; k < array.length; k++) {
                urlStringO = "http://www.futbol24.com/" + l.getCt() + "/" + l.getCnPk() + "/" + l.getLnPk() + "/" + l.getLs();
                req.setReferer(urlStringO);
                urlString = urlStringO + "/?Ajax=1&statT-Limit=0&statT-Table=" + k;
                req.setUrl(urlString);

                filename = l.getCt() + "_" + l.getCnPk() + "_" + l.getLnPk() + "_" + l.getLs() + "_" + k + ".txt";
                fileman.setFilename(filename);
                if (!FileManager.isFile(fileman)) {
                    res = ball24manager.getContent(req);
                    fileman.setContent(res.getContent());
                    fileman.write();
                } else {
                    System.out.println(fileman.getFullpath() + " is exist!!");
                }

                SubLeagueM sl = subDB.getBySubleagueName(l.getSubleagueName());

                if (l.getSubLeagueNamePk() != null) {
                    String urlString1 = urlStringO + "/" + sl.getSubLeagueNamePk() + "/?Ajax=1&statT-Limit=0&statT-Table=" + k;
                    req.setUrl(urlString1);
                    req.setReferer(urlStringO + "/" + sl.getSubLeagueNamePk());
                    filename = l.getCt() + "_" + l.getCnPk() + "_" + l.getLnPk() + "_" + l.getLs() + "_" + sl.getSubLeagueNamePk() + "_" + k + ".txt";
                    fileman.setFilename(filename);
                    if (!FileManager.isFile(fileman)) {
                        res = ball24manager.getContent(req);
                        fileman.setContent(res.getContent());
                        fileman.write();
                    } else {
                        System.out.println(fileman.getFullpath() + " is exist!!");
                    }
                }
            }

        }

    }

    public void getLeagueFixtureContent() {

        FileManager fileman = new FileManager();
        LiveLeagueDAO liveLeagueDAO = new LiveLeagueImpl();
        Vector<LiveLeagueM> liveLeagueVt = new Vector<LiveLeagueM>();
        RequestHeaderM req = new RequestHeaderM();
        ResponseHeaderM res;
        Ball24Manager ball24manager = new Ball24Manager();
        //LiveLeagueM liveLeage = new LiveLeagueM();
        liveLeagueVt = liveLeagueDAO.retrieveByChecked();

        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        String path = "spiderbotdoc/league/fixture/" + ft.format(dNow).toString();
        String filename = "";
        fileman.setPath(path);
        for (LiveLeagueM l : liveLeagueVt) {
            int page = 0;
            res = new ResponseHeaderM();

            if ((l.getLnPk().equals("Serie-A") || l.getLnPk().equals("Serie-B") || l.getLnPk().equals("Serie-C") || l.getLnPk().equals("Copa-do-Brasil-Sub-20")) && l.getCnPk().equals("Brasil")) {
                l.setCnPk("Brazil");
            }
            req.setUrl("http://www.futbol24.com/" + l.getCt() + "/" + l.getCnPk() + "/" + l.getLnPk() + "/" + l.getLs() + "/fixtures/?Ajax=1&statLF-Page=" + page);
            System.out.println(req.getUrl());

            filename = l.getCt() + "_" + l.getCnPk() + "_" + l.getLnPk() + "_" + l.getLs() + ".txt";
            fileman.setFilename(filename);
            if (!FileManager.isFile(fileman)) {
                res = ball24manager.getContent(req);
                fileman.setContent(res.getContent());
                fileman.write();
            } else {
                System.out.println(fileman.getFullpath() + " is exist!!");
            }

        }

    }

    public void getLeagueResultContent() {

        FileManager fileman = new FileManager();
        LiveLeagueDAO liveLeagueDAO = new LiveLeagueImpl();
        Vector<LiveLeagueM> liveLeagueVt = new Vector<LiveLeagueM>();
        RequestHeaderM req = new RequestHeaderM();
        ResponseHeaderM res;
        Ball24Manager ball24manager = new Ball24Manager();
        //LiveLeagueM liveLeage = new LiveLeagueM();
        liveLeagueVt = liveLeagueDAO.retrieveByChecked();

        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        String path = "spiderbotdoc/league/results/" + ft.format(dNow).toString();
        String filename = "";
        fileman.setPath(path);
        for (LiveLeagueM l : liveLeagueVt) {
            int page = 0;
            res = new ResponseHeaderM();

            if ((l.getLnPk().equals("Serie-A") || l.getLnPk().equals("Serie-B") || l.getLnPk().equals("Serie-C") || l.getLnPk().equals("Copa-do-Brasil-Sub-20")) && l.getCnPk().equals("Brasil")) {
                l.setCnPk("Brazil");
            }
            req.setUrl("http://www.futbol24.com/" + l.getCt() + "/" + l.getCnPk() + "/" + l.getLnPk() + "/" + l.getLs() + "/results/?Ajax=1&statLR-Page=" + page);
            System.out.println(req.getUrl());

            filename = l.getCt() + "_" + l.getCnPk() + "_" + l.getLnPk() + "_" + l.getLs() + ".txt";
            fileman.setFilename(filename);
            if (!FileManager.isFile(fileman)) {
                res = ball24manager.getContent(req);
                fileman.setContent(res.getContent());
                fileman.write();
            } else {
                System.out.println(fileman.getFullpath() + " is exist!!");
            }

        }

    }

    public void getTeamResult() {
        //WebPageManager webbot = new WebPageManager();
        //webbot.getLiveMatchXML();        
        LiveMatchDAO liveMatchDAO = new LiveMatchImpl();
        TeamController teamcon = new TeamController();
        ResultController resultCon = new ResultController();
        TeamDAO teamDAO = new TeamImpl();
        Iterable<LiveMatchM> liveMatchMvt = liveMatchDAO.getLiveToday();
        FileManager fileman = new FileManager();

        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        String path = "spiderbotdoc/team_result/" + ft.format(dNow).toString();
        String filename;
        fileman.setPath(path);
        for (LiveMatchM liveMatch : liveMatchMvt) {
            TeamM home = teamDAO.getBytid(liveMatch.getHid());
            TeamM guest = teamDAO.getBytid(liveMatch.getGid());

            filename = home.getComPk() + "_" + home.getTnPk() + ".txt";
            fileman.setFilename(filename);
            if (home.getTid() != 0) {
                if (!FileManager.isFile(fileman)) {
                    if (resultCon.getResultsCompareV2(home.getComPk(), home.getTnPk())) {
                        fileman.setContent("completed");
                        fileman.write();
                    }
                } else {
                    System.out.println("already get " + filename);
                }
            }

            filename = guest.getComPk() + "_" + guest.getTnPk() + ".txt";
            fileman.setFilename(filename);
            if (guest.getTid() != 0) {
                if (!FileManager.isFile(fileman)) {
                    if (resultCon.getResultsCompareV2(guest.getComPk(), guest.getTnPk())) {
                        fileman.setContent("completed");
                        fileman.write();
                    }
                } else {
                    System.out.println("already get " + filename);
                }
            }
        }

    }

    public void getTeamCompare() {
        //WebPageManager webbot = new WebPageManager();
        //webbot.getLiveMatchXML();        
        LiveMatchDAO liveMatchDAO = new LiveMatchImpl();
        TeamController teamcon = new TeamController();
        TeamDAO teamDAO = new TeamImpl();
        Iterable<LiveMatchM> liveMatchMvt = liveMatchDAO.getLiveToday();
        FileManager fileman = new FileManager();

        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        String path = "spiderbotdoc/compare/" + ft.format(dNow).toString();
        fileman.setPath(path);
        for (LiveMatchM liveMatch : liveMatchMvt) {
            TeamM home = teamDAO.getBytid(liveMatch.getHid());
            TeamM guest = teamDAO.getBytid(liveMatch.getGid());
            String filename = home.getComPk() + "_" + home.getTnPk() + "-" + guest.getComPk() + "_" + guest.getTnPk() + ".txt";
            fileman.setFilename(filename);
            if (home.getTid() != 0 && guest.getTid() != 0) {
                if (!FileManager.isFile(fileman)) {
                    if (teamcon.getCompare(home, guest)) {
                        fileman.setContent("completed");
                        fileman.write();
                    }
                } else {
                    System.out.println("already get " + filename);
                }
            }
        }

    }

    public void getTeamFixture() {
        //WebPageManager webbot = new WebPageManager();
        //webbot.getLiveMatchXML();        
        LiveMatchDAO liveMatchDAO = new LiveMatchImpl();
        TeamController teamcon = new TeamController();
        TeamDAO teamDAO = new TeamImpl();
        Iterable<LiveMatchM> liveMatchMvt = liveMatchDAO.getLiveToday();
        FileManager fileman = new FileManager();

        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        String path = "spiderbotdoc/fixture/" + ft.format(dNow).toString();
        fileman.setPath(path);
        for (LiveMatchM liveMatch : liveMatchMvt) {
            fileman.setFilename(liveMatch.getHid() + ".json");
            TeamM home = teamDAO.getBytid(liveMatch.getHid());
            if (!FileManager.isFile(fileman)) {
                boolean success = teamcon.getTeamDetailV2(home, false);
                if (success) {
//                    fileman.setContent("completed");
//                    fileman.write();
                }
            } else {
                boolean success = teamcon.getTeamDetailV2(home, true);

                //System.out.println("already update team " + liveMatch.getHn());
            }
            fileman.setFilename(liveMatch.getGid() + ".json");
            TeamM guest = teamDAO.getBytid(liveMatch.getGid());
            if (!FileManager.isFile(fileman)) {
                boolean success = teamcon.getTeamDetailV2(guest, false);
                if (success) {
//                    fileman.setContent("completed");
//                    fileman.write();
                }
            } else {
                boolean success = teamcon.getTeamDetailV2(guest, true);
                //System.out.println("already update team " + liveMatch.getGn());
            }
        }

    }

    public void getTeamPk() {
        //WebPageManager webbot = new WebPageManager();
        //webbot.getLiveMatchXML();

        Ball24Manager ball24manager = new Ball24Manager();
        LiveMatchDAO liveMatchDAO = new LiveMatchImpl();
        Iterable<LiveMatchM> liveMatchMvt = liveMatchDAO.getLiveToday();
        for (LiveMatchM liveMatch : liveMatchMvt) {
            ball24manager.writeInfo(liveMatch.getMid(), 1, liveMatch.getAo());
        }

    }

    public void getLiveMatch() {
        //WebPageManager webbot = new WebPageManager();
        //webbot.getLiveMatchXML();
        Ball24Manager ball24manager = new Ball24Manager();
        ball24manager.getLiveMatchXML();

    }

    public void getLiveMatchYesterday() {
        //WebPageManager webbot = new WebPageManager();
        //webbot.getLiveMatchXML();
        Ball24Manager ball24manager = new Ball24Manager();
        ball24manager.getLiveXMLDate();

    }

    public void getLiveEvent() {
        //WebPageManager webbot = new WebPageManager();
        //webbot.getLiveMatchXML();
        String sql = "select event.event_id,event.mid,ao from  live_match left join event  on event.mid = live_match.mid  where data_checked='N' and showDate =current_date and sid!=11";

        Ball24Manager ball24manager = new Ball24Manager();
        LiveMatchDAO liveMatchDAO = new LiveMatchImpl();
        Vector<LiveMatchM> liveMatchMvt = new Vector();
        LiveMatchM liveMatchM = new LiveMatchM();
        liveMatchMvt = liveMatchDAO.getLiveMatchWithout11();
        for (LiveMatchM liveMatch : liveMatchMvt) {
            ball24manager.getLiveEventXML(liveMatch.getMid());
        }

    }

    public void getCompetition() {
        Ball24Manager ball24manager = new Ball24Manager();
        ball24manager.getCompetitions();

    }

}
