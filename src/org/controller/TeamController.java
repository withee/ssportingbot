/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.controller;

import static java.lang.System.exit;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.manager.FileManager;
import org.manager.LogManager;
import org.manager.RegularExpression;
import org.model.CompetitionM;
import org.model.FixtureM;
import org.model.LeagueM;
import org.model.LiveLeagueM;
import org.model.LiveMatchM;
import org.model.RequestHeaderM;
import org.model.ResponseHeaderM;
import org.model.ResultVsM;
import org.model.StatTableM;
import org.model.TeamM;
import org.model.db.CompetitionDAO;
import org.model.db.CompetitionImpl;
import org.model.db.FixtureDAO;
import org.model.db.FixtureImpl;
import org.model.db.LiveMatchDAO;
import org.model.db.LiveMatchImpl;
import org.model.db.ResultDAO;
import org.model.db.ResultImpl;
import org.model.db.StatTableDAO;
import org.model.db.StatTableImpl;
import org.model.db.SubLeagueDAO;
import org.model.db.SubLeagueImpl;
import org.model.db.SubLeagueM;
import org.model.db.TeamDAO;
import org.model.db.TeamImpl;

/**
 *
 * @author mrsyrop
 */
public class TeamController {

    private LogManager log = new LogManager();

    public TeamM addTeam(LiveMatchM liveMatch, String info[]) {
        TeamDAO teamDAO = new TeamImpl();
        CompetitionDAO competitionDAO = new CompetitionImpl();
        TeamM team = new TeamM();
        boolean result;
        TeamM t = new TeamM();
        //Vector<Team> vt = new Vector<Team>();
        CompetitionM com = new CompetitionM();

        String cnPk = competitionDAO.getCnPk(info[0]);
        com = competitionDAO.getComByPk(cnPk);

        t.setCid(com.getCompetitionId());
        t.setComPk(info[0]);
        t.setComType(com.getCompetitionType());
        t.setTid(liveMatch.getHid());
        t.setTn(liveMatch.getHn());
        t.setTnPk(info[1]);
        t.setTnTh(liveMatch.getHn());
        //vt.add(t);
        result = teamDAO.addSingle(t);
        //h = tDb.get(l.getHid());
        team = teamDAO.getBytid(liveMatch.getHid());
        System.out.println("add new team " + liveMatch.getHn() + " " + result);

        return team;
    }

    public boolean getTeamDetailV2(TeamM t, boolean haveContent) {
        boolean returnValue = false;

        if (!haveContent) {
            try {
                String urlString = "http://www.futbol24.com/team/" + t.getComPk().replaceAll("\\s", "-") + "/" + t.getTnPk();
                System.out.println(urlString);

//            JSONObject json = RegularExpression.getDataFromURL(urlString);
//            Vector<String> vs = new Vector<String>();
//            String value = json.get("status") + "";
//            if (value.equals("true")) {
//
//                String str = json.getString("data");
//                String rexp = "";
//               returnValue = getFixturesV2(t.getComPk(), t.getTnPk());
//            } else {
//                System.out.println("Url team not found :" + urlString);
//            }
                URL url = new URL(urlString);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();
                int code = connection.getResponseCode();
                //System.out.println(code);
                if (HttpURLConnection.HTTP_OK == code) {
                    returnValue = getFixturesV2content(t.getComPk(), t.getTnPk(), t.getTid());
                    //returnValue = getFixturesV2(t.getComPk(), t.getTnPk());
                } else {
                    System.out.println("Response code:" + code);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                //ErrorLog.writeLog(ex.getStackTrace(), ex.toString());
            }
        } else {
            returnValue = getFixturesV2(t.getComPk(), t.getTnPk(), t.getTid());
        }
        return returnValue;
    }

    public boolean getFixturesV2(String cPk, String tnPk, int tid) {
        boolean returnValue = false;
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        FileManager fileman = new FileManager();
        try {
            //TeamDb tDb = new TeamDb();
            //FixtureDb fixDb = new FixtureDb();
            FixtureDAO fixDAO = new FixtureImpl();
//            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//            String urlString = "http://www.futbol24.com/team/" + cPk + "/" + tnPk + "/fixtures/";
//            System.out.println(urlString);
//            JSONObject json = RegularExpression.getDataFromURL(urlString);

            String path = "spiderbotdoc/fixture/" + ft.format(dNow).toString();
            fileman.setPath(path);
            fileman.setFilename(tid + ".json");

            if (FileManager.isFile(fileman)) {
                log.write("info", "Read team fixture from :" + fileman.getFullpath());
                org.json.JSONObject json = new org.json.JSONObject(fileman.read());
                Vector<String> vs = new Vector<String>();
                String value = json.get("status") + "";
                if (value.equals("true")) {
                    returnValue = true;
                    String str = json.getString("data");
                    String rexp = "<div\\sid=\"statTF\">";
                    vs = RegularExpression.getRegularExpression(rexp, str);
                    if (vs.size() > 0) {
                        //System.out.println("Result VS");
                        rexp = "data-format=\"d\\.m\\.Y\">[^<]+";
                        vs = RegularExpression.getRegularExpression(rexp, str);
                        FixtureM[] flist = new FixtureM[vs.size()];
                        for (int i = 0; i < flist.length; i++) {
                            flist[i] = new FixtureM();
                            Date d = new Date();

                            String splitDate[] = vs.get(i).split(">");
                            String array[] = splitDate[1].split("\\.");
                            String date = array[2] + "-" + array[1] + "-" + array[0];
                            //data-format="d.m.Y">25.11.2012
                            flist[i].setDate(date);

                        }
                        //class="cat dymek" title=" Women Euro U19 Qual. 2012/2013"> WU19Q</a
                        rexp = "class=\"cat[^<]+";
                        vs = RegularExpression.getRegularExpression(rexp, str);
                        for (int i = 0; i < flist.length; i++) {
                            String splitValue[] = vs.get(i).split(">");
                            String lnk = splitValue[1].trim();
                            String splitValue2[] = splitValue[0].split("\"");
                            String ln = splitValue2[3].trim();
                            flist[i].setLnk(lnk);
                            flist[i].setLeagueName(ln);
                        }
                        rexp = "<td\\sclass=\"comp\"><[^<]+";
                        vs = RegularExpression.getRegularExpression(rexp, str);
                        for (int i = 0; i < flist.length; i++) {

                            String splitValue[] = vs.get(i).split(">");
                            String name = splitValue[2].replaceAll(" ", "");
                            String split[] = splitValue[1].split("/");
                            String comType = split[1];
                            String comPk = split[2];
                            String lnPk = split[3];
                            String leagueSeason = split[4];
                            String subLeagueNamePk = "";
                            if (!split[5].contains("\"")) {
                                subLeagueNamePk = split[5];
                            }
                            String subLeagueName = "";

                            //System.out.println("name="+name+",comType="+comType+",comPk="+comPk+",lnPk="+lnPk+",leagueSeason="+leagueSeason+",subLeagueNamePk="+subLeagueNamePk);
                            flist[i].setCompetitionNamePk(comPk);
                            flist[i].setLeagueNamePk(lnPk);
                            flist[i].setCompetitionType(comType);
                            flist[i].setLeagueSeasson(leagueSeason);
                            flist[i].setSubLeagueNamePk(subLeagueNamePk);
                            flist[i].setSubLeagueName("");
                        }
                        rexp = "<td\\sclass=\"team4\"><[^<]+";
                        vs = RegularExpression.getRegularExpression(rexp, str);
                        for (int i = 0; i < flist.length; i++) {
                            String splitValue[] = vs.get(i).split(">");
                            String hn = splitValue[2];
                            String split[] = splitValue[1].split("/");
                            String hcomPk = split[2];
                            String hnPk = split[3];
                            flist[i].setHn(hn);
                            flist[i].setTeamHomeNamePk(hnPk);
                            flist[i].setHcomPk(hcomPk);
                        }
                        rexp = "<td\\sclass=\"team5\"><[^<]+";
                        vs = RegularExpression.getRegularExpression(rexp, str);
                        for (int i = 0; i < flist.length; i++) {
                            String splitValue[] = vs.get(i).split(">");
                            String an = splitValue[2];
                            String split[] = splitValue[1].split("/");
                            String anPk = split[3];
                            String acomPk = split[2];
                            flist[i].setAn(an);
                            flist[i].setTeamAwayNamePk(anPk);
                            flist[i].setAcomPk(acomPk);

                        }
                        fixDAO.add(flist, tnPk);
                    }

                }
            }
        } catch (Exception ex) {
            fileman.delete();
            ex.printStackTrace();
            //ErrorLog.writeLog(ex.getStackTrace(), ex.toString());
        }
        return returnValue;
    }

    public boolean getFixturesV2content(String cPk, String tnPk, int tid) {
        boolean returnValue = false;
        try {
            FixtureDAO fixDAO = new FixtureImpl();
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
            FileManager fileman = new FileManager();

            String path = "spiderbotdoc/fixture/" + ft.format(dNow).toString();
            fileman.setPath(path);

            String urlString = "http://www.futbol24.com/team/" + cPk + "/" + tnPk + "/fixtures/";
            System.out.println(urlString);
            JSONObject json = RegularExpression.getDataFromURL(urlString);
            Vector<String> vs = new Vector<String>();
            String value = json.get("status") + "";
            if (value.equals("true")) {
                fileman.setFilename(tid + ".json");
                fileman.setContent(json.toString());
                fileman.write();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            //ErrorLog.writeLog(ex.getStackTrace(), ex.toString());
        }
        return returnValue;
    }

    public boolean getDetail(int tid) {
        boolean success = false;
        FileManager fileman = new FileManager();
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        String path = "spiderbotdoc/fixture/" + ft.format(dNow).toString();
        fileman.setPath(path);
        fileman.setFilename(tid + ".txt");
        if (FileManager.isFile(fileman)) {
            success = true;
        }
        return success;
    }

    public boolean getCompare(TeamM h, TeamM g) {
        ResultDAO rDb = new ResultImpl();
        boolean success = false;
        JSONObject json;
        int index = 0;
        String rexp = "";
        Vector<String> vs = new Vector<String>();
        while (true) {
            String urlString = "http://www.futbol24.com/teamCompare/" + h.getComPk() + "/" + h.getTnPk() + "/vs/" + g.getComPk() + "/" + g.getTnPk() + "/?Ajax=1&statTLR-Page=" + (index++);
            System.out.println(urlString);
            json = RegularExpression.getDataFromURL(urlString);
            if ((json.getString("status") + "").equals("true")) {

                String str1 = json.getString("data");
                String d3[] = str1.split("<div\\sid=\"statTLR\">");
                if (d3.length == 1) {
                    break;
                } else {
                    String d_2[] = d3[1].split("<div\\sclass=\"half\">");
                    String data = d_2[0];
                    rexp = "data-format=\"d.m.Y\">[^<]+";
                    vs = RegularExpression.getRegularExpression(rexp, data);
                    ResultVsM list[] = new ResultVsM[vs.size()];
                    int i = 0;
                    for (String s : vs) {
                        String split[] = s.split(">");
                        String split1[] = split[1].split("\\.");
                        String date = split1[2] + "-" + split1[1] + "-" + split1[0];
                        list[i] = new ResultVsM();
                        list[i++].setDate(date);
                    }
                    i = 0;
                    rexp = "team4\"><[^<]+";
                    vs = RegularExpression.getRegularExpression(rexp, data);
                    for (String s : vs) {
                        String split[] = s.split("/");
                        String tnPk1 = split[3];
                        list[i++].setTnPk1(tnPk1);
                    }
                    i = 0;
                    rexp = "team5\"><[^<]+";
                    vs = RegularExpression.getRegularExpression(rexp, data);
                    for (String s : vs) {
                        String split[] = s.split("/");
                        String tnPk2 = split[3];
                        list[i++].setTnPk2(tnPk2);
                    }
                    i = 0;
                    rexp = "class=\"cat\\s+dymek\"[^<]+";
                    vs = RegularExpression.getRegularExpression(rexp, data);
                    for (String s : vs) {
                        String split[] = s.split(">");
                        String lnr = split[1];
                        list[i++].setLnr(lnr);
                    }
                    i = 0;
                    rexp = "<a\\shref=\"[^\"]+\"\\sclass=\"cat\\s+dymek";
                    vs = RegularExpression.getRegularExpression(rexp, data);
                    for (String s : vs) {
                        String split[] = s.split("/");
                        String ct = split[1];
                        String cnPk = split[2];
                        String lnPk = split[3];
                        String session = split[4];
                        list[i].setCt(ct);
                        list[i].setCnPk(cnPk);
                        list[i].setLnPk(lnPk);
                        list[i++].setSeason(session);
                    }
                    i = 0;
                    rexp = "<a\\sclass=\"black\\smatchAction\"[^<]+";
                    vs = RegularExpression.getRegularExpression(rexp, data);
                    for (String s : vs) {
                        String split[] = s.split(">");

                        if (split.length < 2) {
                        } else {
                            String score = split[1];
                            String split1[] = split[0].split("/");
                            String matchDate = split1[2] + "/" + split1[3] + "/" + split1[4];
                            String ct = split1[5];
                            String cnPk = split1[6];
                            String lnPk = split1[7];
                            String season = split1[8];
                            String hmn = "";
                            String gmn = "";
                            String sl = "";
                            if (split1.length == 13) {
                                hmn = split1[9];
                                gmn = split1[11];
                            } else if (split1.length == 14) {
                                sl = split1[9];
                                hmn = split1[10];
                                gmn = split1[12];
                            }
                            list[i].setScore(score);
                            list[i].setMatchDate(matchDate);
                            list[i].setCt(ct);
                            list[i].setCnPk(cnPk);
                            list[i].setLnPk(lnPk);
                            list[i].setSeason(season);
                            list[i].setHmn(hmn);
                            list[i].setGmn(gmn);
                            list[i++].setSl(sl);
                        }
                        //System.out.println(list[i].getSl());
                    }
                    //System.out.println(list.length);
                    if (rDb.addResultVs(list, h.getTnPk(), g.getTnPk()) == false) {
//                                        //System.out.println("outer") ;
                        break;
                    } else {
                        success = true;
                    }
                }
            } else {
                //matchAllSuccess = false;
                System.out.println("State 2 Unsuccess");
                break;
            }
            break;
        }

        return success;
    }

    public int getTable(LiveLeagueM l) {
        int status = 0;
        StatTableM statTableList[][] = new StatTableM[3][];
        CompetitionDAO com = new CompetitionImpl();
        Vector<CompetitionM> vc = new Vector<CompetitionM>();
        Vector<LeagueM> vl = new Vector<LeagueM>();
        ///LeagueDb lDb = new LeagueDb();
        StatTableDAO sDb = new StatTableImpl();
        SubLeagueDAO subDb = new SubLeagueImpl();
        String array[] = {null, "h", "g"};
        String urlString = "";
        String urlStringO = "";
        //HttpReqController req = new HttpReqController();
        RequestHeaderM reqh = new RequestHeaderM();
        reqh.setAccept("text/html, */*; q=0.01");
        reqh.setAcceptEncoding("gzip, deflate");
        reqh.setAcceptLanguage("en-us,en;q=0.5");
        reqh.setHost("www.futbol24.com");
        reqh.setXrequestWith("XMLHttpRequest");

        FileManager fileman = new FileManager();
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        String path = "spiderbotdoc/league/statistic/" + ft.format(dNow).toString();
        String filename = "";
        fileman.setPath(path);
        for (int k = 0; k < array.length; k++) {

            try {
//                urlStringO = "http://www.futbol24.com/" + l.getCt() + "/" + l.getCnPk() + "/" + l.getLnPk() + "/" + l.getLs();
//                reqh.setReferer(urlStringO);
//                urlString = urlStringO + "/?Ajax=1&statT-Limit=0&statT-Table=" + k;
//                reqh.setUrl(urlString);
//                System.out.println(urlString);
                ResponseHeaderM response = new ResponseHeaderM();
                filename = l.getCt() + "_" + l.getCnPk() + "_" + l.getLnPk() + "_" + l.getLs() + "_" + k + ".txt";
                fileman.setFilename(filename);
                response.setContent(fileman.read());

                if (response.getStatus() == 1) {
                    status = 1;
                    Vector<String> vs = new Vector<String>();
                    String str = response.getContent();
                    String rexp = "<div\\sid=\"statT\">";
                    vs = RegularExpression.getRegularExpression(rexp, str);
                    //System.out.println("size:"+vs.size());
                    if (vs.size() > 0) {
                        String csplit[] = str.split(rexp);
                        vs = RegularExpression.getRegularExpression("<td\\sclass=\"no\">[^<]*", str);
                        StatTableM st[] = new StatTableM[vs.size()];
                        for (int i = 0; i < vs.size(); i++) {
                            st[i] = new StatTableM();
                            st[i].setCid(l.getCompetitionId());
                            st[i].setLeagueId(l.getLeagueId());
                            st[i].setSubLeagueNamePk("");
                            st[i].setSubLeagueId(l.getLeagueId() + "");

                        }
                        int index = 0;
                        for (String s : vs) {
                            String split[] = s.split(">");
                            if (split.length < 2) {
                                st[index++].setNo(index);
                            } else {
                                st[index++].setNo(Integer.parseInt(split[1]));
                            }
                        }

                        vs = RegularExpression.getRegularExpression("team\\d+", csplit[1]);
                        index = 0;
                        for (String s : vs) {

                            st[index++].setTid(Integer.parseInt(s.replaceAll("team", "")));
                            if (index == st.length) {
                                break;
                            }
                        }
                        vs = RegularExpression.getRegularExpression("<td\\sclass=\"team\"><span\\sclass=\"position\"[^>]*></span>[^>]+|<td\\sclass=\"team\">[^>]+", str);
                        index = 0;
                        for (String s : vs) {
                            s = s.replaceAll("<span\\sclass=\"position\"[^>]*></span>", "");
                            String split[] = s.split("/");
                            st[index++].setTnPk(split[3]);
                        }
                        vs = RegularExpression.getRegularExpression("<td\\sclass=\"gp\">[^<]+", str);
                        index = 0;
                        for (String s : vs) {
                            String split[] = s.split(">");
                            st[index++].setGp(Integer.parseInt(split[1]));
                        }
                        vs = RegularExpression.getRegularExpression("<td\\sclass=\"pts\">[^<]+", str);
                        index = 0;
                        for (String s : vs) {
                            String split[] = s.split(">");
                            st[index++].setPts(Integer.parseInt(split[1]));
                        }
                        vs = RegularExpression.getRegularExpression("<td\\sclass=\"w\">[^<]+", str);
                        index = 0;
                        for (String s : vs) {
                            String split[] = s.split(">");
                            st[index++].setW(Integer.parseInt(split[1]));
                        }
                        vs = RegularExpression.getRegularExpression("<td\\sclass=\"d\">[^<]+", str);
                        index = 0;
                        for (String s : vs) {
                            String split[] = s.split(">");
                            st[index++].setD(Integer.parseInt(split[1]));
                        }
                        vs = RegularExpression.getRegularExpression("<td\\sclass=\"l\">[^<]+", str);
                        index = 0;
                        for (String s : vs) {
                            String split[] = s.split(">");
                            st[index++].setL(Integer.parseInt(split[1]));
                        }
                        vs = RegularExpression.getRegularExpression("<td\\sclass=\"gf\">[^<]+", str);
                        index = 0;
                        for (String s : vs) {
                            String split[] = s.split(">");
                            st[index++].setGf(Integer.parseInt(split[1]));
                        }
                        vs = RegularExpression.getRegularExpression("<td\\sclass=\"ga\">[^<]+", str);
                        index = 0;
                        for (String s : vs) {
                            String split[] = s.split(">");
                            st[index++].setGa(Integer.parseInt(split[1]));

                        }
                        vs = RegularExpression.getRegularExpression("<td\\sclass=\"plusminus\">[^<]+", str);
                        index = 0;
                        for (String s : vs) {
                            String split[] = s.split(">");
                            st[index++].setPlusminus(Integer.parseInt(split[1]));

                        }
                        vs = RegularExpression.getRegularExpression("<td\\sclass=\"form\">[^t]+", str);
                        index = 0;
                        for (String s : vs) {
                            Vector<String> vss = new Vector<String>();
                            vss = RegularExpression.getRegularExpression("<img\\ssrc=\"/i/.", s);
                            JSONArray jArray = new JSONArray();

                            for (String ss : vss) {
                                String splitValue[] = ss.split("/i/");
                                jArray.add(splitValue[1]);
                            }
                            st[index++].setMl(jArray.toString());
                        }
                        statTableList[k] = st;
                        //System.out.println("#status Table :"+k);
                    } else {
                        //System.out.println("IN CASE THIS");
                        SubLeagueM sl = new SubLeagueM();
                        sl = subDb.getBySubleagueName(l.getSubleagueName());
                        if (l.getSubLeagueNamePk() == null) {
                            status = 2;
                            break;
                        }
                        filename = l.getCt() + "_" + l.getCnPk() + "_" + l.getLnPk() + "_" + l.getLs() + "_" + sl.getSubLeagueNamePk() + "_" + k + ".txt";
                        fileman.setFilename(filename);
                        response.setContent(fileman.read());
                        if (response.getStatus() == 1) {
                            str = response.getContent();
                            rexp = "<div\\sid=\"statT\">";
                            vs = RegularExpression.getRegularExpression(rexp, str);
                            int count = 0;
                            if (vs.size() > 0) {
                                vs = RegularExpression.getRegularExpression("<td\\sclass=\"no\">[^<]+", str);
                                StatTableM st[] = new StatTableM[vs.size()];
                                //System.out.println("### size : " + vs.size() + " ###");
                                for (int i = 0; i < vs.size(); i++) {
                                    st[i] = new StatTableM();
                                    st[i].setCid(l.getCompetitionId());
                                    st[i].setLeagueId(l.getLeagueId());
                                    st[i].setSubLeagueNamePk(sl.getSubLeagueNamePk());
                                    st[i].setSubLeagueId(l.getSubleagueId() + "");
                                }

                                int index = 0;
                                for (String s : vs) {
                                    String split[] = s.split(">");
                                    st[index++].setNo(Integer.parseInt(split[1]));
                                }
                                vs = RegularExpression.getRegularExpression("<td\\sclass=\"team\"><span\\sclass=\"position\"[^>]*></span>[^>]+|<td\\sclass=\"team\">[^>]+", str);
                                index = 0;
                                for (String s : vs) {
                                    s = s.replaceAll("<span\\sclass=\"position\"[^>]*></span>", "");
                                    String split[] = s.split("/");
                                    st[index++].setTnPk(split[3]);

                                }
                                vs = RegularExpression.getRegularExpression("<td\\sclass=\"gp\">[^<]+", str);
                                index = 0;
                                for (String s : vs) {
                                    String split[] = s.split(">");
                                    st[index++].setGp(Integer.parseInt(split[1]));

                                }

                                vs = RegularExpression.getRegularExpression("<td\\sclass=\"pts\">[^<]+", str);
                                index = 0;
                                for (String s : vs) {
                                    String split[] = s.split(">");
                                    st[index++].setPts(Integer.parseInt(split[1]));

                                }

                                vs = RegularExpression.getRegularExpression("<td\\sclass=\"w\">[^<]+", str);
                                index = 0;
                                for (String s : vs) {
                                    String split[] = s.split(">");
                                    st[index++].setW(Integer.parseInt(split[1]));

                                }

                                vs = RegularExpression.getRegularExpression("<td\\sclass=\"d\">[^<]+", str);
                                index = 0;
                                for (String s : vs) {
                                    String split[] = s.split(">");
                                    st[index++].setD(Integer.parseInt(split[1]));

                                }

                                vs = RegularExpression.getRegularExpression("<td\\sclass=\"l\">[^<]+", str);
                                index = 0;
                                for (String s : vs) {
                                    String split[] = s.split(">");
                                    st[index++].setL(Integer.parseInt(split[1]));

                                }
                                vs = RegularExpression.getRegularExpression("<td\\sclass=\"gf\">[^<]+", str);
                                index = 0;
                                for (String s : vs) {
                                    String split[] = s.split(">");
                                    st[index++].setGf(Integer.parseInt(split[1]));

                                }
                                vs = RegularExpression.getRegularExpression("<td\\sclass=\"ga\">[^<]+", str);
                                index = 0;
                                for (String s : vs) {
                                    String split[] = s.split(">");
                                    st[index++].setGa(Integer.parseInt(split[1]));

                                }
                                vs = RegularExpression.getRegularExpression("<td\\sclass=\"plusminus\">[^<]+", str);
                                index = 0;
                                for (String s : vs) {
                                    String split[] = s.split(">");
                                    st[index++].setPlusminus(Integer.parseInt(split[1]));

                                }

                                vs = RegularExpression.getRegularExpression("<td\\sclass=\"form\">[^t]+", str);
                                index = 0;
                                for (String s : vs) {
                                    Vector<String> vss = new Vector<String>();
                                    vss = RegularExpression.getRegularExpression("<img\\ssrc=\"/i/.", s);
                                    JSONArray jArray = new JSONArray();

                                    for (String ss : vss) {
                                        String splitValue[] = ss.split("/i/");
                                        jArray.add(splitValue[1]);
                                    }
                                    st[index++].setMl(jArray.toString());
                                }

                                statTableList[k] = st;
                                //System.out.println("#status Table :"+k);
                            } else {
                                status = 2;
                                break;
                            }
                        }
                    }
                    //System.out.println(urlString1);

                } else {
                    status = 0;
                    break;
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        if (status == 1) {
            System.out.println("# Insert Stat Table #");
            sDb.add(statTableList[0]);
            sDb.add(statTableList[1], "h");
            sDb.add(statTableList[2], "g");
        } else if (status == 2) {
            System.out.println("# No Stat Table ###");
            status = 1;
        }
        return status;
    }
}
