/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.controller;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import net.sf.json.JSONObject;
import org.manager.FileManager;
import org.manager.RegularExpression;
import org.model.LiveMatchM;
import org.model.ResultM;
import org.model.TeamM;
import org.model.db.ResultDAO;
import org.model.db.ResultImpl;
import org.model.db.TeamDAO;
import org.model.db.TeamImpl;

/**
 *
 * @author mrsyrop
 */
public class ResultController {

    public boolean getResultsCompareV2(String cPk, String tnPk) {
        boolean returnValue = false;
        TeamDAO tDb = new TeamImpl();
        Vector<TeamM> vt = new Vector<TeamM>();
        ResultDAO rDb = new ResultImpl();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        int index = 0;
        while (true) {
            String urlString = "http://www.futbol24.com/team/" + cPk + "/" + tnPk + "/results/?Ajax=1&statTR-Page=" + (index++);
            System.out.println(urlString);
            JSONObject json = RegularExpression.getDataFromURL(urlString);
            Vector<String> vs = new Vector<String>();
            String value = json.get("status") + "";
            if (value.equals("true")) {
                returnValue = true;
                String str = json.getString("data");
                String rexp = "<div\\sid=\"statTR\">";
                vs = RegularExpression.getRegularExpression(rexp, str);
                try {
                    if (vs.size() > 0) {
                        //System.out.println(urlString);
                        rexp = "data-format=\"d\\.m\\.Y\">[^<]+";
                        vs = RegularExpression.getRegularExpression(rexp, str);
                        ResultM[] flist = new ResultM[vs.size()];
                        for (int i = 0; i < flist.length; i++) {
                            flist[i] = new ResultM();
                            flist[i].setTeamNamePk(tnPk);
                            Date d = new Date();

                            String splitDate[] = vs.get(i).split(">");
                            String array[] = splitDate[1].split("\\.");
                            String date = array[2] + "-" + array[1] + "-" + array[0];
                            //data-format="d.m.Y">25.11.2012
                            flist[i].setDate(date);
//                        flist[i].setLeagueId(leagueId);
//                        flist[i].setSubLeagueId(subLeagueId);

                        }
                        rexp = "class=\"cat[^<]+";
                        vs = RegularExpression.getRegularExpression(rexp, str);
                        for (int i = 0; i < flist.length; i++) {
                            String splitValue[] = vs.get(i).split(">");
                            String lnk = splitValue[1].trim();
                            String splitValue2[] = splitValue[0].split("\"");
                            String ln = splitValue2[3].trim();
                            flist[i].setLnk(lnk);
                            flist[i].setLeagueName(ln);
                        }
                        rexp = "<td\\sclass=\"comp\"><[^<]+";
                        vs = RegularExpression.getRegularExpression(rexp, str);
                        //System.out.println("comp:"+vs.size());
                        for (int i = 0; i < flist.length; i++) {

                            String splitValue[] = vs.get(i).split(">");
                            String name = splitValue[2].replaceAll(" ", "");
                            String split[] = splitValue[1].split("/");
                            String comType = split[1];
                            String comPk = split[2];
                            String lnPk = split[3];
                            String leagueSeason = split[4];
                            String subLeagueNamePk = "";
                            if (!split[5].contains("\"")) {
                                subLeagueNamePk = split[5];
                            }
                            //System.out.println("name="+name+",comType="+comType+",comPk="+comPk+",lnPk="+lnPk+",leagueSeason="+leagueSeason+",subLeagueNamePk="+subLeagueNamePk);
                            flist[i].setCompetitionNamePk(comPk);
                            flist[i].setLeagueNamePk(lnPk);
                            flist[i].setCompetitionType(comType);
                            flist[i].setLeagueSeasson(leagueSeason);
                            flist[i].setSubLeagueNamePk(subLeagueNamePk);
                        }
                        rexp = "<td\\sclass=\"team4\"><[^<]+";
                        vs = RegularExpression.getRegularExpression(rexp, str);
                        //System.out.println("team4:"+vs.size());
                        for (int i = 0; i < flist.length; i++) {
                            String splitValue[] = vs.get(i).split(">");
                            String hn = splitValue[2];
                            String split[] = splitValue[1].split("/");
                            String hcomPk = split[2];
                            String hnPk = split[3];
                            //System.out.println("teamNamePk:"+hnPk);
                            flist[i].setHn(hn);
                            flist[i].setTeamHomeNamePk(hnPk);

                            flist[i].setHcomPk(hcomPk);
                        }
                        rexp = "<td\\sclass=\"team5\"><[^<]+";
                        vs = RegularExpression.getRegularExpression(rexp, str);
                        //System.out.println("team5:"+vs.size());
                        for (int i = 0; i < flist.length; i++) {
                            String splitValue[] = vs.get(i).split(">");
                            String an = splitValue[2];
                            String split[] = splitValue[1].split("/");
                            String anPk = split[3];
                            String acomPk = split[2];

                            flist[i].setAn(an);
                            flist[i].setTeamAwayNamePk(anPk);
                            flist[i].setAcomPk(acomPk);
                            //System.out.println(an + "," + anPk + "," + acomPk);
                        }
                        rexp = "<td\\sclass=\"dash\"><[^<]+";
                        vs = RegularExpression.getRegularExpression(rexp, str);
                        //System.out.println("dash:"+vs.size());
                        for (int i = 0; i < flist.length; i++) {
                            String splitValue[] = vs.get(i).split(">");
                            String score = "";
                            if (splitValue.length >= 3) {
                                score = splitValue[2];
                            }
                            String split[] = splitValue[1].split("\"");
//                            System.out.println(splitValue[1]);
//                            System.out.println(split.length);
                            String link = split[3];
                            flist[i].setScore(score);
                            //System.out.println(link);
                            flist[i].setLink(link);
                        }

                        rexp = "<td><img\\ssrc=\"/i/[^\\.]+";
                        vs = RegularExpression.getRegularExpression(rexp, str);
                        //System.out.println("src:"+vs.size());
                        for (int i = 0; i < flist.length; i++) {
                            String splitValue[] = vs.get(i).split("/i/");
                            String r = splitValue[1];
                            //System.out.println(r);
                            flist[i].setR(r);

                        }
                        if (rDb.add(flist) == false) {
                            break;
                        }
                    } else {
                        break;
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
            break;
        }
        return returnValue;

    }

    public boolean haveResultsCompare(TeamM team) {
        boolean success = false;

        FileManager fileman = new FileManager();

        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        String path = "spiderbotdoc/team_result/" + ft.format(dNow).toString();
        String filename;
        fileman.setPath(path);

        filename = team.getComPk() + "_" + team.getTnPk() + ".txt";
        fileman.setFilename(filename);
        if (FileManager.isFile(fileman)) {
            success = true;
        }

        return success;
    }
}
