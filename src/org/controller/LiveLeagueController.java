/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.manager.FileManager;
import org.model.FixtureM;
import org.model.LiveLeagueCheckedM;
import org.model.LiveLeagueM;
import org.model.LiveMatchM;
import org.model.RequestHeaderM;
import org.model.ResponseHeaderM;
import org.model.ResultM;
import org.model.TeamM;
import org.model.db.FixtureDAO;
import org.model.db.FixtureImpl;
import org.model.db.LiveLeagueCheckedDAO;
import org.model.db.LiveLeagueCheckedImpl;
import org.model.db.LiveLeagueDAO;
import org.model.db.LiveLeagueImpl;
import org.model.db.LiveMatchDAO;
import org.model.db.LiveMatchImpl;
import org.model.db.ResultDAO;
import org.model.db.ResultImpl;
import org.model.db.StatTableDAO;
import org.model.db.StatTableImpl;

/**
 *
 * @author mrsyrop
 */
public class LiveLeagueController {

    String rexpResultDate = "data-format=\"H:i\"\\stitle=\"[^\"]+";
    String rexpResultComp = "<td\\sclass=\"comp\"><[^<]+";
    String rexpResultHome = "<td\\sclass=\"team4\"><[^<]+|<td\\sclass=\"team2\"><[^<]+";
    String rexpResultAway = "<td\\sclass=\"team5\"><[^<]+|<td\\sclass=\"team3\"><[^<]+";
    String rexpResultMatchDetail = "<td\\sclass=\"dash\"><[^<]+";
    String rexpFHTF = "Half-time\\s/\\sFull-time";
    String rexpUnder = "<td\\sclass=\"under\">[^<]+";
    String rexpPercent = "<td\\sclass=\"percent\">[^<]+";
    String rexpQty = "</div></div>\\d+";

    public void update(Vector<LiveLeagueM> ll) {

        RequestHeaderM req = new RequestHeaderM();
        ResponseHeaderM res;
        ResultDAO resultDb = new ResultImpl();
        TeamController tc = new TeamController();
        FileManager fileman = new FileManager();
//        LeagueStatDb lsDb = new LeagueStatDb();
//        LiveMatchDb lmDb = new LiveMatchDb();
        StatTableDAO st = new StatTableImpl();
        LiveLeagueCheckedDAO llcDb = new LiveLeagueCheckedImpl();
//        new LeagueDb().writeFile();
        LiveLeagueDAO liveLeagueDAO = new LiveLeagueImpl();
        liveLeagueDAO.writeFile();

        int index = 1;
        for (LiveLeagueM l : ll) {

            System.out.println("##### " + (index++) + " of " + ll.size() + " #####");
            int page = 0;
            boolean value = true;
            res = new ResponseHeaderM();

            if ((l.getLnPk().equals("Serie-A") || l.getLnPk().equals("Serie-B") || l.getLnPk().equals("Serie-C") || l.getLnPk().equals("Copa-do-Brasil-Sub-20")) && l.getCnPk().equals("Brasil")) {
                l.setCnPk("Brazil");
            }
            //req.setUrl("http://www.futbol24.com/" + l.getCt() + "/" + l.getCnPk() + "/" + l.getLnPk() + "/" + l.getLs() + "/results/?Ajax=1&statLR-Page=" + page);
            //System.out.println(req.getUrl());

            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
            String path = "spiderbotdoc/league/results/" + ft.format(dNow).toString();
            String filename = l.getCt() + "_" + l.getCnPk() + "_" + l.getLnPk() + "_" + l.getLs() + ".txt";
            fileman.setPath(path);
            fileman.setFilename(filename);

            //res = http.getContent(req);
            res.setContent(fileman.read());
            //if (res.getStatus() == 1) {
            if (FileManager.isFile(fileman)) {
                ResultM list[] = getResults(res.getContent(), l.getLeagueId() + "", l.getSubleagueId() + "", l.getLn());
                if (list.length == 0) {
                    System.out.println("Empty file:" + fileman.getFullpath());
                    fileman.delete();
                } else if (resultDb.addLeagueResult(list) == false) {

                }
                int fixtureStatus = getFixtures(l);
                if (fixtureStatus == 1) {
                    int statTableStatus = tc.getTable(l);
                    if (statTableStatus == 1) {

                        st.writeFile(l.getLeagueId());
                        if (l.getLeagueId() != l.getSubleagueId()) {
                            st.writeFile(l.getSubleagueId());
                        }
                        LiveLeagueCheckedM llc = new LiveLeagueCheckedM();
                        llc.setDate(l.getDate());
                        llc.setLeagueId(l.getLeagueId() + "");
//                        System.out.println("Live League Checked leagueId:" + l.getLeagueId());
                        llcDb.add(llc);
                    }
                }

            }
        }

    }

    public ResultM[] getResults(String content, String leagueId, String subLeagueId, String leagueName) {
        //data-format="H:i" title="07.10.2012 17:00"
        Vector<String> vs = new Vector<String>();
        String rexpResultDate2 = "data-dymek=\"[^\"]+\"";
        vs = RE.getRegularExpression(rexpResultDate2, content);
        //System.out.println(vs.size());
        ResultM list[] = new ResultM[vs.size()];
        if (list.length > 0) {
            for (int i = 0; i < vs.size(); i++) {
                String s = vs.get(i);
                //System.out.println(s);
                String split[] = s.split("\"");
                list[i] = new ResultM();
                list[i].setDatetime(split[1]);
                list[i].setLeagueId(leagueId);
                list[i].setSubLeagueId(subLeagueId);
                list[i].setLeagueName(leagueName);
                //System.out.println(split[1]);

            }
            vs = RE.getRegularExpression(rexpResultHome, content);
            for (int i = 0; i < vs.size(); i++) {
                String s = vs.get(i);
                String split[] = s.split(">");
                list[i].setHn(split[2].trim());
                String split2[] = split[1].split("/");
                list[i].setHcomPk(split2[2]);
                list[i].setTeamHomeNamePk(split2[3]);
            }
            vs = RE.getRegularExpression(rexpResultAway, content);
            for (int i = 0; i < vs.size(); i++) {
                String s = vs.get(i);
                String split[] = s.split(">");
                list[i].setAn(split[2].trim());
                String split2[] = split[1].split("/");
                list[i].setAcomPk(split2[2]);
                list[i].setTeamAwayNamePk(split2[3]);
            }
            vs = RE.getRegularExpression(rexpResultMatchDetail, content);
            for (int i = 0; i < vs.size(); i++) {
                String s = vs.get(i);
                String split[] = s.split(">");
                if (split.length < 3) {
                } else {
                    list[i].setScore(split[2]);
                    String link[] = split[1].split("\"");
                    list[i].setLink(link[3]);
                }

            }
            vs = RE.getRegularExpression(rexpResultComp, content);
            if (list.length == vs.size()) {
                for (int i = 0; i < vs.size(); i++) {
                    String s = vs.get(i);
                    String split[] = s.split("\"");
                    String link = split[3];
                    String split2[] = link.split("/");
                    list[i].setCompetitionType(split2[1]);
                    list[i].setCompetitionNamePk(split2[2]);
                    list[i].setLeagueNamePk(split2[3]);
                    list[i].setLeagueSeasson(split2[4]);
                    list[i].setSubLeagueName(split[split.length - 1].replaceAll(">", ""));

                    if (split2.length == 6) {
                        list[i].setSubLeagueNamePk(split2[5]);
                    }

                }
            } else {
                for (int i = 0; i < list.length; i++) {
                    if (list[i].getLink() == null) {
                    } else {
                        String split[] = list[i].getLink().split("/");
                        list[i].setCompetitionType(split[5]);
                        list[i].setCompetitionNamePk(split[6]);
                        list[i].setLeagueNamePk(split[7]);
                        list[i].setLeagueSeasson(split[8]);
                        list[i].setSubLeagueName("");
                        list[i].setSubLeagueNamePk("");
                    }
                }
            }

        }

        return list;
    }

    public int getFixtures(LiveLeagueM l) {
        int status = 0;
        FileManager fileman = new FileManager();
        FixtureDAO fixtureDb = new FixtureImpl();
        int page = 0;
        int total = 0;
        ResponseHeaderM res;
        RequestHeaderM req = new RequestHeaderM();
        Vector<String> vs = new Vector<String>();
        Vector<FixtureM[]> vf = new Vector<FixtureM[]>();
        res = new ResponseHeaderM();
        //http://www.futbol24.com/national/England/Premier-League/2012-2013/fixtures/?Ajax=1&statLF-Page=1
        //req.setUrl("http://www.futbol24.com/" + l.getCt() + "/" + l.getCnPk() + "/" + l.getLnPk() + "/" + l.getLs() + "/fixtures/?Ajax=1&statLF-Page=" + page);
        //System.out.println(req.getUrl());

        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        String path = "spiderbotdoc/league/fixture/" + ft.format(dNow).toString();
        String filename = l.getCt() + "_" + l.getCnPk() + "_" + l.getLnPk() + "_" + l.getLs() + ".txt";
        fileman.setPath(path);
        fileman.setFilename(filename);

        res.setContent(fileman.getContent());
//        res = http.getContent(req);
//        if (res.getStatus() == 1) {
        if (FileManager.isFile(fileman)) {
            status = res.getStatus();
            String content = res.getContent();
            String rexpResultDate2 = "data-dymek=\"[^\"]+\"";
            vs = RE.getRegularExpression(rexpResultDate2, content);
            FixtureM[] list = new FixtureM[vs.size()];
            //System.out.println(list.length);
            if (list.length > 0) {
                for (int i = 0; i < vs.size(); i++) {
                    String s = vs.get(i);
                    String split[] = s.split("\"");
                    list[i] = new FixtureM();
                    list[i].setDatetime(split[1]);
                    list[i].setLeagueId(l.getLeagueId() + "");
                    list[i].setSubLeagueId(l.getSubleagueId() + "");
                    list[i].setLeagueName(l.getLn());

                    //list[i].setSubLeagueName(l.getSubleagueName());
                }
                vs = RE.getRegularExpression(rexpResultHome, content);
                for (int i = 0; i < vs.size(); i++) {
                    String s = vs.get(i);
                    String split[] = s.split(">");
                    list[i].setHn(split[2].trim());
                    String split2[] = split[1].split("/");
                    list[i].setHcomPk(split2[2]);
                    list[i].setTeamHomeNamePk(split2[3]);
                }
                vs = RE.getRegularExpression(rexpResultAway, content);
                for (int i = 0; i < vs.size(); i++) {
                    String s = vs.get(i);
                    String split[] = s.split(">");
                    list[i].setAn(split[2].trim());
                    String split2[] = split[1].split("/");
                    list[i].setAcomPk(split2[2]);
                    list[i].setTeamAwayNamePk(split2[3]);
                }

                vs = RE.getRegularExpression(rexpResultComp, content);
                if (list.length == vs.size()) {
                    for (int i = 0; i < vs.size(); i++) {
                        String s = vs.get(i);
                        String split[] = s.split("\"");
                        String link = split[3];
                        String split2[] = link.split("/");
                        list[i].setCompetitionType(split2[1]);
                        list[i].setCompetitionNamePk(split2[2]);
                        list[i].setLeagueNamePk(split2[3]);
                        list[i].setLeagueSeasson(split2[4]);
                        list[i].setSubLeagueName(split[split.length - 1].replaceAll(">", ""));
                        if (split2.length == 6) {
                            list[i].setSubLeagueNamePk(split2[5]);
                        }

                    }
                } else {

                    for (int i = 0; i < list.length; i++) {

                        list[i].setCompetitionType(l.getCt());
                        list[i].setCompetitionNamePk(l.getCnPk());
                        list[i].setLeagueNamePk(l.getLnPk());
                        list[i].setLeagueSeasson(l.getLs());
                        list[i].setSubLeagueName("");
                        list[i].setSubLeagueNamePk("");
                    }
                }

            }
            for (FixtureM f : list) {
                if (f.getSubLeagueNamePk() == null) {
                    f.setSubLeagueName("");
                    f.setSubLeagueNamePk("");
                }
            }

            if (list.length > 0) {

                vf.add(list);

            }

            if (list.length == 0) {

                if (vf.size() > 0) {

                    fixtureDb.addLeagueFixture(vf);
                }
            }
        }
        return status;

    }

    public void getLeagueInfo() {

        LiveMatchDAO lDb = new LiveMatchImpl();
        LiveLeagueDAO llDb = new LiveLeagueImpl();
//        ResultVsDb rDb = new ResultVsDb();
//        TeamDb tDb = new TeamDb();
//        FormTableDb fDb = new FormTableDb();
//        Head2HeadDb h2hDb = new Head2HeadDb();
//        StatOUDb sDb = new StatOUDb();
//        CompetitionsDb comDb = new CompetitionsDb();
        Vector<LiveMatchM> vl = new Vector<LiveMatchM>();
        vl = lDb.retrieve(); // LiveMatch Vector
//        TeamController tc = new TeamController();
        TeamM h = new TeamM();
        TeamM g = new TeamM();
        Vector<LiveLeagueM> ll = new Vector<LiveLeagueM>();
        ll = llDb.retrieveByChecked(); //LiveLeague Vector
        System.out.println("######## GET League Info #########");
        System.out.println("LiveMatch :" + vl.size());
        System.out.println("LiveLeague :" + ll.size());
        //lcon.getLeagueInfo(); //update league info 
        Date d = new Date();
        Long start = d.getTime();
        this.update(ll); // update league stat

    }

}
