/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

import java.util.HashMap;
import java.util.Map;
import org.utility.IntegerUtility;

/**
 *
 * @author superx
 */
public class LiveLeagueM {

    // Country
    private int competitionId = 0;
    private String kn = "";
    private String kkod = "";

    // League And SubLeague
    private int leagueId = 0;
    private String date = "";
    private int subleagueId = 0;
    private String subleagueName = "";
    private int oid = 0;
    private String ln = "";
    private String lnk = "";
    private String fg = "";
    private String bg = "";

    // Not Know?
    private String cn = "";
    private String cnk = "";
    private String cnPk = "";
    private String ct = "";
    private String ls = "";
    private String lnPk = "";
    private String subLeagueNamePk = "";
    private String subLeagueId = "";

    // Group league
    public void setValueByKey(String key, String value) {

        switch (key) {
            case "kid":
                this.competitionId = IntegerUtility.string2Int(value);
                break;
            case "kn":
                this.kn = value;
                break;
            case "kkod":
                this.kkod = value;
                break;
        }

    }

    public void setValueByKeyLeague(String key, String value) {

        switch (key) {
            case "lid":
                this.leagueId = IntegerUtility.string2Int(value);
                this.subleagueId = IntegerUtility.string2Int(value);
                break;
            case "oid":
                this.oid = IntegerUtility.string2Int(value);
                break;
            case "ln":
                this.ln = value;
                break;
            case "lnk":
                this.lnk = value;
                break;
            case "fg":
                this.fg = value;
                break;
            case "bg":
                this.bg = value;
                break;
        }

    }

    public void setValueByKeySubLeague(String key, String value) {

        switch (key) {
            case "lid":
                this.subleagueId = IntegerUtility.string2Int(value);
                break;
            case "_lid":
                this.leagueId = IntegerUtility.string2Int(value);
                break;
            case "ln":
                this.ln = value;
                break;
            case "lnk":
                this.lnk = value;
                break;
        }

    }

    public void setValueByCountry(HashMap<String, String> map) {
        for (Map.Entry<String, String> attr : map.entrySet()) {
            this.setValueByKey(attr.getKey(), attr.getValue());
        }
    }

    public void setValueByLeague(HashMap<String, String> map) {
        for (Map.Entry<String, String> attr : map.entrySet()) {
            this.setValueByKeyLeague(attr.getKey(), attr.getValue());
        }
    }

    public void setValueBySubLeague(HashMap<String, String> map) {
        for (Map.Entry<String, String> attr : map.entrySet()) {
            this.setValueByKeySubLeague(attr.getKey(), attr.getValue());
        }
    }

    public String getSqlInsertValue() {
        String query = "REPLACE INTO live_league (leagueId,competitionId,date,subleagueId,subleagueName,oid,ln,lnk,fg,bg,kn,kkod,new) VALUES ";
        String value = "(";
        value += "'" + this.leagueId + "'";
        value += ",'" + this.competitionId + "'";
        value += ",'" + this.date + "'";
        value += ",'" + this.subleagueId + "'";
        value += ",'" + this.subleagueName + "'";
        value += ",'" + this.oid + "'";
        value += ",'" + this.ln + "'";
        value += ",'" + this.lnk + "'";
        value += ",'" + this.fg + "'";
        value += ",'" + this.bg + "'";
        value += ",'" + this.kn + "'";
        value += ",'" + this.kkod + "'";
        value += ",'Y'";
        value += ")";

        return query += value;
    }

    public int getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(int leagueId) {
        this.leagueId = leagueId;
    }

    public int getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(int competitionId) {
        this.competitionId = competitionId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getSubleagueId() {
        return subleagueId;
    }

    public void setSubleagueId(int subleagueId) {
        this.subleagueId = subleagueId;
    }

    public String getSubleagueName() {
        return subleagueName;
    }

    public void setSubleagueName(String subleagueName) {
        this.subleagueName = subleagueName;
    }

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public String getLn() {
        return ln;
    }

    public void setLn(String ln) {
        this.ln = ln;
    }

    public String getLnk() {
        return lnk;
    }

    public void setLnk(String lnk) {
        this.lnk = lnk;
    }

    public String getFg() {
        return fg;
    }

    public void setFg(String fg) {
        this.fg = fg;
    }

    public String getBg() {
        return bg;
    }

    public void setBg(String bg) {
        this.bg = bg;
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getCnk() {
        return cnk;
    }

    public void setCnk(String cnk) {
        this.cnk = cnk;
    }

    public String getKkod() {
        return kkod;
    }

    public void setKkod(String kkod) {
        this.kkod = kkod;
    }

    public String getKn() {
        return kn;
    }

    public void setKn(String kn) {
        this.kn = kn;
    }

    public String getCnPk() {
        return cnPk;
    }

    public void setCnPk(String cnPk) {
        this.cnPk = cnPk;
    }

    public String getCt() {
        return ct;
    }

    public void setCt(String ct) {
        this.ct = ct;
    }

    public String getLs() {
        return ls;
    }

    public void setLs(String ls) {
        this.ls = ls;
    }

    public String getLnPk() {
        return lnPk;
    }

    public void setLnPk(String lnPk) {
        this.lnPk = lnPk;
    }

    public String getSubLeagueNamePk() {
        return subLeagueNamePk;
    }

    public void setSubLeagueNamePk(String subLeagueNamePk) {
        this.subLeagueNamePk = subLeagueNamePk;
    }

    public String getSubLeagueId() {
        return subLeagueId;
    }

    public void setSubLeagueId(String subLeagueId) {
        this.subLeagueId = subLeagueId;
    }
}
