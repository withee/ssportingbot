/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

/**
 *
 * @author mrsyrop
 */
public class ResultM {

    private String date;
    private String teamHomeNamePk;
    private String teamAwayNamePk;
    private String leagueNamePk;
    private String competitionType;
    private String competitionNamePk;
    private String leagueSeasson;
    private String subLeagueNamePk;
    private String leagueName;
    private String hn;
    private String an;
    private String hcomPk;
    private String acomPk;
    private String r;
    private String score;
    private String dateMo;
    private String link;
    private String teamNamePk;
    private String datetime;
    private String subLeagueName;
    private String leagueId;
    private String subLeagueId;
    private String lnk;

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the teamHomeNamePk
     */
    public String getTeamHomeNamePk() {
        return teamHomeNamePk;
    }

    /**
     * @param teamHomeNamePk the teamHomeNamePk to set
     */
    public void setTeamHomeNamePk(String teamHomeNamePk) {
        this.teamHomeNamePk = teamHomeNamePk;
    }

    /**
     * @return the teamAwayNamePk
     */
    public String getTeamAwayNamePk() {
        return teamAwayNamePk;
    }

    /**
     * @param teamAwayNamePk the teamAwayNamePk to set
     */
    public void setTeamAwayNamePk(String teamAwayNamePk) {
        this.teamAwayNamePk = teamAwayNamePk;
    }

    /**
     * @return the leagueNamePk
     */
    public String getLeagueNamePk() {
        return leagueNamePk;
    }

    /**
     * @param leagueNamePk the leagueNamePk to set
     */
    public void setLeagueNamePk(String leagueNamePk) {
        this.leagueNamePk = leagueNamePk;
    }

    /**
     * @return the competitionType
     */
    public String getCompetitionType() {
        return competitionType;
    }

    /**
     * @param competitionType the competitionType to set
     */
    public void setCompetitionType(String competitionType) {
        this.competitionType = competitionType;
    }

    /**
     * @return the competitionNamePk
     */
    public String getCompetitionNamePk() {
        return competitionNamePk;
    }

    /**
     * @param competitionNamePk the competitionNamePk to set
     */
    public void setCompetitionNamePk(String competitionNamePk) {
        this.competitionNamePk = competitionNamePk;
    }

    /**
     * @return the leagueSeasson
     */
    public String getLeagueSeasson() {
        return leagueSeasson;
    }

    /**
     * @param leagueSeasson the leagueSeasson to set
     */
    public void setLeagueSeasson(String leagueSeasson) {
        this.leagueSeasson = leagueSeasson;
    }

    /**
     * @return the subLeagueNamePk
     */
    public String getSubLeagueNamePk() {
        return subLeagueNamePk;
    }

    /**
     * @param subLeagueNamePk the subLeagueNamePk to set
     */
    public void setSubLeagueNamePk(String subLeagueNamePk) {
        this.subLeagueNamePk = subLeagueNamePk;
    }

    /**
     * @return the leagueName
     */
    public String getLeagueName() {
        return leagueName;
    }

    /**
     * @param leagueName the leagueName to set
     */
    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    /**
     * @return the hn
     */
    public String getHn() {
        return hn;
    }

    /**
     * @param hn the hn to set
     */
    public void setHn(String hn) {
        this.hn = hn;
    }

    /**
     * @return the an
     */
    public String getAn() {
        return an;
    }

    /**
     * @param an the an to set
     */
    public void setAn(String an) {
        this.an = an;
    }

    /**
     * @return the hcomPk
     */
    public String getHcomPk() {
        return hcomPk;
    }

    /**
     * @param hcomPk the hcomPk to set
     */
    public void setHcomPk(String hcomPk) {
        this.hcomPk = hcomPk;
    }

    /**
     * @return the acomPk
     */
    public String getAcomPk() {
        return acomPk;
    }

    /**
     * @param acomPk the acomPk to set
     */
    public void setAcomPk(String acomPk) {
        this.acomPk = acomPk;
    }

    /**
     * @return the r
     */
    public String getR() {
        return r;
    }

    /**
     * @param r the r to set
     */
    public void setR(String r) {
        this.r = r;
    }

    /**
     * @return the score
     */
    public String getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(String score) {
        this.score = score;
    }

    /**
     * @return the dateMo
     */
    public String getDateMo() {
        return dateMo;
    }

    /**
     * @param dateMo the dateMo to set
     */
    public void setDateMo(String dateMo) {
        this.dateMo = dateMo;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return the teamNamePk
     */
    public String getTeamNamePk() {
        return teamNamePk;
    }

    /**
     * @param teamNamePk the teamNamePk to set
     */
    public void setTeamNamePk(String teamNamePk) {
        this.teamNamePk = teamNamePk;
    }

    /**
     * @return the datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * @param datetime the datetime to set
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    /**
     * @return the subLeagueName
     */
    public String getSubLeagueName() {
        return subLeagueName;
    }

    /**
     * @param subLeagueName the subLeagueName to set
     */
    public void setSubLeagueName(String subLeagueName) {
        this.subLeagueName = subLeagueName;
    }

    /**
     * @return the leagueId
     */
    public String getLeagueId() {
        return leagueId;
    }

    /**
     * @param leagueId the leagueId to set
     */
    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }

    /**
     * @return the subLeagueId
     */
    public String getSubLeagueId() {
        return subLeagueId;
    }

    /**
     * @param subLeagueId the subLeagueId to set
     */
    public void setSubLeagueId(String subLeagueId) {
        this.subLeagueId = subLeagueId;
    }

    /**
     * @return the lnk
     */
    public String getLnk() {
        return lnk;
    }

    /**
     * @param lnk the lnk to set
     */
    public void setLnk(String lnk) {
        this.lnk = lnk;
    }
}
