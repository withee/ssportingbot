/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

import java.util.Map;
import java.util.Vector;

/**
 *
 * @author mrsyrop
 */
public class ResponseHeaderM {

    private Map headerContent;
    private String content;
    private Vector<String> contentVector;
    private int status = 1;

    /**
     * @return the headerContent
     */
    public Map getHeaderContent() {
        return headerContent;
    }

    /**
     * @param headerContent the headerContent to set
     */
    public void setHeaderContent(Map headerContent) {
        this.headerContent = headerContent;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the contentVector
     */
    public Vector<String> getContentVector() {
        return contentVector;
    }

    /**
     * @param contentVector the contentVector to set
     */
    public void setContentVector(Vector<String> contentVector) {
        this.contentVector = contentVector;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }
}
