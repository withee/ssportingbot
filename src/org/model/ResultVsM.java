/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

/**
 *
 * @author mrsyrop
 */
public class ResultVsM {

    private String tnPk1;
    private String tnPk2;
    private String cnPk;
    private String date;
    private String ct;
    private String lnPk;
    private String season;
    private String lnr;
    private String matchDate;
    private String hmn;
    private String gmn;
    private String score;
    private String vs;
    private String sl;

    /**
     * @return the tnPk1
     */
    public String getTnPk1() {
        return tnPk1;
    }

    /**
     * @param tnPk1 the tnPk1 to set
     */
    public void setTnPk1(String tnPk1) {
        this.tnPk1 = tnPk1;
    }

    /**
     * @return the tnPk2
     */
    public String getTnPk2() {
        return tnPk2;
    }

    /**
     * @param tnPk2 the tnPk2 to set
     */
    public void setTnPk2(String tnPk2) {
        this.tnPk2 = tnPk2;
    }

    /**
     * @return the cnPk
     */
    public String getCnPk() {
        return cnPk;
    }

    /**
     * @param cnPk the cnPk to set
     */
    public void setCnPk(String cnPk) {
        this.cnPk = cnPk;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the ct
     */
    public String getCt() {
        return ct;
    }

    /**
     * @param ct the ct to set
     */
    public void setCt(String ct) {
        this.ct = ct;
    }

    /**
     * @return the lnPk
     */
    public String getLnPk() {
        return lnPk;
    }

    /**
     * @param lnPk the lnPk to set
     */
    public void setLnPk(String lnPk) {
        this.lnPk = lnPk;
    }

    /**
     * @return the season
     */
    public String getSeason() {
        return season;
    }

    /**
     * @param season the season to set
     */
    public void setSeason(String season) {
        this.season = season;
    }

    /**
     * @return the lnr
     */
    public String getLnr() {
        return lnr;
    }

    /**
     * @param lnr the lnr to set
     */
    public void setLnr(String lnr) {
        this.lnr = lnr;
    }

    /**
     * @return the matchDate
     */
    public String getMatchDate() {
        return matchDate;
    }

    /**
     * @param matchDate the matchDate to set
     */
    public void setMatchDate(String matchDate) {
        this.matchDate = matchDate;
    }

    /**
     * @return the hmn
     */
    public String getHmn() {
        return hmn;
    }

    /**
     * @param hmn the hmn to set
     */
    public void setHmn(String hmn) {
        this.hmn = hmn;
    }

    /**
     * @return the gmn
     */
    public String getGmn() {
        return gmn;
    }

    /**
     * @param gmn the gmn to set
     */
    public void setGmn(String gmn) {
        this.gmn = gmn;
    }

    /**
     * @return the score
     */
    public String getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(String score) {
        this.score = score;
    }

    /**
     * @return the vs
     */
    public String getVs() {
        return vs;
    }

    /**
     * @param vs the vs to set
     */
    public void setVs(String vs) {
        this.vs = vs;
    }

    /**
     * @return the sl
     */
    public String getSl() {
        return sl;
    }

    /**
     * @param sl the sl to set
     */
    public void setSl(String sl) {
        this.sl = sl;
    }
}
