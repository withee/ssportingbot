/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

/**
 *
 * @author mrsyrop
 */
public class TeamM {
    private int tid;
    private String tn;
    private String tnPk;
    private String tnTh;
    private String tnBig;
    private String tnGb;
    private String tnEn;
    private String tnVn;
    private String tnKr;
    private String tnLa;
    private int cid;
    private String comPk;
    private String comType;
    private String comNameTh;
    private String comNameBig;
    private String comNameGb;
    private String comNameEn;
    private String comNameVn;
    private String comNameKr;
    private String comNameLa;
    private String comName;

    /**
     * @return the tid
     */
    public int getTid() {
        return tid;
    }

    /**
     * @param tid the tid to set
     */
    public void setTid(int tid) {
        this.tid = tid;
    }

    /**
     * @return the tn
     */
    public String getTn() {
        return tn;
    }

    /**
     * @param tn the tn to set
     */
    public void setTn(String tn) {
        this.tn = tn;
    }

    /**
     * @return the tnPk
     */
    public String getTnPk() {
        return tnPk;
    }

    /**
     * @param tnPk the tnPk to set
     */
    public void setTnPk(String tnPk) {
        this.tnPk = tnPk;
    }

    /**
     * @return the tnTh
     */
    public String getTnTh() {
        return tnTh;
    }

    /**
     * @param tnTh the tnTh to set
     */
    public void setTnTh(String tnTh) {
        this.tnTh = tnTh;
    }

    /**
     * @return the tnBig
     */
    public String getTnBig() {
        return tnBig;
    }

    /**
     * @param tnBig the tnBig to set
     */
    public void setTnBig(String tnBig) {
        this.tnBig = tnBig;
    }

    /**
     * @return the tnGb
     */
    public String getTnGb() {
        return tnGb;
    }

    /**
     * @param tnGb the tnGb to set
     */
    public void setTnGb(String tnGb) {
        this.tnGb = tnGb;
    }

    /**
     * @return the tnEn
     */
    public String getTnEn() {
        return tnEn;
    }

    /**
     * @param tnEn the tnEn to set
     */
    public void setTnEn(String tnEn) {
        this.tnEn = tnEn;
    }

    /**
     * @return the tnVn
     */
    public String getTnVn() {
        return tnVn;
    }

    /**
     * @param tnVn the tnVn to set
     */
    public void setTnVn(String tnVn) {
        this.tnVn = tnVn;
    }

    /**
     * @return the tnKr
     */
    public String getTnKr() {
        return tnKr;
    }

    /**
     * @param tnKr the tnKr to set
     */
    public void setTnKr(String tnKr) {
        this.tnKr = tnKr;
    }

    /**
     * @return the tnLa
     */
    public String getTnLa() {
        return tnLa;
    }

    /**
     * @param tnLa the tnLa to set
     */
    public void setTnLa(String tnLa) {
        this.tnLa = tnLa;
    }

    /**
     * @return the cid
     */
    public int getCid() {
        return cid;
    }

    /**
     * @param cid the cid to set
     */
    public void setCid(int cid) {
        this.cid = cid;
    }

    /**
     * @return the comPk
     */
    public String getComPk() {
        return comPk;
    }

    /**
     * @param comPk the comPk to set
     */
    public void setComPk(String comPk) {
        this.comPk = comPk;
    }

    /**
     * @return the comType
     */
    public String getComType() {
        return comType;
    }

    /**
     * @param comType the comType to set
     */
    public void setComType(String comType) {
        this.comType = comType;
    }

    /**
     * @return the comNameTh
     */
    public String getComNameTh() {
        return comNameTh;
    }

    /**
     * @param comNameTh the comNameTh to set
     */
    public void setComNameTh(String comNameTh) {
        this.comNameTh = comNameTh;
    }

    /**
     * @return the comNameBig
     */
    public String getComNameBig() {
        return comNameBig;
    }

    /**
     * @param comNameBig the comNameBig to set
     */
    public void setComNameBig(String comNameBig) {
        this.comNameBig = comNameBig;
    }

    /**
     * @return the comNameGb
     */
    public String getComNameGb() {
        return comNameGb;
    }

    /**
     * @param comNameGb the comNameGb to set
     */
    public void setComNameGb(String comNameGb) {
        this.comNameGb = comNameGb;
    }

    /**
     * @return the comNameEn
     */
    public String getComNameEn() {
        return comNameEn;
    }

    /**
     * @param comNameEn the comNameEn to set
     */
    public void setComNameEn(String comNameEn) {
        this.comNameEn = comNameEn;
    }

    /**
     * @return the comNameVn
     */
    public String getComNameVn() {
        return comNameVn;
    }

    /**
     * @param comNameVn the comNameVn to set
     */
    public void setComNameVn(String comNameVn) {
        this.comNameVn = comNameVn;
    }

    /**
     * @return the comNameKr
     */
    public String getComNameKr() {
        return comNameKr;
    }

    /**
     * @param comNameKr the comNameKr to set
     */
    public void setComNameKr(String comNameKr) {
        this.comNameKr = comNameKr;
    }

    /**
     * @return the comNameLa
     */
    public String getComNameLa() {
        return comNameLa;
    }

    /**
     * @param comNameLa the comNameLa to set
     */
    public void setComNameLa(String comNameLa) {
        this.comNameLa = comNameLa;
    }

    /**
     * @return the comName
     */
    public String getComName() {
        return comName;
    }

    /**
     * @param comName the comName to set
     */
    public void setComName(String comName) {
        this.comName = comName;
    }
    
}
