/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.utility.IntegerUtility;

/**
 *
 * @author superx
 */
public class LiveMatchM {

    private int zid = 0;
    private int mid = 0;
    private int x = 0;
    private int lid = 0; //subleague
    private int slid = 0;
    private int _lid = 0;//with _ is mainleague
    private int oid = 0;
    private int sid = 0;
    private int kid = 0;
    private int lnr = 0;
    private long c0 = 0;
    private long c1 = 0;
    private long c2 = 0;
    private int ml = 0;
    private int hid = 0;
    private int gid = 0;
    private String hn = "";
    private String gn = "";
    private int hrci = 0;
    private int grci = 0;
    private String grc = "";
    private String hrc = "";
    private String s1 = "";
    private String s2 = "";
    private int mstan = 0;
    private String otv = "";
    private int hstan = 0;
    private int gstan = 0;
    private int goli = 0;
    private int l = 0;
    private int a = 0;
    private int ao = 0;
    private int cx = 0;
    private String info = "";
    private int rek_h = 0;
    private String date = "";
    private int gols = 0;
    private String showDate = "";
    private int rcs = 0;
    private String m = "";
    private String lnPk = "";
    private String cnPk = "";
    private String event_id = "";

    public void setValueByKey(String key, String value) {

        switch (key) {
            case "zid":
                this.zid = IntegerUtility.string2Int(value);
                break;
            case "mid":
                this.mid = IntegerUtility.string2Int(value);
                break;
            case "x":
                this.x = IntegerUtility.string2Int(value);
                break;
            case "lid":
                this.lid = IntegerUtility.string2Int(value);
                this.slid = IntegerUtility.string2Int(value);
                break;
            case "slid":
                this.slid = IntegerUtility.string2Int(value);
                break;
            case "_lid":
                this._lid = IntegerUtility.string2Int(value);                
                break;
            case "oid":
                this.oid = IntegerUtility.string2Int(value);
                break;
            case "sid":
                this.sid = IntegerUtility.string2Int(value);
                break;
            case "kid":
                this.kid = IntegerUtility.string2Int(value);
                break;
            case "lnr":
                this.lnr = IntegerUtility.string2Int(value);
                break;
            case "c0":
                this.c0 = IntegerUtility.string2Int(value);
                break;
            case "c1":
                this.c1 = IntegerUtility.string2Int(value);
                break;
            case "c2":
                this.c2 = IntegerUtility.string2Int(value);
                break;
            case "ml":
                this.ml = IntegerUtility.string2Int(value);
                break;
            case "hid":
                this.hid = IntegerUtility.string2Int(value);
                break;
            case "gid":
                this.gid = IntegerUtility.string2Int(value);
                break;
            case "hn":
                this.hn = value;
                break;
            case "gn":
                this.gn = value;
                break;
            case "hrci":
                this.hrci = IntegerUtility.string2Int(value);
                break;
            case "grci":
                this.grci = IntegerUtility.string2Int(value);
                break;
            case "mstan":
                this.mstan = IntegerUtility.string2Int(value);
                break;
            case "hstan":
                this.hstan = IntegerUtility.string2Int(value);
                break;
            case "gstan":
                this.gstan = IntegerUtility.string2Int(value);
                break;
            case "goli":
                this.goli = IntegerUtility.string2Int(value);
                break;
            case "l":
                this.l = IntegerUtility.string2Int(value);
                break;
            case "a":
                this.a = IntegerUtility.string2Int(value);
                break;
            case "ao":
                this.ao = IntegerUtility.string2Int(value);
                break;
            case "cx":
                this.cx = IntegerUtility.string2Int(value);
                break;
            case "rek_h":
                this.rek_h = IntegerUtility.string2Int(value);
                break;
            case "gols":
                this.gols = IntegerUtility.string2Int(value);
                break;
            case "rcs":
                this.rcs = IntegerUtility.string2Int(value);
                break;
            case "info":
                this.info = value;
                break;
            case "date":
                this.date = value;
                break;
            case "showDate":
                this.showDate = value;
                break;
            case "m":
                this.m = value;
                break;
            case "lnPk":
                this.lnPk = value;
                break;
            case "cnPk":
                this.cnPk = value;
                break;
            case "event_id":
                this.event_id = value;
                break;
            case "otv":
                this.otv = value;
                break;
            case "grc":
                this.grc = value;
                break;
            case "hrc":
                this.hrc = value;
                break;
            case "s1":
                this.s1 = value;
                break;
            case "s2":
                this.s2 = value;
                break;
        }

    }

    public void setValueByKey(HashMap<String, String> map) {
        for (Map.Entry<String, String> attr : map.entrySet()) {
            this.setValueByKey(attr.getKey(), attr.getValue());
        }
    }

    public void setUPdateValueByKey(String key, String value) {

        switch (key) {
            case "zid":
                this.zid = IntegerUtility.string2Int(value);
                break;
            case "mid":
                this.mid = IntegerUtility.string2Int(value);
                break;
            case "x":
                this.x = IntegerUtility.string2Int(value);
                break;
            case "lid":
                this.lid = IntegerUtility.string2Int(value);
                this.slid = IntegerUtility.string2Int(value);
                break;
            case "slid":
                this.slid = IntegerUtility.string2Int(value);
                break;
            case "_lid":
                this._lid = IntegerUtility.string2Int(value);                
                break;
            case "oid":
                this.oid = IntegerUtility.string2Int(value);
                break;
            case "sid":
                this.sid = IntegerUtility.string2Int(value);
                break;
            case "kid":
                this.kid = IntegerUtility.string2Int(value);
                break;
            case "lnr":
                this.lnr = IntegerUtility.string2Int(value);
                break;
            case "c0":
                this.c0 = IntegerUtility.string2Int(value);
                break;
            case "c1":
                this.c1 = IntegerUtility.string2Int(value);
                break;
            case "c2":
                this.c2 = IntegerUtility.string2Int(value);
                break;
            case "ml":
                this.ml = IntegerUtility.string2Int(value);
                break;
            case "hid":
                this.hid = IntegerUtility.string2Int(value);
                break;
            case "gid":
                this.gid = IntegerUtility.string2Int(value);
                break;
            case "hn":
                this.hn = value;
                break;
            case "gn":
                this.gn = value;
                break;
            case "hrci":
                this.hrci = IntegerUtility.string2Int(value);
                break;
            case "grci":
                this.grci = IntegerUtility.string2Int(value);
                break;
            case "mstan":
                this.mstan = IntegerUtility.string2Int(value);
                break;
            case "hstan":
                this.hstan = IntegerUtility.string2Int(value);
                break;
            case "gstan":
                this.gstan = IntegerUtility.string2Int(value);
                break;
            case "goli":
                this.goli = IntegerUtility.string2Int(value);
                break;
            case "l":
                this.l = IntegerUtility.string2Int(value);
                break;
            case "a":
                this.a = IntegerUtility.string2Int(value);
                break;
            case "ao":
                this.ao = IntegerUtility.string2Int(value);
                break;
            case "cx":
                this.cx = ("".equals(value)) ? 0 : IntegerUtility.string2Int(value);
                break;
            case "rek_h":
                this.rek_h = ("".equals(value)) ? -1 : IntegerUtility.string2Int(value);
                break;
            case "gols":
                this.gols = IntegerUtility.string2Int(value);
                break;
            case "rcs":
                this.rcs = IntegerUtility.string2Int(value);
                break;
            case "info":
                this.info = value;
                break;
            case "date":
                this.date = value;
                break;
            case "showDate":
                this.showDate = value;
                break;
            case "m":
                this.m = value;
                break;
            case "lnPk":
                this.lnPk = value;
                break;
            case "cnPk":
                this.cnPk = value;
                break;
            case "event_id":
                this.event_id = value;
                break;
            case "otv":
                this.otv = value;
                break;
            case "grc":
                this.grc = value;
                break;
            case "hrc":
                this.hrc = value;
                break;
            case "s1":
                this.s1 = value;
                break;
            case "s2":
                this.s2 = value;
                break;
        }

    }

    public void setUpdatedValueByKey(HashMap<String, String> map) {

        this.ml = -1;
        this.mstan = -1;
        this.cx = -1;
        this.x = -1;
        this.l = -1;
        this.ao = -1;
        this.gstan = -1;
        this.goli = -1;
        this.a = -1;
        this.gols = -1;
        this.hstan = -1;
        this.rek_h = -1;
        this.c0 = -1;
        this.c1 = -1;
        this.c2 = -1;
        this.hrci = -1;
        this.grci = -1;
        this.gid = -1;
        this.hid = -1;
        this.lnr = -1;

        for (Map.Entry<String, String> attr : map.entrySet()) {
            this.setValueByKey(attr.getKey(), attr.getValue());
        }
    }

    public String getSqlInsertValue() {

        String query = "REPLACE INTO live_match ("
                + "mid,"
                + "lid,"
                + "_lid,"
                + "oid,"
                + "sid,"
                + "kid,"
                + "lnr,"
                + "c0,"
                + "c1,"
                + "c2,"
                + "ml,"
                + "hid,"
                + "gid,"
                + "hn,"
                + "gn,"
                + "hrci,"
                + "grci,"
                + "grc,"
                + "hrc,"
                + "s1,"
                + "s2,"
                + "mstan,"
                + "otv,"
                + "hstan,"
                + "gstan,"
                + "golI,"
                + "l,"
                + "a,"
                + "ao,"
                + "cx,"
                + "info,"
                + "rek_h,"
                + "date,"
                + "showDate,"
                + "hide,"
                + "m,"
                + "new) VALUES ";
        String value = "(";
        value += "'" + this.mid + "'";
        value += ",'" + this.lid + "'";
        value += ",'" + this._lid + "'";
        value += ",'" + this.oid + "'";
        value += ",'" + this.sid + "'";
        value += ",'" + this.kid + "'";
        value += ",'" + this.lnr + "'";
        value += ",'" + this.c0 + "'";
        value += ",'" + this.c1 + "'";
        value += ",'" + this.c2 + "'";
        value += ",'" + this.ml + "'";
        value += ",'" + this.hid + "'";
        value += ",'" + this.gid + "'";
        value += ",'" + this.hn + "'";
        value += ",'" + this.gn + "'";
        value += ",'" + this.hrci + "'";
        value += ",'" + this.grci + "'";
        value += ",'" + this.grc + "'";
        value += ",'" + this.hrc + "'";
        value += ",'" + this.s1 + "'";
        value += ",'" + this.s2 + "'";
        value += ",'" + this.mstan + "'";
        value += ",'" + this.otv + "'";
        value += ",'" + this.hstan + "'";
        value += ",'" + this.gstan + "'";
        value += ",'" + this.goli + "'";
        value += ",'" + this.l + "'";
        value += ",'" + this.a + "'";
        value += ",'" + this.ao + "'";
        value += ",'" + this.cx + "'";
        value += ",'" + this.info + "'";
        value += ",'" + this.rek_h + "'";
        value += ",'" + this.date + "'";
        value += ",'" + this.showDate + "'";
        value += ",'N'";
        value += ",''";
        value += ",'Y'";
        value += ")";
        return query += value;

    }

    public PreparedStatement getSqlInsertValue(Connection conn) {

        PreparedStatement stm = null;
        String query = "REPLACE INTO live_match ("
                + "mid,"
                + "lid,"
                + "_lid,"
                + "oid,"
                + "sid,"
                + "kid,"
                + "lnr,"
                + "c0,"
                + "c1,"
                + "c2,"
                + "ml,"
                + "hid,"
                + "gid,"
                + "hn,"
                + "gn,"
                + "hrci,"
                + "grci,"
                + "grc,"
                + "hrc,"
                + "s1,"
                + "s2,"
                + "mstan,"
                + "otv,"
                + "hstan,"
                + "gstan,"
                + "golI,"
                + "l,"
                + "a,"
                + "ao,"
                + "cx,"
                + "info,"
                + "rek_h,"
                + "date,"
                + "showDate,"
                + "hide,"
                + "m,"
                + "new) VALUES ";
        query += "(";
        query += "?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ",?";
        query += ")";
        try {
            stm = conn.prepareStatement(query);
            stm.setInt(1, this.mid);
            stm.setInt(2, this.lid);
            stm.setInt(3, this._lid);
            stm.setInt(4, this.oid);
            stm.setInt(5, this.sid);
            stm.setInt(6, this.kid);
            stm.setInt(7, this.lnr);
            stm.setLong(8, this.c0);
            stm.setLong(9, this.c1);
            stm.setLong(10, this.c2);
            stm.setInt(11, this.ml);
            stm.setInt(12, this.hid);
            stm.setInt(13, this.gid);
            stm.setString(14, this.hn);
            stm.setString(15, this.gn);
            stm.setInt(16, this.hrci);
            stm.setInt(17, this.grci);
            stm.setString(18, this.grc);
            stm.setString(19, this.hrc);
            stm.setString(20, this.s1);
            stm.setString(21, this.s2);
            stm.setInt(22, this.mstan);
            stm.setString(23, this.otv);
            stm.setInt(24, this.hstan);
            stm.setInt(25, this.gstan);
            stm.setInt(26, this.goli);
            stm.setInt(27, this.l);
            stm.setInt(28, this.a);
            stm.setInt(29, this.ao);
            stm.setLong(30, this.cx);
            stm.setString(31, this.info);
            stm.setInt(32, this.rek_h);
            stm.setString(33, this.date);
            stm.setString(34, this.showDate);
            stm.setString(35, "N");
            stm.setString(36, "");
            stm.setString(37, "Y");
        } catch (SQLException ex) {
            Logger.getLogger(LiveMatchM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stm;

    }

    public int getZid() {
        return zid;
    }

    public void setZid(int zid) {
        this.zid = zid;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getLid() {
        return lid;
    }

    public void setLid(int lid) {
        this.lid = lid;
    }

    public int getSlid() {
        return slid;
    }

    public void setSlid(int slid) {
        this.lid = slid;
    }

    public int get_lid() {
        return _lid;
    }

    public void set_lid(int slid) {
        this._lid = slid;
    }

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getKid() {
        return kid;
    }

    public void setKid(int kid) {
        this.kid = kid;
    }

    public int getLnr() {
        return lnr;
    }

    public void setLnr(int lnr) {
        this.lnr = lnr;
    }

    public long getC0() {
        return c0;
    }

    public void setC0(int c0) {
        this.c0 = c0;
    }

    public long getC1() {
        return c1;
    }

    public void setC1(int c1) {
        this.c1 = c1;
    }

    public long getC2() {
        return c2;
    }

    public void setC2(int c2) {
        this.c2 = c2;
    }

    public int getMl() {
        return ml;
    }

    public void setMl(int ml) {
        this.ml = ml;
    }

    public int getHid() {
        return hid;
    }

    public void setHid(int hid) {
        this.hid = hid;
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public String getHn() {
        return hn;
    }

    public void setHn(String hn) {
        this.hn = hn;
    }

    public String getGn() {
        return gn;
    }

    public void setGn(String gn) {
        this.gn = gn;
    }

    public int getHrci() {
        return hrci;
    }

    public void setHrci(int hrci) {
        this.hrci = hrci;
    }

    public int getGrci() {
        return grci;
    }

    public void setGrci(int grci) {
        this.grci = grci;
    }

    public String getGrc() {
        return grc;
    }

    public void setGrc(String grc) {
        this.grc = grc;
    }

    public String getHrc() {
        return hrc;
    }

    public void setHrc(String hrc) {
        this.hrc = hrc;
    }

    public String getS1() {
        return s1;
    }

    public void setS1(String s1) {
        this.s1 = s1;
    }

    public String getS2() {
        return s2;
    }

    public void setS2(String s2) {
        this.s2 = s2;
    }

    public int getMstan() {
        return mstan;
    }

    public void setMstan(int mstan) {
        this.mstan = mstan;
    }

    public String getOtv() {
        return otv;
    }

    public void setOtv(String otv) {
        this.otv = otv;
    }

    public int getHstan() {
        return hstan;
    }

    public void setHstan(int hstan) {
        this.hstan = hstan;
    }

    public int getGstan() {
        return gstan;
    }

    public void setGstan(int gstan) {
        this.gstan = gstan;
    }

    public int getGoli() {
        return goli;
    }

    public void setGoli(int goli) {
        this.goli = goli;
    }

    public int getL() {
        return l;
    }

    public void setL(int l) {
        this.l = l;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getAo() {
        return ao;
    }

    public void setAo(int ao) {
        this.ao = ao;
    }

    public int getCx() {
        return cx;
    }

    public void setCx(int cx) {
        this.cx = cx;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getRek_h() {
        return rek_h;
    }

    public void setRek_h(int rek_h) {
        this.rek_h = rek_h;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getGols() {
        return gols;
    }

    public void setGols(int gols) {
        this.gols = gols;
    }

    public String getShowDate() {
        return showDate;
    }

    public void setShowDate(String showDate) {
        this.showDate = showDate;
    }

    public int getRcs() {
        return rcs;
    }

    public void setRcs(int rcs) {
        this.rcs = rcs;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public String getLnPk() {
        return lnPk;
    }

    public void setLnPk(String lnPk) {
        this.lnPk = lnPk;
    }

    public String getCnPk() {
        return cnPk;
    }

    public void setCnPk(String cnPk) {
        this.cnPk = cnPk;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

}
