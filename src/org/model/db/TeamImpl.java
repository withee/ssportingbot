/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.manager.FileManager;
import org.manager.LogManager;
import org.model.StatTableM;
import org.model.TeamM;

/**
 *
 * @author mrsyrop
 */
public class TeamImpl extends DBConnection implements TeamDAO {

    private String table = "team";
    private String nation[] = {"Th", "En"};
    private LogManager log = new LogManager();

    @Override
    public TeamM getBytid(int tid) {
        TeamM teamM = new TeamM();

        this.connectDB();
        String query = "select team.tid,team.tn,team.tnPk,team.cid,lang_team.teamNameTh as tnTh,lang_team.teamNameEn as tnEn,lang_team.teamNameBig as tnBig,lang_team.teamNameGb as tnGb,lang_team.teamNameVn as tnVn,lang_team.teamNameKr as tnKr,lang_team.teamNameLa as tnLa, "
                + "lang_competition.comNameTh,lang_competition.comNameEn,lang_competition.comNameBig,lang_competition.comNameGb,lang_competition.comNameKr,lang_competition.comNameLa,lang_competition.comNameVn,"
                + "competitions.competitionName  as comName,competitions.competitionType as comType,team.comPk as comPk "
                + "from team "
                + " left join lang_team on lang_team.tid = team.tid"
                + " left join competitions on competitions.competitionId = team.cid"
                + " left join lang_competition on lang_competition.cid = team.cid"
                + " where team.tid=" + tid + "";
        //System.out.println(query);
        try {
            this.rs = stmt.executeQuery(query);
            while (this.rs.next()) {

                teamM.setCid(this.rs.getInt("cid"));
                teamM.setTid(this.rs.getInt("tid"));
                //System.out.println(this.rs.getString("tnTh"));
                teamM.setTn(this.rs.getString("tn"));
                teamM.setTnPk(this.rs.getString("tnPk"));
                teamM.setTnTh(this.rs.getString("tnTh"));
                teamM.setTnBig(this.rs.getString("tnBig"));
                teamM.setTnEn(this.rs.getString("tnEn"));
                teamM.setTnGb(this.rs.getString("tnGb"));
                teamM.setTnKr(this.rs.getString("tnKr"));
                teamM.setTnLa(this.rs.getString("tnLa"));
                teamM.setTnVn(this.rs.getString("tnVn"));

                teamM.setComPk(this.rs.getString("comPk"));
                teamM.setComType(this.rs.getString("comType"));
                teamM.setComName(this.rs.getString("comName"));
                teamM.setComNameTh(this.rs.getString("comNameTh"));
                teamM.setComNameEn(this.rs.getString("comNameEn"));
                teamM.setComNameBig(this.rs.getString("comNameBig"));
                teamM.setComNameGb(this.rs.getString("comNameGb"));
                teamM.setComNameKr(this.rs.getString("comNameKr"));
                teamM.setComNameLa(this.rs.getString("comNameLa"));
                teamM.setComNameVn(this.rs.getString("comNameVn"));

            }

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            this.closeDB();
        }

        return teamM;
    }

    @Override
    public boolean addSingle(TeamM t) {
        boolean returnValue = false;
        boolean value = false;
        String query2 = null;
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:ii:ss");

        this.connectDB();
        try {
            //
//            connectDB();
            String query = "delete from team where tid='" + t.getTid() + "'";
            if (this.stmt.executeUpdate(query) != 0) {
                System.out.println("deleted " + t.getTid() + ":" + t.getTn());
            }

            query2 = "insert into team(tid,tn,tnPk,tnTh,cid,comPk,comType) value(" + t.getTid() + ",\"" + t.getTn() + "\",\"" + t.getTnPk() + "\",\"" + t.getTnTh() + "\"," + t.getCid() + ",\"" + t.getComPk() + "\",\"" + t.getComType() + "\"" + ")";
            //System.out.println(query2);
            int i = this.stmt.executeUpdate(query2);
            if (i != 0) {
                returnValue = true;
                //this.stmt.execute("DELETE FROM `team_logos` WHERE tid=" + t.getTid());
                this.rs = this.stmt.executeQuery("SELECT * FROM `team_logos` WHERE `tid`=" + t.getTid());
                if (!this.rs.next()) {
                    query = "INSERT INTO `team_logos` (`tid`, `32x32`, `64x64`, `256x256`, `update_at`) VALUES (" + t.getTid() + ", 'http://api.ssporting.com/teams_clean/team_default_32x32.png', 'http://api.ssporting.com/teams_clean/team_default_64x64.png', 'http://api.ssporting.com/teams_clean/team_default_256x256.png', '" + ft.format(dNow).toString() + "');";
                    if (this.stmt.execute(query)) {
                        log.write("info", t.getTnEn() + " add new team logo.");
                    } else {
                        log.write("info", t.getTnEn() + " add new team logo fail.");
                    }

                } else {
                    log.write("info", t.getTnEn() + " already has team logo.");
                }
            }

        } catch (Exception ex) {
            //closeDB();
            System.out.println(query2);
            ex.printStackTrace();
        } finally {
            this.closeDB();
        }
        return returnValue;
    }

    @Override
    public void writeFile(TeamM h, TeamM g, int mid) {
        JSONObject json = new JSONObject();
        JSONObject json2 = new JSONObject();
        JSONObject jsonTh = new JSONObject();
        JSONObject jsonTh2 = new JSONObject();
        JSONObject obj = new JSONObject();
        JSONObject objTh;
        JSONObject team1 = new JSONObject();
        JSONObject team2 = new JSONObject();
        JSONObject teamTh1 = new JSONObject();
        JSONObject teamTh2 = new JSONObject();
        JSONObject[] teamObjH = new JSONObject[nation.length];
        JSONObject[] teamObjG = new JSONObject[nation.length];
        //{"Th", "Big", "Gb", "Kr", "Vn", "La", "En"};
        String tidlist = "0";
        String comArrayH[] = {h.getComNameTh(), h.getComNameEn()};
        String teamArrayH[] = {h.getTnTh(), h.getTnEn()};
        String comArrayG[] = {g.getComNameTh(), g.getComNameEn()};
        String teamArrayG[] = {g.getTnTh(), g.getTnEn()};

        for (int i = 0; i < nation.length; i++) {
            JSONObject teamObj = new JSONObject();
            teamObj.put("tid", h.getTid());
            teamObj.put("tn", h.getTn());
            teamObj.put("tnPk", h.getTnPk());
            teamObj.put("cid", h.getCid());
            teamObj.put("comPk", h.getComPk());
            teamObj.put("comType", h.getComType());
            teamObj.put("comName", h.getComName());
            if (comArrayH[i] != null) {
                teamObj.put("comName", comArrayH[i]);
            }
            if (teamArrayH[i] != null) {
                teamObj.put("tn", teamArrayH[i]);
            }
            teamObjH[i] = teamObj;
        }
        for (int i = 0; i < nation.length; i++) {
            JSONObject teamObj = new JSONObject();
            teamObj.put("tid", g.getTid());
            teamObj.put("tn", g.getTn());
            teamObj.put("tnPk", g.getTnPk());
            teamObj.put("cid", g.getCid());
            teamObj.put("comPk", g.getComPk());
            teamObj.put("comType", g.getComType());
            teamObj.put("comName", g.getComName());
            if (comArrayG[i] != null) {
                teamObj.put("comName", comArrayG[i]);
            }
            if (teamArrayG[i] != null) {
                teamObj.put("tn", teamArrayG[i]);
            }
            teamObjG[i] = teamObj;
        }

        JSONObject jsonArray[] = new JSONObject[nation.length];
        JSONObject jsonArray2[] = new JSONObject[nation.length];
        JSONObject teamArray1[] = new JSONObject[nation.length];
        JSONObject teamArray2[] = new JSONObject[nation.length];
        for (int i = 0; i < nation.length; i++) {
            jsonArray[i] = new JSONObject();
            jsonArray[i].put("team1", teamObjH[i]);
            jsonArray[i].put("team2", teamObjG[i]);
            jsonArray2[i] = new JSONObject();
            jsonArray2[i].put("team2", teamObjH[i]);
            jsonArray2[i].put("team1", teamObjG[i]);
            teamArray1[i] = new JSONObject();
            teamArray1[i].put("team", teamObjH[i]);
            teamArray2[i] = new JSONObject();
            teamArray2[i].put("team", teamObjG[i]);
        }
        String result_vs_query = "select result_vs.*,"
                + "team1.tn as teamName1,t1.teamNameTh as teamNameTh1,t1.teamNameEn as teamNameEn1,"
                + "team2.tn as teamName2,t2.teamNameTh as teamNameTh2,t2.teamNameEn as teamNameEn2,"
                + "team1.tid as tid1,team2.tid as tid2 from result_vs "
                + " left join team as team1 on result_vs.tnPk1 = team1.tnPk"
                + " left join team as team2 on result_vs.tnPk2 = team2.tnPk"
                + " left join lang_team as t1 on team1.tid=t1.tid "
                + " left join lang_team as t2 on team2.tid=t2.tid "
                + "where (result_vs.tnPk1=\"" + h.getTnPk() + "\" and result_vs.tnPk2=\"" + g.getTnPk() + "\" ) or (result_vs.tnPk1=\"" + g.getTnPk() + "\" and result_vs.tnPk2=\"" + h.getTnPk() + "\" )  group by result_vs.date\n "
                + " order by result_vs.date DESC limit 5 ";
        //System.out.println(result_vs_query);
        //System.exit(0);
        String result1_query = "select result.*,league.leagueId,"
                + "t1.teamNameTh as teamNameTh1,t1.teamNameEn as teamNameEn1,"
                + "t2.teamNameTh as teamNameTh2,t2.teamNameEn as teamNameEn2,"
                + "team1.tid as tid1,team2.tid as tid2 from result "
                + "left join competitions on competitions.competitionNamePk = result.competitionNamePk "
                + "left join league on league.leagueNamePk = result.leagueNamePk and league.competitionId = competitions.competitionId "
                + " left join team as team1 on team1.tnPk =result.teamHomeNamePk"
                + " left join team as team2 on team2.tnPk = result.teamAwayNamepk"
                + " left join lang_team as t1 on t1.tid =team1.tid "
                + " left join lang_team as t2 on t2.tid =team2.tid "
                + "where result.teamNamePk=\"" + h.getTnPk() + "\" and result.r!='0'  group by result.date order by result.date DESC limit 10";
        //System.out.println(result1_query);
        String result2_query = "select result.*,league.leagueId ,"
                + "t1.teamNameTh as teamNameTh1,t1.teamNameEn as teamNameEn1,"
                + "t2.teamNameTh as teamNameTh2,t2.teamNameEn as teamNameEn2,"
                + "team1.tid as tid1,team2.tid as tid2 from result "
                + "left join competitions on competitions.competitionNamePk = result.competitionNamePk "
                + "left join league on league.leagueNamePk = result.leagueNamePk and league.competitionId = competitions.competitionId "
                + " left join team as team1 on team1.tnPk =result.teamHomeNamePk"
                + " left join team as team2 on team2.tnPk = result.teamAwayNamepk"
                + " left join lang_team as t1 on t1.tid =team1.tid "
                + " left join lang_team as t2 on t2.tid =team2.tid "
                + "where result.teamNamePk=\"" + g.getTnPk() + "\" and result.r!='0' group by result.date order by result.date DESC limit 10";
        //System.out.println(result2_query);
        String stat_table_1 = "select *from stat_table where tnPk=\"" + h.getTnPk() + "\" group by leagueId";
        String stat_table_2 = "select *from stat_table where tnPk=\"" + g.getTnPk() + "\" group by leagueId";
        connectDB();
        try {
            setRs(getStmt().executeQuery(result_vs_query));
            JSONArray resultVsArray[] = new JSONArray[nation.length];
            for (int i = 0; i < nation.length; i++) {
                resultVsArray[i] = new JSONArray();
            }
            while (getRs().next()) {

                obj = new JSONObject();
                obj.put("tnPk1", getRs().getString("tnPk1"));
                obj.put("tnPk2", getRs().getString("tnPk2"));
                obj.put("cnPk", getRs().getString("cnPk"));
                obj.put("date", getRs().getString("date"));
                obj.put("ct", getRs().getString("ct"));
                obj.put("lnPk", getRs().getString("lnPk"));
                obj.put("season", getRs().getString("season"));
                obj.put("lnr", getRs().getString("lnr"));
                obj.put("matchDate", getRs().getString("matchDate"));
//                obj.put("hmn", rs.getString("hmn"));
//                obj.put("gmn", rs.getString("gmn"));
                obj.put("score", getRs().getString("score"));
                obj.put("sl", getRs().getString("sl"));
                obj.put("tid1", getRs().getString("tid1") + "");
                obj.put("tid2", getRs().getString("tid2") + "");
                tidlist += "," + getRs().getString("tid1");
                tidlist += "," + getRs().getString("tid2");
                for (int i = 0; i < nation.length; i++) {
                    obj.put("hmn", getRs().getString("hmn"));
                    obj.put("gmn", getRs().getString("gmn"));
//                    /System.out.println(obj.get("hmn")+":"+obj.get("gmn"));
                    if (getRs().getString("teamName" + nation[i] + "1") != null) {
                        obj.put("hmn", getRs().getString("teamName" + nation[i] + "1"));
                    } else if (getRs().getString("teamName1") != null) {
                        obj.put("hmn", getRs().getString("teamName1"));
                    }
                    if (getRs().getString("teamName" + nation[i] + "2") != null) {
                        obj.put("gmn", getRs().getString("teamName" + nation[i] + "2"));
                    } else if (getRs().getString("teamName2") != null) {
                        obj.put("gmn", getRs().getString("teamName2"));
                    }
                    resultVsArray[i].add(obj);
                }
            }
            for (int i = 0; i < nation.length; i++) {
                jsonArray[i].put("result_vs", resultVsArray[i]);
                jsonArray2[i].put("result_vs", resultVsArray[i]);
            }
            setRs(getStmt().executeQuery(result1_query));
            JSONArray result1[] = new JSONArray[nation.length];
            for (int i = 0; i < nation.length; i++) {
                result1[i] = new JSONArray();
            }
            while (getRs().next()) {
                obj = new JSONObject();
                obj.put("date", getRs().getString("date"));
                obj.put("teamHomeNamePk", getRs().getString("teamHomeNamePk"));
                obj.put("teamAwayNamePk", getRs().getString("teamAwayNamePk"));
                obj.put("competitionType", getRs().getString("competitionType"));
                obj.put("competitionNamePk", getRs().getString("competitionNamePk"));
                obj.put("leagueSeason", getRs().getString("leagueSeason"));
                obj.put("subLeagueNamePk", getRs().getString("subLeagueNamePk"));
                obj.put("leaugeNamePk", getRs().getString("leagueNamePk"));
                obj.put("hcomPk", getRs().getString("hcomPk"));
                obj.put("acomPk", getRs().getString("acomPk"));
                obj.put("r", getRs().getString("r"));
                obj.put("score", getRs().getString("score"));
                obj.put("leagueName", getRs().getString("leagueName"));
                obj.put("lnk", getRs().getString("lnk"));
                obj.put("teamNamePk", getRs().getString("teamNamePk"));
                obj.put("leagueId", getRs().getString("leagueId"));
                obj.put("tid1", getRs().getString("tid1") + "");
                obj.put("tid2", getRs().getString("tid2") + "");
                tidlist += "," + getRs().getString("tid1");
                tidlist += "," + getRs().getString("tid2");
                for (int i = 0; i < nation.length; i++) {
                    obj.put("hn", getRs().getString("hn"));
                    obj.put("an", getRs().getString("an"));
                    if (getRs().getString("teamName" + nation[i] + "1") != null) {
                        obj.put("hn", getRs().getString("teamName" + nation[i] + "1"));

                    }
                    if (getRs().getString("teamName" + nation[i] + "2") != null) {
                        obj.put("an", getRs().getString("teamName" + nation[i] + "2"));

                    }
                    result1[i].add(obj);
                }

            }
            for (int i = 0; i < nation.length; i++) {
                jsonArray[i].put("result1", result1[i]);
                jsonArray2[i].put("result2", result1[i]);
                teamArray1[i].put("result", result1[i]);
            }

            setRs(getStmt().executeQuery(result2_query));
            JSONArray result2[] = new JSONArray[nation.length];
            for (int i = 0; i < nation.length; i++) {
                result2[i] = new JSONArray();
            }
            while (getRs().next()) {
                obj = new JSONObject();
                obj.put("date", getRs().getString("date"));
                obj.put("teamHomeNamePk", getRs().getString("teamHomeNamePk"));
                obj.put("teamAwayNamePk", getRs().getString("teamAwayNamePk"));
                obj.put("competitionType", getRs().getString("competitionType"));
                obj.put("competitionNamePk", getRs().getString("competitionNamePk"));
                obj.put("leagueSeason", getRs().getString("leagueSeason"));
                obj.put("subLeagueNamePk", getRs().getString("subLeagueNamePk"));
                obj.put("leaugeNamePk", getRs().getString("leagueNamePk"));
                obj.put("hcomPk", getRs().getString("hcomPk"));
                obj.put("acomPk", getRs().getString("acomPk"));
                obj.put("r", getRs().getString("r"));
                obj.put("score", getRs().getString("score"));
                obj.put("leagueName", getRs().getString("leagueName"));
                obj.put("lnk", getRs().getString("lnk"));
                obj.put("teamNamePk", getRs().getString("teamNamePk"));
                obj.put("leagueId", getRs().getString("leagueId"));
                obj.put("tid1", getRs().getString("tid1") + "");
                obj.put("tid2", getRs().getString("tid2") + "");
                tidlist += "," + getRs().getString("tid1");
                tidlist += "," + getRs().getString("tid2");
                for (int i = 0; i < nation.length; i++) {
                    obj.put("hn", getRs().getString("hn"));
                    obj.put("an", getRs().getString("an"));
                    if (getRs().getString("teamName" + nation[i] + "1") != null) {
                        obj.put("hn", getRs().getString("teamName" + nation[i] + "1"));

                    }
                    if (getRs().getString("teamName" + nation[i] + "2") != null) {
                        obj.put("an", getRs().getString("teamName" + nation[i] + "2"));

                    }
                    result2[i].add(obj);
                }

            }

            for (int i = 0; i < nation.length; i++) {
                jsonArray[i].put("result2", result2[i]);
                jsonArray2[i].put("result1", result2[i]);
                teamArray2[i].put("result", result2[i]);
            }
            setRs(getStmt().executeQuery(stat_table_1));

            Vector<StatTableM> st = new Vector<StatTableM>();
            while (getRs().next()) {
                StatTableM s = new StatTableM();
                s.setLeagueId(getRs().getInt("leagueId"));
                s.setSubLeagueId(getRs().getString("subLeagueId"));
                st.add(s);
            }
            JSONArray statTableListArray[] = new JSONArray[nation.length];
            for (int i = 0; i < nation.length; i++) {
                statTableListArray[i] = new JSONArray();
            }
            for (StatTableM s : st) {
                JSONObject leagueInfoArray[] = new JSONObject[nation.length];

                String sql = "select stat_table.*,team.tid,"
                        + "t.teamNameTh,t.teamNameEn,"
                        + "team.tn"
                        + " from stat_table "
                        + "            left join team on (team.tnPk=stat_table.tnPk) "
                        + "            left join lang_team t on t.tid = team.tid "
                        + "            where stat_table.leagueId=" + s.getLeagueId()
                        + "            group by stat_table.tnPk order by stat_table.no ASC";
                setRs(getStmt().executeQuery(sql));
                JSONArray leagueListArray[] = new JSONArray[nation.length];
                for (int i = 0; i < nation.length; i++) {
                    leagueListArray[i] = new JSONArray();
                }
                while (getRs().next()) {
                    JSONObject stObj = new JSONObject();
                    stObj.put("tnPk", getRs().getString("tnPk"));
                    stObj.put("cid", getRs().getString("cid"));
                    stObj.put("leagueId", s.getLeagueId() + "");
                    stObj.put("subLeagueNamePk", getRs().getString("subLeagueNamePk"));
                    stObj.put("no", getRs().getString("no"));
                    stObj.put("no_h", getRs().getString("no_h"));
                    stObj.put("no_g", getRs().getString("no_g"));
                    stObj.put("gp", getRs().getString("gp"));
                    stObj.put("gp_h", getRs().getString("gp_h"));
                    stObj.put("gp_g", getRs().getString("gp_g"));
                    stObj.put("pts", getRs().getString("pts"));
                    stObj.put("pts_h", getRs().getString("pts_h"));
                    stObj.put("pts_g", getRs().getString("pts_g"));
                    stObj.put("w", getRs().getString("w"));
                    stObj.put("w_h", getRs().getString("w_h"));
                    stObj.put("w_g", getRs().getString("w_g"));
                    stObj.put("d", getRs().getString("d"));
                    stObj.put("d_h", getRs().getString("d_h"));
                    stObj.put("d_g", getRs().getString("d_g"));
                    stObj.put("l", getRs().getString("l"));
                    stObj.put("l_h", getRs().getString("l_h"));
                    stObj.put("l_g", getRs().getString("l_g"));
                    stObj.put("gf", getRs().getString("gf"));
                    stObj.put("gf_h", getRs().getString("gf_h"));
                    stObj.put("gf_g", getRs().getString("gf_g"));
                    stObj.put("ga", getRs().getString("ga"));
                    stObj.put("ga_h", getRs().getString("ga_h"));
                    stObj.put("ga_g", getRs().getString("ga_g"));
                    stObj.put("plusminus", getRs().getString("plusminus"));
                    stObj.put("plusminus_h", getRs().getString("plusminus_h"));
                    stObj.put("plusminus_g", getRs().getString("plusminus_g"));
                    JSONArray ml = new JSONArray();
                    JSONArray ml_g = new JSONArray();
                    JSONArray ml_h = new JSONArray();
                    if (getRs().getString("ml") != null) {
                        ml = (JSONArray) JSONSerializer.toJSON(getRs().getString("ml"));
                    }
                    if (getRs().getString("ml_h") != null) {
                        ml_h = (JSONArray) JSONSerializer.toJSON(getRs().getString("ml_h"));
                    }
                    if (getRs().getString("ml_g") != null) {
                        ml_g = (JSONArray) JSONSerializer.toJSON(getRs().getString("ml_g"));
                    }

                    stObj.put("ml", ml);
                    stObj.put("ml_h", ml_h);
                    stObj.put("ml_g", ml_g);
                    stObj.put("subLeagueId", getRs().getString("subLeagueId"));
                    stObj.put("tid", getRs().getString("tid") + "");
                    tidlist += "," + getRs().getString("tid");
                    for (int i = 0; i < nation.length; i++) {
                        stObj.put("tn", getRs().getString("tn"));
                        if (getRs().getString("teamName" + nation[i]) != null) {
                            stObj.put("tn", getRs().getString("teamName" + nation[i]));
                        }
                        leagueListArray[i].add(stObj);
                    }
                }
                for (int i = 0; i < nation.length; i++) {
                    leagueInfoArray[i] = new JSONObject();
                    leagueInfoArray[i].put("list", leagueListArray[i]);

                }
                sql = "select league.*,"
                        + "lang_league.leagueNameTh,lang_league.leagueNameEn"
                        + " from league "
                        + " left join lang_league on lang_league.leagueId = league.leagueId"
                        + " where league.leagueId=" + s.getLeagueId();
                //System.out.println(sql);
                setRs(getStmt().executeQuery(sql));
                JSONObject league = new JSONObject();
                while (getRs().next()) {
                    league.put("leagueId", getRs().getString("leagueId"));
                    league.put("rid", getRs().getString("rid"));
                    league.put("competitionId", getRs().getString("competitionId"));
                    league.put("leagueNamePk", getRs().getString("leagueNamePk"));

                    //league.put("leagueNameTh", rs.getString("leagueNameTh"));
                    league.put("leagueSeason", getRs().getString("leagueSeason"));
                    league.put("priority", getRs().getString("priority"));
                    for (int i = 0; i < nation.length; i++) {
                        league.put("leagueName", getRs().getString("leagueName"));
                        if (getRs().getString("leagueName" + nation[i]) != null) {
                            league.put("leagueName", getRs().getString("leagueName" + nation[i]));
                        }
                        leagueInfoArray[i].put("league", league);
                    }

                }
                for (int i = 0; i < nation.length; i++) {
                    statTableListArray[i].add(leagueInfoArray[i]);
                }
            }
            for (int i = 0; i < nation.length; i++) {
                teamArray1[i].put("stat_table", statTableListArray[i]);
            }
            setRs(getStmt().executeQuery(stat_table_2));
            //System.out.println(stat_table_2);
            st = new Vector<StatTableM>();
            while (getRs().next()) {
                StatTableM s = new StatTableM();
                s.setLeagueId(getRs().getInt("leagueId"));
                s.setSubLeagueId(getRs().getString("subLeagueId"));
                st.add(s);
            }
            //System.out.println(st.size());
            statTableListArray = new JSONArray[nation.length];
            for (int i = 0; i < nation.length; i++) {
                statTableListArray[i] = new JSONArray();
            }
            for (StatTableM s : st) {
                JSONObject leagueInfoArray[] = new JSONObject[nation.length];

                String sql = "select stat_table.*,team.tid,"
                        + "t.teamNameTh,t.teamNameEn,"
                        + "team.tn"
                        + " from stat_table "
                        + "            left join team on (team.tnPk=stat_table.tnPk) "
                        + "            left join lang_team t on t.tid = team.tid "
                        + "            where stat_table.leagueId=" + s.getLeagueId()
                        + "            group by stat_table.tnPk order by stat_table.no ASC";
                setRs(getStmt().executeQuery(sql));
                JSONArray leagueListArray[] = new JSONArray[nation.length];
                for (int i = 0; i < nation.length; i++) {
                    leagueListArray[i] = new JSONArray();
                }
                while (getRs().next()) {
                    JSONObject stObj = new JSONObject();
                    stObj.put("tnPk", getRs().getString("tnPk"));
                    stObj.put("cid", getRs().getString("cid"));
                    stObj.put("leagueId", s.getLeagueId() + "");
                    stObj.put("subLeagueNamePk", getRs().getString("subLeagueNamePk"));
                    stObj.put("no", getRs().getString("no"));
                    stObj.put("no_h", getRs().getString("no_h"));
                    stObj.put("no_g", getRs().getString("no_g"));
                    stObj.put("gp", getRs().getString("gp"));
                    stObj.put("gp_h", getRs().getString("gp_h"));
                    stObj.put("gp_g", getRs().getString("gp_g"));
                    stObj.put("pts", getRs().getString("pts"));
                    stObj.put("pts_h", getRs().getString("pts_h"));
                    stObj.put("pts_g", getRs().getString("pts_g"));
                    stObj.put("w", getRs().getString("w"));
                    stObj.put("w_h", getRs().getString("w_h"));
                    stObj.put("w_g", getRs().getString("w_g"));
                    stObj.put("d", getRs().getString("d"));
                    stObj.put("d_h", getRs().getString("d_h"));
                    stObj.put("d_g", getRs().getString("d_g"));
                    stObj.put("l", getRs().getString("l"));
                    stObj.put("l_h", getRs().getString("l_h"));
                    stObj.put("l_g", getRs().getString("l_g"));
                    stObj.put("gf", getRs().getString("gf"));
                    stObj.put("gf_h", getRs().getString("gf_h"));
                    stObj.put("gf_g", getRs().getString("gf_g"));
                    stObj.put("ga", getRs().getString("ga"));
                    stObj.put("ga_h", getRs().getString("ga_h"));
                    stObj.put("ga_g", getRs().getString("ga_g"));
                    stObj.put("plusminus", getRs().getString("plusminus"));
                    stObj.put("plusminus_h", getRs().getString("plusminus_h"));
                    stObj.put("plusminus_g", getRs().getString("plusminus_g"));
                    JSONArray ml = new JSONArray();
                    JSONArray ml_g = new JSONArray();
                    JSONArray ml_h = new JSONArray();
                    if (getRs().getString("ml") != null) {
                        ml = (JSONArray) JSONSerializer.toJSON(getRs().getString("ml"));
                    }
                    if (getRs().getString("ml_h") != null) {
                        ml_h = (JSONArray) JSONSerializer.toJSON(getRs().getString("ml_h"));
                    }
                    if (getRs().getString("ml_g") != null) {
                        ml_g = (JSONArray) JSONSerializer.toJSON(getRs().getString("ml_g"));
                    }
                    stObj.put("ml", ml);
                    stObj.put("ml_h", ml_h);
                    stObj.put("ml_g", ml_g);
                    stObj.put("subLeagueId", getRs().getString("subLeagueId"));
                    stObj.put("tid", getRs().getString("tid") + "");
                    tidlist += "," + getRs().getString("tid");
                    for (int i = 0; i < nation.length; i++) {
                        stObj.put("tn", getRs().getString("tn"));
                        if (getRs().getString("teamName" + nation[i]) != null) {
                            stObj.put("tn", getRs().getString("teamName" + nation[i]));
                        }
                        leagueListArray[i].add(stObj);
                    }
                }
                for (int i = 0; i < nation.length; i++) {
                    leagueInfoArray[i] = new JSONObject();
                    leagueInfoArray[i].put("list", leagueListArray[i]);

                }
                sql = "select league.*,"
                        + "lang_league.leagueNameTh,lang_league.leagueNameEn"
                        + " from league "
                        + " left join lang_league on lang_league.leagueId = league.leagueId"
                        + " where league.leagueId=" + s.getLeagueId();
                setRs(getStmt().executeQuery(sql));
                JSONObject league = new JSONObject();
                while (getRs().next()) {
                    league.put("leagueId", getRs().getString("leagueId"));
                    league.put("rid", getRs().getString("rid"));
                    league.put("competitionId", getRs().getString("competitionId"));
                    league.put("leagueNamePk", getRs().getString("leagueNamePk"));

                    //league.put("leagueNameTh", rs.getString("leagueNameTh"));
                    league.put("leagueSeason", getRs().getString("leagueSeason"));
                    league.put("priority", getRs().getString("priority"));
                    for (int i = 0; i < nation.length; i++) {
                        league.put("leagueName", getRs().getString("leagueName"));
                        if (getRs().getString("leagueName" + nation[i]) != null) {
                            league.put("leagueName", getRs().getString("leagueName" + nation[i]));
                        }
                        leagueInfoArray[i].put("league", league);
                    }

                }
                for (int i = 0; i < nation.length; i++) {
                    statTableListArray[i].add(leagueInfoArray[i]);
                }
            }
            for (int i = 0; i < nation.length; i++) {
                teamArray2[i].put("stat_table", statTableListArray[i]);
            }
            String fixture1 = "select fixture.*,team1.tid as tid1,team2.tid as tid2,"
                    + "t1.teamNameTh as teamNameTh1,t1.teamNameEn as teamNameEn1,"
                    + "t2.teamNameTh as teamNameTh2,t2.teamNameEn as teamNameEn2 "
                    + " from fixture "
                    + " left join team as team1 on team1.tnPk=fixture.teamHomeNamePk"
                    + " left join team as team2 on team2.tnPk=fixture.teamAwayNamePk"
                    + " left join lang_team as t1 on t1.tid = team1.tid"
                    + " left join lang_team as t2 on t2.tid = team2.tid"
                    + " where teamNamePk=\"" + h.getTnPk() + "\" and date>=current_date group by date \n"
                    + " having  count(date)=1 order by date ASC limit 10";
            String fixture2 = "select fixture.*,"
                    + "team1.tid as tid1,team2.tid as tid2,"
                    + "t1.teamNameTh as teamNameTh1,t1.teamNameEn as teamNameEn1,"
                    + "t2.teamNameTh as teamNameTh2,t2.teamNameEn as teamNameEn2 "
                    + " from fixture "
                    + " left join team as team1 on team1.tnPk=fixture.teamHomeNamePk"
                    + " left join team as team2 on team2.tnPk=fixture.teamAwayNamePk"
                    + " left join lang_team as t1 on t1.tid = team1.tid"
                    + " left join lang_team as t2 on t2.tid = team2.tid"
                    + " where teamNamePk=\"" + g.getTnPk() + "\" and date>=current_date group by date \n"
                    + " having  count(date)=1 order by date ASC limit 10";
            setRs(getStmt().executeQuery(fixture1));
            //System.out.println(fixture1);
            JSONArray fixtureListArray[] = new JSONArray[nation.length];
            for (int i = 0; i < nation.length; i++) {
                fixtureListArray[i] = new JSONArray();
            }
            while (getRs().next()) {
                JSONObject fixture = new JSONObject();
                fixture.put("date", getRs().getString("date"));
                fixture.put("teamHomeNamePk", getRs().getString("teamHomeNamePk"));
                fixture.put("teamAwayNamePk", getRs().getString("teamAwayNamePk"));
                fixture.put("competitionType", getRs().getString("competitionType"));
                fixture.put("competitionNamePk", getRs().getString("competitionNamePk"));
                fixture.put("leagueSeason", getRs().getString("leagueSeason"));
                fixture.put("subLeagueNamePk", getRs().getString("subLeagueNamePk"));
                fixture.put("leagueNamepk", getRs().getString("leagueNamePk"));

                fixture.put("hcomPk", getRs().getString("hcomPk"));
                fixture.put("acomPk", getRs().getString("acomPk"));
                fixture.put("leagueName", getRs().getString("leagueName"));
                fixture.put("lnk", getRs().getString("lnk"));
                fixture.put("teamNamePk", getRs().getString("teamNamePk"));
                fixture.put("tid1", getRs().getString("tid1") + "");
                fixture.put("tid2", getRs().getString("tid2") + "");
                tidlist += "," + getRs().getString("tid1");
                tidlist += "," + getRs().getString("tid2");
//                fixture.put("teamNameTh1", rs.getString("teamNameTh1") + "");
//                fixture.put("teamNameTh2", rs.getString("teamNameTh2") + "");
                for (int i = 0; i < nation.length; i++) {
                    fixture.put("hn", getRs().getString("hn"));
                    fixture.put("an", getRs().getString("an"));
                    if (getRs().getString("teamName" + nation[i] + "1") != null) {
                        fixture.put("hn", getRs().getString("teamName" + nation[i] + "1"));
                    }
                    if (getRs().getString("teamName" + nation[i] + "2") != null) {
                        fixture.put("an", getRs().getString("teamName" + nation[i] + "2"));
                    }
                    fixtureListArray[i].add(fixture);
                }

            }
            for (int i = 0; i < nation.length; i++) {
                teamArray1[i].put("fixture", fixtureListArray[i]);

            }
            setRs(getStmt().executeQuery(fixture2));
            fixtureListArray = new JSONArray[nation.length];
            for (int i = 0; i < nation.length; i++) {
                fixtureListArray[i] = new JSONArray();
            }
            while (getRs().next()) {
                JSONObject fixture = new JSONObject();
                fixture.put("date", getRs().getString("date"));
                fixture.put("teamHomeNamePk", getRs().getString("teamHomeNamePk"));
                fixture.put("teamAwayNamePk", getRs().getString("teamAwayNamePk"));
                fixture.put("competitionType", getRs().getString("competitionType"));
                fixture.put("competitionNamePk", getRs().getString("competitionNamePk"));
                fixture.put("leagueSeason", getRs().getString("leagueSeason"));
                fixture.put("subLeagueNamePk", getRs().getString("subLeagueNamePk"));
                fixture.put("leagueNamepk", getRs().getString("leagueNamePk"));
                fixture.put("hn", getRs().getString("hn"));
                fixture.put("an", getRs().getString("an"));
                fixture.put("hcomPk", getRs().getString("hcomPk"));
                fixture.put("acomPk", getRs().getString("acomPk"));
                fixture.put("leagueName", getRs().getString("leagueName"));
                fixture.put("lnk", getRs().getString("lnk"));
                fixture.put("teamNamePk", getRs().getString("teamNamePk"));
                fixture.put("tid1", getRs().getString("tid1") + "");
                fixture.put("tid2", getRs().getString("tid2") + "");
                tidlist += "," + getRs().getString("tid1");
                tidlist += "," + getRs().getString("tid2");
                for (int i = 0; i < nation.length; i++) {
                    fixture.put("hn", getRs().getString("hn"));
                    fixture.put("an", getRs().getString("an"));
                    if (getRs().getString("teamName" + nation[i] + "1") != null) {
                        fixture.put("hn", getRs().getString("teamName" + nation[i] + "1"));
                    }
                    if (getRs().getString("teamName" + nation[i] + "2") != null) {
                        fixture.put("an", getRs().getString("teamName" + nation[i] + "2"));
                    }
                    fixtureListArray[i].add(fixture);
                }
            }
            for (int i = 0; i < nation.length; i++) {
                teamArray2[i].put("fixture", fixtureListArray[i]);
            }

            //System.out.println("TeamDb-tidlist:" + tidlist);
            JSONObject logos = new JSONObject();
            String logossql = "SELECT * FROM team_logos WHERE tid IN (" + tidlist + ") GROUP BY tid";
            setRs(getStmt().executeQuery(logossql));
            while (getRs().next()) {
                JSONObject obj1 = new JSONObject();
                obj1.put("id", getRs().getString("id"));
                obj1.put("tid", getRs().getString("tid"));
                obj1.put("32x32", getRs().getString("32x32"));
                obj1.put("64x64", getRs().getString("64x64"));
                obj1.put("256x256", getRs().getString("256x256"));
                obj1.put("update_at", getRs().getString("update_at"));
                logos.put(getRs().getString("tid"), obj1);
            }
            for (int i = 0; i < nation.length; i++) {
                teamArray2[i].put("Teamlogos", logos);
                teamArray1[i].put("Teamlogos", logos);
                jsonArray[i].put("Teamlogos", logos);
                jsonArray2[i].put("Teamlogos", logos);

                teamArray2[i].put("source", "file");
                teamArray1[i].put("source", "file");
                jsonArray[i].put("source", "file");
                jsonArray2[i].put("source", "file");
            }

            FileManager fileman = new FileManager();
            String path;
            String filename;
            for (int i = 0; i < nation.length; i++) {

                path = "../../compare_team/" + nation[i].toLowerCase();
                filename = h.getTid() + "_" + g.getTid() + ".json";
                fileman.setPath(path);
                fileman.setFilename(filename);
                fileman.setContent(jsonArray[i].toString());
                fileman.write();

                path = "../../compare_team/" + nation[i].toLowerCase();
                filename = g.getTid() + "_" + h.getTid() + ".json";
                fileman.setPath(path);
                fileman.setFilename(filename);
                fileman.setContent(jsonArray2[i].toString());
                fileman.write();

                path = "../../team_data/" + nation[i].toLowerCase();
                filename = h.getTid() + ".json";
                fileman.setPath(path);
                fileman.setFilename(filename);
                fileman.setContent(teamArray1[i].toString());
                fileman.write();

                path = "../../team_data/" + nation[i].toLowerCase();
                filename = g.getTid() + ".json";
                fileman.setPath(path);
                fileman.setFilename(filename);
                fileman.setContent(teamArray2[i].toString());
                fileman.write();
//
//                FileController.writeFile("../../compare_team/" + nation[i].toLowerCase() + "/" + h.getTid() + "_" + g.getTid() + ".json", jsonArray[i].toString());
//                FileController.writeFile("../../compare_team/" + nation[i].toLowerCase() + "/" + g.getTid() + "_" + h.getTid() + ".json", jsonArray2[i].toString());
//                FileController.writeFile("../../team_data/" + nation[i].toLowerCase() + "/" + h.getTid() + ".json", teamArray1[i].toString());
//                FileController.writeFile("../../team_data/" + nation[i].toLowerCase() + "/" + g.getTid() + ".json", teamArray2[i].toString());

            }

            String sql = "insert into live_match_checked(date,mid) values(current_date," + mid + ")";
            //System.out.println(sql);
            getStmt().execute(sql);

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();
            //closeDB();
        } finally {
            closeDB();
        }

    }

    @Override
    public JSONObject getLogo(int tid) {
        this.connectDB();
        JSONObject obj = new JSONObject();
        try {
            JSONObject teamlogos = new JSONObject();
            String logosql = "SELECT * FROM team_logos WHERE tid=" + tid + " limit 1";
            log.write("info", "Get logo of " + tid + " with " + logosql);
            //this.setRs(this.getStmt().executeQuery(logosql));
            this.rs = this.stmt.executeQuery(logosql);
            String l32 = "http://api.ssporting.com/teams_clean/team_default_32x32.png";
            String l64 = "http://api.ssporting.com/teams_clean/team_default_64x64.png";
            String l256 = "http://api.ssporting.com/teams_clean/team_default_256x256.png";

            obj.put("id", 0);
            obj.put("tid", tid);
            obj.put("32x32", l32);
            obj.put("64x64", l64);
            obj.put("256x256", l256);
            obj.put("update_at", "");
            while (this.getRs().next()) {
                obj.put("id", getRs().getString("id"));
                obj.put("tid", getRs().getString("tid"));
                obj.put("32x32", getRs().getString("32x32"));
                obj.put("64x64", getRs().getString("64x64"));
                obj.put("256x256", getRs().getString("256x256"));
                obj.put("update_at", getRs().getString("update_at"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(TeamImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }

        return obj;

    }

}
