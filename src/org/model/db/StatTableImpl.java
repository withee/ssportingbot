/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.SQLException;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.manager.FileManager;
import org.model.StatTableM;

/**
 *
 * @author mrsyrop
 */
public class StatTableImpl extends DBConnection implements StatTableDAO {

    private String table = "stat_table";
    private String nation[] = {"Th", "En"};

    @Override
    public boolean add(StatTableM st[]) {
        boolean returnValue = false;
        connectDB();
        try {

//            connectDB();
            getConn().setAutoCommit(false);
            for (StatTableM s : st) {
                boolean value = false;
                String query = "select *from " + table + " where tid=" + s.getTid() + " and leagueId=" + s.getLeagueId() + " and subLeagueId=" + s.getSubLeagueId();
                setRs(getStmt().executeQuery(query));
                while (getRs().next()) {
                    value = true;
                }
                //System.out.println(value);
                if (value) {
                    String query2 = "update " + table + " set tid=" + s.getTid() + ",no=" + s.getNo() + ",gp=" + s.getGp() + ",pts=" + s.getPts() + ",w=" + s.getW() + ",d=" + s.getD() + ",l=" + s.getL() + ",gf=" + s.getGf() + ",ga=" + s.getGa() + ",plusminus=" + s.getPlusminus() + ",ml='" + s.getMl() + "' where tnPk=\"" + s.getTnPk() + "\" and leagueId=" + s.getLeagueId() + " and subLeagueId=" + s.getSubLeagueId();
                    int i = getStmt().executeUpdate(query2);
                    if (i != 0) {
                        returnValue = true;
                    }
                    //System.out.println(query2);
                } else {
                    //System.out.println(s.getSubLeagueNamePk());
                    String query2 = "insert into " + table + "(tnPk,cid,leagueId,subLeagueNamePk,no,gp,pts,w,d,l,gf,ga,plusminus,ml,subLeagueId,tid) values('" + s.getTnPk() + "'," + s.getCid() + "," + s.getLeagueId() + ",\"" + (s.getSubLeagueNamePk() == null ? "" : s.getSubLeagueNamePk()) + "\"," + s.getNo() + "," + s.getGp() + "," + s.getPts() + "," + s.getW() + "," + s.getD() + "," + s.getL() + "," + s.getGf() + "," + s.getGa() + "," + s.getPlusminus() + ",'" + s.getMl() + "'," + s.getSubLeagueId() + "," + s.getTid() + ")";
                    int i = getStmt().executeUpdate(query2);
                    if (i != 0) {
                        returnValue = true;
                    }
                    //System.out.println(query2);
                }

            }
            getConn().commit();

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();

        } finally {
            closeDB();
        }
        return returnValue;
    }

    @Override
    public boolean add(StatTableM st[], String side) {
        boolean returnValue = false;
        connectDB();
        try {

//            connectDB();
            getConn().setAutoCommit(false);
            JSONObject json = new JSONObject();
            JSONArray array = new JSONArray();
            for (StatTableM s : st) {
                boolean value = false;
                String query = "select *from " + table + " where tid=" + s.getTid() + " and leagueId=" + s.getLeagueId() + " and subLeagueNamePk=\"" + (s.getSubLeagueNamePk() == null ? "" : s.getSubLeagueNamePk()) + "\"";
                setRs(getStmt().executeQuery(query));
                while (getRs().next()) {
                    value = true;
                }
                //System.out.println(value);
                if (value) {
                    String query2 = "update " + table + " set tid=" + s.getTid() + ",no_" + side + "=" + s.getNo() + ",gp_" + side + "=" + s.getGp() + ",pts_" + side + "=" + s.getPts() + ",w_" + side + "=" + s.getW() + ",d_" + side + "=" + s.getD() + ",l_" + side + "=" + s.getL() + ",gf_" + side + "=" + s.getGf() + ",ga_" + side + "=" + s.getGa() + ",plusminus_" + side + "=" + s.getPlusminus() + ",ml_" + side + "='" + s.getMl() + "' where tnPk=\"" + s.getTnPk() + "\" and leagueId=" + s.getLeagueId() + " and subLeagueNamePk=\"" + (s.getSubLeagueNamePk() == null ? "" : s.getSubLeagueNamePk()) + "\"";
                    int i = getStmt().executeUpdate(query2);
                    if (i != 0) {
                        returnValue = true;
                    }
                    //System.out.println(query2);
                } else {
                    //System.out.println(s.getSubLeagueNamePk());
                    String query2 = "insert into " + table + "(tnPk,cid,leagueId,subLeagueNamePk,no,gp,pts,w,d,l,gf,ga,plusminus,ml,subLeagueId,tid) values('" + s.getTnPk() + "'," + s.getCid() + "," + s.getLeagueId() + ",\"" + (s.getSubLeagueNamePk() == null ? "" : s.getSubLeagueNamePk()) + "\"," + s.getNo() + "," + s.getGp() + "," + s.getPts() + "," + s.getW() + "," + s.getD() + "," + s.getL() + "," + s.getGf() + "," + s.getGa() + "," + s.getPlusminus() + ",'" + s.getMl() + "'," + s.getSubLeagueId() + "," + s.getTid() + ")";
                    int i = getStmt().executeUpdate(query2);
                    if (i != 0) {
                        returnValue = true;
                    }
                    //System.out.println(query2);
                }

            }
            getConn().commit();

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();
        } finally {
            closeDB();
        }
        return returnValue;
    }

    @Override
    public void writeFile(int leagueId) {
        connectDB();
        JSONArray array = new JSONArray();
        JSONObject json = new JSONObject();
        JSONObject jsonTh = new JSONObject();
        JSONArray arrayTh = new JSONArray();
        FileManager fileman = new FileManager();
        String tidlist = "0";
        String query = "select stat_table.*,team.tn,t.* from stat_table "
                + "            left join team on (team.tid=stat_table.tid) "
                + "            left join lang_team t on t.tid = stat_table.tid "
                + "            where stat_table.leagueId=" + leagueId
                + "            group by stat_table.tid order by stat_table.no ASC";
        JSONArray[] jsonArray = new JSONArray[nation.length];
        for (int i = 0; i < jsonArray.length; i++) {
            jsonArray[i] = new JSONArray();
        }
        try {
            setRs(getStmt().executeQuery(query));
            while (getRs().next()) {
                JSONObject obj = new JSONObject();
                obj.put("tnPk", getRs().getString("tnPk"));
                obj.put("cid", getRs().getString("cid"));
                obj.put("leagueId", getRs().getString("leagueId"));
                obj.put("subLeagueNamePk", getRs().getString("subLeagueNamePk"));
                obj.put("no", getRs().getString("no"));
                obj.put("no_h", getRs().getString("no_h"));
                obj.put("no_g", getRs().getString("no_g"));
                obj.put("gp", getRs().getString("gp"));
                obj.put("gp_h", getRs().getString("gp_h"));
                obj.put("gp_g", getRs().getString("gp_g"));
                obj.put("pts", getRs().getString("pts"));
                obj.put("pts_h", getRs().getString("pts_h"));
                obj.put("pts_g", getRs().getString("pts_g"));
                obj.put("w", getRs().getString("w"));
                obj.put("w_h", getRs().getString("w_h"));
                obj.put("w_g", getRs().getString("w_g"));
                obj.put("d", getRs().getString("d"));
                obj.put("d_h", getRs().getString("d_h"));
                obj.put("d_g", getRs().getString("d_g"));
                obj.put("l", getRs().getString("l"));
                obj.put("l_h", getRs().getString("l_h"));
                obj.put("l_g", getRs().getString("l_g"));
                obj.put("gf", getRs().getString("gf"));
                obj.put("gf_h", getRs().getString("gf_h"));
                obj.put("gf_g", getRs().getString("gf_g"));
                obj.put("ga", getRs().getString("ga"));
                obj.put("ga_h", getRs().getString("ga_h"));
                obj.put("ga_g", getRs().getString("ga_g"));
                obj.put("plusminus", getRs().getString("plusminus"));
                obj.put("plusminus_h", getRs().getString("plusminus_h"));
                obj.put("plusminus_g", getRs().getString("plusminus_g"));
                obj.put("ml", getRs().getString("ml"));
                obj.put("ml_g", getRs().getString("ml_g"));
                obj.put("ml_h", getRs().getString("ml_h"));
                obj.put("subLeagueId", getRs().getString("subLeagueId"));
                obj.put("tid", getRs().getString("tid"));
                obj.put("tn", getRs().getString("tn"));
                String myString = getRs().getString("teamNameTh");
                tidlist += "," + getRs().getString("tid");
                //System.out.println(myString);
                for (int i = 0; i < nation.length; i++) {
                    obj.put("tn", getRs().getString("teamName" + nation[i]) != null ? getRs().getString("teamName" + nation[i]) : getRs().getString("tn"));
                    jsonArray[i].add(obj);
                }
            }
            query = "select league.leagueId,league.rid,league.competitionId,league.leagueNamePk,league.leagueName,league.leagueSeason,league.priority,"
                    + "lang_league.* from league "
                    + "            left join lang_league on lang_league.leagueId = league.leagueId"
                    + "            where league.leagueId=" + leagueId;
            setRs(getStmt().executeQuery(query));
            JSONObject info = new JSONObject();
            JSONObject infoTh = new JSONObject();
            JSONArray[] infoArray = new JSONArray[nation.length];
            for (int i = 0; i < infoArray.length; i++) {
                infoArray[i] = new JSONArray();
            }
            while (getRs().next()) {
                info.put("leagueId", getRs().getString("leagueId"));
                info.put("rid", getRs().getString("rid"));
                info.put("competitionId", getRs().getString("competitionId"));
                info.put("leagueNamePk", getRs().getString("leagueNamePk"));
                info.put("leagueName", getRs().getString("leagueName"));
                info.put("leagueSeason", getRs().getString("leagueSeason"));
                info.put("priority", getRs().getString("priority"));
                for (int i = 0; i < nation.length; i++) {
                    info.put("leagueName", getRs().getString("leagueName" + nation[i]) != null ? getRs().getString("leagueName" + nation[i]) : getRs().getString("leagueName"));
                    infoArray[i].add(info);
                }
            }

            for (int i = 0; i < nation.length; i++) {
                json.put("stat_table", jsonArray[i]);
                json.put("info", infoArray[i]);
                //FileController.writeFile("../../gen_file_stat_table/" + nation[i].toLowerCase() + "/" + leagueId + ".json", json.toString());
                fileman.setPath("../../gen_file_stat_table/" + nation[i].toLowerCase());
                fileman.setFilename(leagueId + ".json");
                fileman.setContent(json.toString());
                fileman.write();
            }
            String sql = "SELECT  l.*,home.tid as hid ,away.tid as aid, `date`,  `teamHomeNamePk`,  `teamAwayNamePk`,  `competitionType`,  `competitionNamePk`,  `leagueSeason`,  `subLeagueNamePk`,  `leagueNamePk`,  `hn`,  `an`,  `hcomPk`,  `acomPk`,  `r`,`score`,result_league.subLeagueName,result_league.leagueName,"
                    + "t1.teamNameTh as t1Th,t1.teamNameEn as t1En,"
                    + "t2.teamNameTh as t2Th,t2.teamNameEn as t2En\n"
                    + "FROM result_league\n"
                    + "left join team as home on (result_league.teamHomeNamePk  = home.tnPk)\n"
                    + "left join team as away on (result_league.teamAwayNamePk  = away.tnPk)\n"
                    + "left join lang_team as t1 on (t1.tid = home.tid)\n"
                    + "left join lang_team as t2 on (t2.tid = away.tid)\n"
                    + "left join lang_league as l on (l.leagueId =result_league.leagueId)\n"
                    + "where result_league.leagueId=" + leagueId + "\n"
                    + "group by date,teamHomeNamePk,teamAwayNamePk\n"
                    + "order by result_league.date DESC limit 20\n";
            setRs(getStmt().executeQuery(sql));
            array = new JSONArray();
            arrayTh = new JSONArray();
            JSONArray jsonArray1[] = new JSONArray[nation.length];

            for (int i = 0; i < jsonArray1.length; i++) {
                jsonArray1[i] = new JSONArray();
            }
            while (getRs().next()) {
                JSONObject obj = new JSONObject();
                obj.put("hid", getRs().getString("hid") + "");
                obj.put("aid", getRs().getString("aid") + "");
                obj.put("date", getRs().getString("date"));
                obj.put("teamHomeNamePk", getRs().getString("teamHomeNamePk"));
                obj.put("teamAwayNamePk", getRs().getString("teamAwayNamePk"));
                obj.put("competitionType", getRs().getString("competitionType"));
                obj.put("competitionNamePk", getRs().getString("competitionNamePk"));
                obj.put("leaguSeason", getRs().getString("leagueSeason"));
                obj.put("subLeagueNamePk", getRs().getString("subLeagueNamePk") + "");
                obj.put("leagueNamePk", getRs().getString("leagueNamePk"));
                obj.put("hn", getRs().getString("hn"));
                obj.put("an", getRs().getString("an"));
                obj.put("hcomPk", getRs().getString("hcomPk"));
                obj.put("acomPk", getRs().getString("acomPk"));
                obj.put("score", getRs().getString("score"));
                obj.put("subLeagueName", getRs().getString("subLeagueName") + "");
                obj.put("leagueName", getRs().getString("leagueName"));
                tidlist += "," + getRs().getString("hid") + "," + getRs().getString("aid");
                for (int i = 0; i < nation.length; i++) {
                    obj.put("leagueName", getRs().getString("leagueName" + nation[i]) != null ? getRs().getString("leagueName" + nation[i]) : getRs().getString("leagueName"));
                    obj.put("hn", getRs().getString("t1" + nation[i]) != null ? getRs().getString("t1" + nation[i]) : getRs().getString("hn"));
                    obj.put("an", getRs().getString("t2" + nation[i]) != null ? getRs().getString("t2" + nation[i]) : getRs().getString("an"));
                    jsonArray1[i].add(obj);

                }
            }
            sql = "select l.*,home.tid as hid,away.tid as aid,fixture_league.*, "
                    + "t1.teamNameTh as t1Th,t1.teamNameEn as t1En,"
                    + "t2.teamNameTh as t2Th,t2.teamNameEn as t2En\n"
                    + "from  fixture_league \n"
                    + "left join team as home on (home.tnPk=fixture_league.teamHomeNamePk)\n"
                    + "left join team as away on (away.tnPk=fixture_league.teamAwayNamePk)\n"
                    + "left join lang_team as t1 on (t1.tid = home.tid)\n"
                    + "left join lang_team as t2 on (t2.tid = away.tid)\n"
                    + "left join lang_league as l on (l.leagueId = fixture_league.leagueId)\n"
                    + "where fixture_league.leagueId=" + leagueId + " and fixture_league.date>=current_date\n"
                    + "group by date,teamHomeNamePk,teamAwayNamePk\n"
                    + "order by datetime ASC limit 20";
            setRs(getStmt().executeQuery(sql));
            array = new JSONArray();
            arrayTh = new JSONArray();
            JSONArray jsonArray2[] = new JSONArray[nation.length];
            for (int i = 0; i < jsonArray2.length; i++) {
                jsonArray2[i] = new JSONArray();
            }
            while (getRs().next()) {
                JSONObject obj = new JSONObject();
                obj.put("hid", getRs().getString("hid") + "");
                obj.put("aid", getRs().getString("aid") + "");
                obj.put("date", getRs().getString("date"));
                obj.put("teamHomeNamePk", getRs().getString("teamHomeNamePk"));
                obj.put("teamAwayNamePk", getRs().getString("teamAwayNamePk"));
                obj.put("competitionType", getRs().getString("competitionType"));
                obj.put("competitionNamePk", getRs().getString("competitionNamePk"));
                obj.put("leaguSeason", getRs().getString("leagueSeason"));
                obj.put("subLeagueNamePk", getRs().getString("subLeagueNamePk") + "");
                obj.put("leagueNamePk", getRs().getString("leagueNamePk"));
                obj.put("hn", getRs().getString("hn"));
                obj.put("an", getRs().getString("an"));
                obj.put("hcomPk", getRs().getString("hcomPk"));
                obj.put("acomPk", getRs().getString("acomPk"));
                obj.put("subLeagueName", getRs().getString("subLeagueName") + "");
                obj.put("leagueName", getRs().getString("leagueName"));
                tidlist += "," + getRs().getString("hid") + "," + getRs().getString("aid");
                for (int i = 0; i < nation.length; i++) {
                    obj.put("leagueName", getRs().getString("leagueName" + nation[i]) != null ? getRs().getString("leagueName" + nation[i]) : getRs().getString("leagueName"));
                    obj.put("hn", getRs().getString("t1" + nation[i]) != null ? getRs().getString("t1" + nation[i]) : getRs().getString("hn"));
                    obj.put("an", getRs().getString("t2" + nation[i]) != null ? getRs().getString("t2" + nation[i]) : getRs().getString("an"));
                    jsonArray2[i].add(obj);
                }
            }
            JSONObject logos = new JSONObject();
            String logossql = "SELECT * FROM team_logos WHERE tid IN (" + tidlist + ") GROUP BY tid";
            setRs(getStmt().executeQuery(logossql));
            while (getRs().next()) {
                JSONObject obj = new JSONObject();
                obj.put("id", getRs().getString("id"));
                obj.put("tid", getRs().getString("tid"));
                obj.put("32x32", getRs().getString("32x32"));
                obj.put("64x64", getRs().getString("64x64"));
                obj.put("256x256", getRs().getString("256x256"));
                obj.put("update_at", getRs().getString("update_at"));
                logos.put(getRs().getString("tid"), obj);

            }

            for (int i = 0; i < nation.length; i++) {
                json = new JSONObject();
                json.put("stat_table", jsonArray[i]);
                json.put("info", infoArray[i]);
                json.put("resultList", jsonArray1[i].toString());
                json.put("fixtureList", jsonArray2[i].toString());
                json.put("source", "file");
                json.put("logos", logos);
                //FileController.writeFile("../../gen_file_league/" + nation[i].toLowerCase() + "/" + leagueId + ".json", json.toString());
                fileman.setPath("../../gen_file_league/" + nation[i].toLowerCase());
                fileman.setFilename(leagueId + ".json");
                fileman.setContent(json.toString());
                fileman.write();
            }

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();

        } finally {
            closeDB();
        }
    }

}
