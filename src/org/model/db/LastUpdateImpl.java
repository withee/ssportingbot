/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mrsyrop
 */
public class LastUpdateImpl extends DBConnection implements LastUpdateDAO {

    @Override
    public boolean updateByType(String type) {
        boolean returnValue = false;
        try {
            this.connectDB();
            String sql = "update last_update set datetime=current_timestamp where type='" + type + "'";

            if (0!= this.stmt.executeUpdate(sql)) {
                returnValue = true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(LastUpdateImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }
        return returnValue;
    }

}
