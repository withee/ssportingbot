/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 *
 * @author superx
 */
public class LanguageLeagueImpl extends DBConnection implements LanguageLeagueDAO {

    @Override
    public JSONObject getJsonFromAll() {

        // Variable declearation
        JSONObject returnJson = new JSONObject();
        String[] strLanguage = {"En", "Big", "Gb", "Kr", "Vn", "La", "Th"};

        JSONArray[] jsonArr = new JSONArray[strLanguage.length];
        for (int i = 0; i < strLanguage.length; ++i) {
            jsonArr[i] = new JSONArray();
        }

        // 1. Connet to the database
        this.connectDB();

        // 2. Prepare sql command
        String strQuery = "select "
                + "   league.leagueId";
        for (int i = 0; i < strLanguage.length; ++i) {
            strQuery += ",if(lang_league.leagueName" + strLanguage[i] + " is null,league.leagueName,lang_league.leagueName" + strLanguage[i] + ") as ln" + strLanguage[i] + " ";
        }
        strQuery += "from "
                + "   league "
                + "   left join lang_league "
                + "       on lang_league.leagueId = league.leagueId";

        try {
            // 3. Excuite sql command
            this.setRs(this.getStmt().executeQuery(strQuery));

            // 4. Manage resultset
            while (getRs().next()) {

                for (int i = 0; i < strLanguage.length; ++i) {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put(getRs().getInt("leagueId"), getRs().getString("ln" + strLanguage[i]));
                    jsonArr[i].add(jsonObj);
                }
            }
            
            // 5. Put array to json object
            for (int i = 0; i < strLanguage.length; ++i) {
                
                returnJson.put(strLanguage[i], jsonArr[i]);
                
            }
            
            
            
        } catch (SQLException ex) {
            Logger.getLogger(LiveFileImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }

        // 6. Return JSON string
        return returnJson;

    }

}
