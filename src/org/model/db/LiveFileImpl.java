/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.model.LiveFileM;
import org.utility.Constants;

/**
 *
 * @author superx
 */
public class LiveFileImpl extends DBConnection implements LiveFileDAO {

    @Override
    public LiveFileM getLiveFile() {

        // 1. Variable declearation
        LiveFileM liveFileM = new LiveFileM();
        String strQuery = "";

        // 2. Create database connection and object data for log table
        String name = new Exception("is not thrown").getStackTrace()[0].getMethodName();
        name = this.toString() + name;
        connectDB(name);

        // 3. Prepare sql command
        strQuery = Constants.LIVE_FILE_GET_LAST_ROW;

        try {
            // 4. Excuite sql command
            this.setRs(this.getStmt().executeQuery(strQuery));

            // 5. Manage resultset
            while (getRs().next()) {
                liveFileM.setDate(getRs().getString("date"));
                liveFileM.setTime(getRs().getString("time"));
                liveFileM.setMa(getRs().getInt("ma"));
                liveFileM.setMl00(getRs().getInt("ml00"));
                liveFileM.setMl01(getRs().getInt("ml01"));
                liveFileM.setMl02(getRs().getInt("ml02"));
                liveFileM.setMl10(getRs().getInt("ml10"));
                liveFileM.setMl11(getRs().getInt("ml11"));
                liveFileM.setMl12(getRs().getInt("ml12"));
                liveFileM.setPlinkXML(getRs().getString("plinkXML"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(LiveFileImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }
        // 6. Return data
        return liveFileM;
    }

    @Override
    public boolean insertLiveFile(LiveFileM liveFileM) {
        boolean success = false;
        try {
            this.connectDB();
            String query = "select *from live_file where date='" + liveFileM.getDate() + "'";
            this.setRs(this.getStmt().executeQuery(query));
            if (this.getRs().next()) {
                query = "update live_file set time=current_time,plinkXML='" + liveFileM.getPlinkXML() + "',ml00=" + liveFileM.getMl00() + ",ml01=" + liveFileM.getMl01() + ",ml02=" + liveFileM.getMl02() + ",ml10=" + liveFileM.getMl10() + ",ml11=" + liveFileM.getMl11() + ",ml12=" + liveFileM.getMl12()
                        + " where date='" + liveFileM.getDate() + "'";

            } else {
                query = "insert into live_file (date,time,plinkXML,ml00,ml01,ml02,ml10,ml11,ml12,ma) values ('" + liveFileM.getDate() + "',current_time,'" + liveFileM.getPlinkXML()
                        + "'," + liveFileM.getMl00() + "," + liveFileM.getMl01() + "," + liveFileM.getMl02() + "," + liveFileM.getMl10() + "," + liveFileM.getMl11() + "," + liveFileM.getMl12() + "," + liveFileM.getMa() + ")";

            }

            System.out.println(query);
            if (1 == this.getStmt().executeUpdate(query)) {
                success = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(LiveFileImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }
        return success;
    }

}
