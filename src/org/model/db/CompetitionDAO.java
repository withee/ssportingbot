/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import org.model.CompetitionM;

/**
 *
 * @author mrsyrop
 */
public interface CompetitionDAO {
    public CompetitionM get(int cid);
    public void getCompetitionById(String cid);
    public boolean add(CompetitionM competiton);
    public String getCnPk(String origin);
    public CompetitionM getComByPk(String comPk); 
    
}
