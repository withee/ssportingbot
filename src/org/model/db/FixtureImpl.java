/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.SQLException;
import java.util.Vector;
import org.manager.LogManager;
import org.model.FixtureM;

/**
 *
 * @author mrsyrop
 */
public class FixtureImpl extends DBConnection implements FixtureDAO {

    private LogManager log = new LogManager();

    @Override
    public boolean add(FixtureM[] vt, String tnPk) {
        boolean returnValue = false;
        this.connectDB();
        try {
            //
//            connectDB();
            this.getConn().setAutoCommit(false);
            String sql = "delete from fixture where teamNamePk=\"" + tnPk + "\"";
            this.getStmt().executeUpdate(sql);
            for (int i = 0; i < vt.length; i++) {
                FixtureM f = vt[i];
                String query = "insert into fixture (date,teamHomeNamePk,teamAwayNamePk,competitionType,competitionNamePk,leagueSeason,subLeagueNamePk,leagueNamePk,hn,an,acomPk,hcomPk,leagueName,lnk,teamNamePk) values('"
                        + f.getDate() + "',\"" + f.getTeamHomeNamePk() + "\",\"" + f.getTeamAwayNamePk() + "\",\"" + f.getCompetitionType() + "\",\"" + f.getCompetitionNamePk() + "\",\""
                        + f.getLeagueSeasson() + "\",\"" + f.getSubLeagueNamePk() + "\",\"" + f.getLeagueNamePk() + "\",\"" + f.getHn() + "\",\"" + f.getAn() + "\",\"" + f.getAcomPk() + "\",\"" + f.getHcomPk()
                        + "\",\"" + f.getLeagueName() + "\",\"" + f.getLnk() + "\",\"" + tnPk + "\")";
                try {
                    int j = this.getStmt().executeUpdate(query);
                    if (j != 0) {
                        log.write("info", "Cmpleted: " + query);
                        returnValue = true;
                    } else {
                        System.out.println(i + ":" + vt.length);
                    }
                } catch (Exception ex) {
                    continue;
                }
            }
            this.getConn().commit();
        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();
        } finally {
            this.closeDB();
        }
        return returnValue;
    }

    @Override
    public boolean addLeagueFixture(Vector<FixtureM[]> list) {

        boolean returnValue = false;
        connectDB();
        try {
            //
//            connectDB();
            String query = "";
            FixtureM f = new FixtureM();

            getConn().setAutoCommit(false);
            for (int i = 0; i < list.size(); i++) {
                FixtureM fList[] = list.get(i);
                for (int j = 0; j < fList.length; j++) {
                    f = fList[j];

                    String datetimeArray[] = f.getDatetime().split(" ");
                    String dateArray[] = datetimeArray[0].split("\\.");
                    String datetime = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0] + " " + datetimeArray[1];
                    String date = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0];
                    f.setDate(date);
                    f.setDatetime(datetime);
                    if (i == 0 && j == 0) {
                        query = "delete from fixture_league where leagueId=" + f.getLeagueId() + " and subLeagueId=" + f.getSubLeagueId() + " and leagueSeason='" + f.getLeagueSeasson() + "'";
                        getStmt().executeUpdate(query);
                    }

                    query = "insert into fixture_league (date,teamHomeNamePk,teamAwayNamePk,competitionType,competitionNamePk,leagueSeason,subLeagueNamePk,leagueNamePk,hn,an,hcomPk,acomPk,subLeagueName,datetime,leagueId,subLeagueId,leagueName)"
                            + " values('" + f.getDate() + "',\"" + f.getTeamHomeNamePk() + "\",\"" + f.getTeamAwayNamePk() + "\",'" + f.getCompetitionType() + "',\"" + f.getCompetitionNamePk() + "\",'"
                            + f.getLeagueSeasson() + "',\"" + f.getSubLeagueNamePk() + "\",\"" + f.getLeagueNamePk() + "\",\"" + f.getHn() + "\",\"" + f.getAn() + "\",\"" + f.getHcomPk() + "\",\""
                            + f.getAcomPk() + "\",'" + f.getSubLeagueName() + "','" + f.getDatetime() + "'," + f.getLeagueId() + "," + f.getSubLeagueId() + ",\"" + f.getLeagueName() + "\")";

                    try {
                        int k = getStmt().executeUpdate(query);

                    } catch (SQLException ex) {
                        //System.out.println("conitnue");
                        continue;
                    }
                }
            }
            getConn().commit();
            returnValue = true;
//            closeDB();
        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();

        } finally {
            closeDB();
        }
        return returnValue;
    }

}
