/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 *
 * @author superx
 */
public class LanguageTeamImpl extends DBConnection implements LanguageTeamDAO {

    @Override
    public JSONObject getJsonFromAll() {

        // Variable declearation
        JSONObject returnJson = new JSONObject();
        String[] strLanguage = {"En", "Big", "Gb", "Kr", "Vn", "La", "Th"};

        JSONArray[] jsonArr = new JSONArray[strLanguage.length];
        for (int i = 0; i < strLanguage.length; ++i) {
            jsonArr[i] = new JSONArray();
        }

        // 1. Connet to the database
        this.connectDB();

        // 2. Prepare sql command
        String strQuery = "select "
                + "         team.tid";
                 for (int i = 0; i < strLanguage.length; ++i) {
            strQuery += ",if(lang_team.teamName" + strLanguage[i] + " is null,team.teamName,lang_team.teamName" + strLanguage[i] + ") as tn" + strLanguage[i] + " ";
        }
        strQuery += "from "
                + "   team "
                + "   left join lang_team "
                + "       on lang_team.tid = team.tid";

        try {
            // 3. Excuite sql command
            this.setRs(this.getStmt().executeQuery(strQuery));

            // 4. Manage resultset
            while (getRs().next()) {

                for (int i = 0; i < strLanguage.length; ++i) {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put(getRs().getInt("tid"), getRs().getString("tn" + strLanguage[i]));
                    jsonArr[i].add(jsonObj);
                }
            }

            // 5. Put array to json object
            for (int i = 0; i < strLanguage.length; ++i) {

                returnJson.put(strLanguage[i], jsonArr[i]);

            }

        } catch (SQLException ex) {
            Logger.getLogger(LiveFileImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }

        // 6. Return JSON string
        return returnJson;
    }

}
