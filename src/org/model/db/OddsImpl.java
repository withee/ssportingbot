/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONObject;
import org.manager.FileManager;

/**
 *
 * @author mrsyrop
 */
public class OddsImpl extends DBConnection implements OddsDAO {

    @Override
    public void Today() {
        this.connectServDB();
        String sql = "select 0 as user_defined,odds_7m.*,lang_league.leagueId,t1.tid as tid1,t2.tid as tid2,\n"
                + "lang_league.leagueName,lang_league.leagueNameEn,lang_league.leagueNameTh,lang_league.leagueNameEn,\n"
                + "t1.teamName as teamName1,t1.teamNameTh as teamNameTh1,t1.teamNameEn as teamNameEn1,\n"
                + "t2.teamName as teamName2,t2.teamNameTh as teamNameTh2,t2.teamNameEn as teamNameEn2\n"
                + "from odds_7m\n"
                + "left join lang_league on lang_league.id7m = odds_7m.league_id\n"
                + "left join lang_team as t1 on t1.id7m = odds_7m.home_id\n"
                + "left join lang_team as t2 on t2.id7m = odds_7m.away_id\n"
                + "where (odds_7m.odds_date >= current_date  - interval 1 day )\n"
                + "and lang_league.id7m is  not null and t1.id7m is not null and t2.id7m is not null and odds_7m.hdp !=''";

        try {
            this.rs = this.stmt.executeQuery(sql);
            //System.out.println(rs.wasNull());

            JSONObject list = new JSONObject();
            JSONObject lang_league = new JSONObject();
            JSONObject lang_team1 = new JSONObject();
            JSONObject lang_team2 = new JSONObject();
            JSONObject data = new JSONObject();
            JSONObject logos = new JSONObject();
            JSONObject odds_type = new JSONObject();
            int prev_lid = 0;
            while (rs.next()) {

                //System.out.println(rs.getString("leagueName"));
                JSONObject league = new JSONObject();
                league.put("en", rs.getString("leagueNameEn"));
                league.put("th", rs.getString("leagueNameTh"));
                lang_league.put(rs.getInt("leagueId"), league);

                JSONObject team = new JSONObject();
                team.put("en", rs.getString("teamNameEn1"));
                team.put("th", rs.getString("teamNameTh1"));
                lang_team1.put(rs.getInt("tid1"), league);

                team.put("en", rs.getString("teamNameEn2"));
                team.put("th", rs.getString("teamNameTh2"));
                lang_team2.put(rs.getInt("tid2"), league);

                JSONObject odds = new JSONObject();
                odds.put("user_defined", rs.getInt("user_defined"));
                odds.put("match_id", rs.getInt("match_id"));
                odds.put("league_id", rs.getInt("league_id"));
                odds.put("home_id", rs.getInt("home_id"));
                odds.put("away_id", rs.getInt("away_id"));
                odds.put("type", rs.getString("type"));
                odds.put("kick_off", rs.getInt("kick_off"));

                odds.put("hdp_home", rs.getFloat("hdp_home"));
                odds.put("hdp_away", rs.getFloat("hdp_away"));
                odds.put("hdp", rs.getFloat("hdp"));
                odds.put("hdp_home_e", rs.getFloat("hdp_home_e"));
                odds.put("hdp_away_e", rs.getFloat("hdp_away_e"));
                odds.put("hdp_e", rs.getFloat("hdp_e"));
                odds.put("hpd_datetime_update", rs.getString("hpd_datetime_update"));

                odds.put("ou_over_e", rs.getFloat("ou_over_e"));
                odds.put("ou_under_e", rs.getFloat("ou_under_e"));
                odds.put("ou_e", rs.getFloat("ou_e"));
                odds.put("ou_over", rs.getFloat("ou_over"));
                odds.put("ou_under", rs.getFloat("ou_under"));
                odds.put("ou", rs.getFloat("ou"));
                odds.put("ou_datetime_update", rs.getString("ou_datetime_update"));

                odds.put("odds1x2_home_e", rs.getFloat("odds1x2_home_e"));
                odds.put("odds1x2_away_e", rs.getFloat("odds1x2_away_e"));
                odds.put("odds1x2_draw_e", rs.getFloat("odds1x2_draw_e"));
                odds.put("odds1x2_home", rs.getFloat("odds1x2_home"));
                odds.put("odds1x2_away", rs.getFloat("odds1x2_away"));
                odds.put("odds1x2_draw", rs.getFloat("odds1x2_draw"));
                odds.put("odds1x2_datetime_update", rs.getString("odds1x2_datetime_update"));
                odds.put("odds_date", rs.getString("odds_date"));

                odds.put("leagueId", rs.getInt("leagueId"));
                odds.put("tid1", rs.getInt("tid1"));
                odds.put("tid2", rs.getInt("tid2"));
                odds.put("img", "");

                if (prev_lid != rs.getInt("leagueId")) {                                     
                    if (prev_lid != 0) {
                        String key = rs.getInt("leagueId") + "_" + rs.getInt("tid1") + "_" + rs.getInt("tid2");
                        data.put(key, odds_type);
                    }                    
                    odds_type = new JSONObject();
                    odds_type.put(rs.getString("type"), odds);
                    prev_lid = rs.getInt("leagueId");
                } else {
                    odds_type.put(rs.getString("type"), odds);
                }

            }
            list.put("data", data);
            list.put("lang_lague", lang_league);
            list.put("lang_team1", lang_team1);           
            list.put("lang_team2", lang_team2);
            
            

            FileManager fileman = new FileManager();
            fileman.setPath("./");
            fileman.setFilename("today.json");
            fileman.setContent(list.toString());
            fileman.write();

        } catch (SQLException ex) {
            Logger.getLogger(OddsImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }
    }

}
