/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

/**
 *
 * @author mrsyrop
 */
public class SubLeagueM {

    private String subLeagueNamePk;
    private String subLeagueName;
    private String leagueNamePk;
    private int leagueId;
    private String subLeagueNameTh;
    private String competitionType;
    private String competitionNamePk;
    private String leagueSeason;
    private int competitionId;

    /**
     * @return the subLeagueNamePk
     */
    public String getSubLeagueNamePk() {
        return subLeagueNamePk;
    }

    /**
     * @param subLeagueNamePk the subLeagueNamePk to set
     */
    public void setSubLeagueNamePk(String subLeagueNamePk) {
        this.subLeagueNamePk = subLeagueNamePk;
    }

    /**
     * @return the subLeagueName
     */
    public String getSubLeagueName() {
        return subLeagueName;
    }

    /**
     * @param subLeagueName the subLeagueName to set
     */
    public void setSubLeagueName(String subLeagueName) {
        this.subLeagueName = subLeagueName;
    }

    /**
     * @return the leagueNamePk
     */
    public String getLeagueNamePk() {
        return leagueNamePk;
    }

    /**
     * @param leagueNamePk the leagueNamePk to set
     */
    public void setLeagueNamePk(String leagueNamePk) {
        this.leagueNamePk = leagueNamePk;
    }

    /**
     * @return the leagueId
     */
    public int getLeagueId() {
        return leagueId;
    }

    /**
     * @param leagueId the leagueId to set
     */
    public void setLeagueId(int leagueId) {
        this.leagueId = leagueId;
    }

    /**
     * @return the subLeagueNameTh
     */
    public String getSubLeagueNameTh() {
        return subLeagueNameTh;
    }

    /**
     * @param subLeagueNameTh the subLeagueNameTh to set
     */
    public void setSubLeagueNameTh(String subLeagueNameTh) {
        this.subLeagueNameTh = subLeagueNameTh;
    }

    /**
     * @return the competitionType
     */
    public String getCompetitionType() {
        return competitionType;
    }

    /**
     * @param competitionType the competitionType to set
     */
    public void setCompetitionType(String competitionType) {
        this.competitionType = competitionType;
    }

    /**
     * @return the competitionNamePk
     */
    public String getCompetitionNamePk() {
        return competitionNamePk;
    }

    /**
     * @param competitionNamePk the competitionNamePk to set
     */
    public void setCompetitionNamePk(String competitionNamePk) {
        this.competitionNamePk = competitionNamePk;
    }

    /**
     * @return the leagueSeason
     */
    public String getLeagueSeason() {
        return leagueSeason;
    }

    /**
     * @param leagueSeason the leagueSeason to set
     */
    public void setLeagueSeason(String leagueSeason) {
        this.leagueSeason = leagueSeason;
    }

    /**
     * @return the competitionId
     */
    public int getCompetitionId() {
        return competitionId;
    }

    /**
     * @param competitionId the competitionId to set
     */
    public void setCompetitionId(int competitionId) {
        this.competitionId = competitionId;
    }
}
