/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.manager.FileManager;
import org.model.CompetitionM;
import org.utility.Constants;

/**
 *
 * @author mrsyrop
 */
public class CompetitionImpl extends DBConnection implements CompetitionDAO {

    @Override
    public CompetitionM get(int cid) {
        CompetitionM competitionM = new CompetitionM();
        this.connectDB();
        String query = Constants.GET_COMPETITION_BY_ID + cid;
        //System.out.println(query);
        try {
            this.rs = this.stmt.executeQuery(query);
            while (this.rs.next()) {

                competitionM.setCompetitionId(this.rs.getInt("competitionId"));
                competitionM.setCompetitionType(this.rs.getString("competitionType"));
                competitionM.setCompetitionName(this.rs.getString("competitionName"));
                competitionM.setCompetitionNamePk(this.rs.getString("competitionNamePk"));
                competitionM.setLeagueNamePk(this.rs.getString("leagueNamePk"));
                competitionM.setSeason(this.rs.getString("season"));
                competitionM.setCompetitionNameTh(this.rs.getString("competitionNameTh"));
            }

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();

        } finally {
            this.closeDB();
        }

        return competitionM;
    }

    @Override
    public void getCompetitionById(String cid) {
        FileManager fileman = new FileManager();
        fileman.setFilename(FileManager.getOldestFile(Constants.BALL24_COMPETITION));
        fileman.setPath(Constants.BALL24_COMPETITION);
        //System.out.println(fileman.getFullpath());
        if (FileManager.isFile(fileman)) {
            try {
                JSONParser parser = new JSONParser();
                JSONObject jsonObj = new JSONObject();
                Object obj = parser.parse(fileman.read());
                jsonObj = (JSONObject) obj;
                JSONObject competitionObj;
                competitionObj = (JSONObject) jsonObj.get(cid);
                if (!competitionObj.isEmpty()) {
                    //System.out.println(competition.get("season"));
                    CompetitionM competitionM = new CompetitionM();
                    competitionM.setCompetitionId(Integer.parseInt(cid));
                    competitionM.setCompetitionType((String) competitionObj.get("competitionType"));
                    competitionM.setCompetitionName((String) competitionObj.get("competitionName"));
                    competitionM.setCompetitionNamePk((String) competitionObj.get("competitionNamePk"));
                    competitionM.setLeagueNamePk((String) competitionObj.get("leagueNamePk"));
                    competitionM.setSeason((String) competitionObj.get("season"));
                    this.add(competitionM);
                } else {

                }
            } catch (ParseException ex) {
                Logger.getLogger(CompetitionImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println(fileman.getFullpath() + " not found");
        }
    }

    @Override
    public boolean add(CompetitionM competition) {
        boolean success = true;
        this.connectDB();
        try {
            if (competition.getCompetitionId() == 145) {
                competition.setCompetitionNamePk("South-Korea");
                System.out.println(competition.getCompetitionNamePk());
            }
            PreparedStatement stm;
            stm = competition.getSqlInsertValue(this.getConn());
            //System.out.println(stm.toString());
            if (stm.execute()) {
                System.out.println("update competition success");
            } else {
                System.out.println("update competition success");
            }
        } catch (SQLException ex) {
            success = false;
            Logger.getLogger(CompetitionImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }
        return success;
    }

    @Override
    public String getCnPk(String origin) {
        String cnPk = origin;
        if (origin.equals("Brazil")) {
            cnPk = "Brasil";
        }
        return cnPk;
    }

    @Override
    public CompetitionM getComByPk(String comPk) {
        CompetitionM c = new CompetitionM();
        connectDB();
        String query = "select *from competitions  where competitionNamePk='" + comPk + "'";
        //System.out.println(query);
        try {
            rs = stmt.executeQuery(query);
            while (rs.next()) {

                c.setCompetitionId(rs.getInt("competitionId"));
                c.setCompetitionType(rs.getString("competitionType"));
                c.setCompetitionName(rs.getString("competitionName"));
                c.setCompetitionNamePk(rs.getString("competitionNamePk"));
                c.setLeagueNamePk(rs.getString("leagueNamePk"));
                c.setSeason(rs.getString("season"));
                c.setCompetitionNameTh(rs.getString("competitionNameTh"));
            }


        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();            
        } finally {
            closeDB();
        }
        return c;
    }

}
