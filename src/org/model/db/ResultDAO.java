/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.SQLException;
import org.model.ResultM;
import org.model.ResultVsM;

/**
 *
 * @author mrsyrop
 */
public interface ResultDAO {
    public boolean add(ResultVsM[] list);
     public boolean add(ResultM[] vt);
    public boolean addResultVs(ResultVsM[] list, String tnPk1, String tnPk2);
    public boolean addLeagueResult(ResultM list[]); 
}
