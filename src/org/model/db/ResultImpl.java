/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.SQLException;
import org.model.ResultM;
import org.model.ResultVsM;

/**
 *
 * @author mrsyrop
 */
public class ResultImpl extends DBConnection implements ResultDAO {

    @Override
    public boolean add(ResultVsM[] list) {
        boolean returnValue = true;
        connectDB();
        try {
            //
//            connectDB();
            getConn().setAutoCommit(false);
            for (int i = 0; i < list.length; i++) {
                boolean value = false;
                ResultVsM r = list[i];
                String query = "select *from result_vs where tnPk1=\"" + r.getTnPk1() + "\" and tnPk2=\"" + r.getTnPk2() + "\" and date='" + r.getDate() + "'";
                setRs(getStmt().executeQuery(query));
                if (getRs().next()) {
                    returnValue = false;
                    break;

                }
                query = "insert into result_vs (tnPk1,tnPk2,cnPk,date,ct,lnPk,season,lnr,matchDate,hmn,gmn,score,sl) values(\"" + r.getTnPk1() + "\",\""
                        + r.getTnPk2() + "\",\"" + r.getCnPk() + "\",\"" + r.getDate() + "\",\"" + r.getCt() + "\",\"" + r.getLnPk() + "\",\"" + r.getSeason() + "\",\"" + r.getLnr() + "\",\""
                        + r.getMatchDate() + "\",\"" + r.getHmn() + "\",\"" + r.getGmn() + "\",\"" + r.getScore() + "\",\"" + r.getSl() + "\")";
                int j = getStmt().executeUpdate(query);
                if (j != 0) {
                    returnValue = true;

                }
            }
            getConn().commit();

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();
        } finally {
            closeDB();
        }
        return returnValue;
    }

    @Override
    public boolean addResultVs(ResultVsM[] list, String tnPk1, String tnPk2) {
        boolean returnValue = true;
        connectDB();
        try {
            //
//            connectDB();
            getConn().setAutoCommit(false);
            String sql = "delete from result_vs where (tnPk1='" + tnPk1 + "' and tnPk2='" + tnPk2 + "') or (tnPk1='" + tnPk2 + "' and tnPk2='" + tnPk1 + "')";
            getStmt().executeUpdate(sql);
            for (int i = 0; i < list.length; i++) {
                boolean value = false;
                ResultVsM r = list[i];

                String query = "insert into result_vs (tnPk1,tnPk2,cnPk,date,ct,lnPk,season,lnr,matchDate,hmn,gmn,score,sl) values(\"" + r.getTnPk1() + "\",\""
                        + r.getTnPk2() + "\",\"" + r.getCnPk() + "\",\"" + r.getDate() + "\",\"" + r.getCt() + "\",\"" + r.getLnPk() + "\",\"" + r.getSeason() + "\",\"" + r.getLnr() + "\",\""
                        + r.getMatchDate() + "\",\"" + r.getHmn() + "\",\"" + r.getGmn() + "\",\"" + r.getScore() + "\",\"" + r.getSl() + "\")";
                int j = getStmt().executeUpdate(query);
                if (j != 0) {
                    returnValue = true;

                }
            }
            getConn().commit();
//            closeDB();

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();

        } finally {
            closeDB();
        }
        return returnValue;
    }

    @Override
    public boolean add(ResultM[] vt) {
        boolean returnValue = false;
        connectDB();
        try {
            //
//            connectDB();
            getConn().setAutoCommit(false);
            for (int i = 0; i < vt.length; i++) {
                ResultM f = new ResultM();
                f = vt[i];
                boolean value = true;

                String query = "select *from result where date='" + f.getDate() + "' and teamHomeNamePk=\"" + f.getTeamHomeNamePk() + "\" and teamAwayNamePk=\"" + f.getTeamAwayNamePk() + "\" and teamNamePk=\"" + f.getTeamNamePk() + "\"";
                setRs(getStmt().executeQuery(query));
//                while (getRs().next()) {
//                    returnValue = false;
//                    value = false;
//                    break;
//                }
                if (!getRs().next()) {
                    String r = null;
                    String r1 = null;
                    String r2 = null;
                    String teamNamePk = f.getTeamNamePk();
                    r = f.getR();
                    query = "insert into result (date,teamHomeNamePk,teamAwayNamePk,competitionType,competitionNamePk,leagueSeason,subLeagueNamePk,leagueNamePk,hn,an,acomPk,hcomPk,r,score,link,leagueName,lnk,teamNamePk) values('"
                            + f.getDate() + "',\"" + f.getTeamHomeNamePk() + "\",\"" + f.getTeamAwayNamePk() + "\",\"" + f.getCompetitionType() + "\",\"" + f.getCompetitionNamePk() + "\",\""
                            + f.getLeagueSeasson() + "\",\"" + f.getSubLeagueNamePk() + "\",\"" + f.getLeagueNamePk() + "\",\"" + f.getHn() + "\",\"" + f.getAn() + "\",\"" + f.getAcomPk() + "\",\"" + f.getHcomPk() + "\",\"" + r + "\",\"" + f.getScore() + "\",\"" + f.getLink() + "\",\""
                            + f.getLeagueName() + "\",\"" + f.getLnk() + "\",\"" + teamNamePk + "\")";

                    int j = getStmt().executeUpdate(query);
                    if (j != 0) {
                        returnValue = true;

                    } else {
                        System.out.println(query);
                    }
                } else {
                    break;
                }
            }
            getConn().commit();
//            closeDB();

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();

        } finally {
            closeDB();
        }
        return returnValue;
    }

    @Override
    public boolean addLeagueResult(ResultM list[]) {

        boolean returnValue = false;
        connectDB();
        try {
            //
//            connectDB();
            String query = "";
            ResultM r = new ResultM();
            int lastIndex = list.length;
            outer:
            for (int i = 0; i < list.length; i++) {
                r = list[i];
                String datetimeArray[] = r.getDatetime().split(" ");
                String dateArray[] = datetimeArray[0].split("\\.");
                String datetime = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0] + " " + datetimeArray[1];
                String date = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0];
                list[i].setDate(date);
                list[i].setDatetime(datetime);
                query = "select *from result_league where date='" + date + "' and teamHomeNamePk=\"" + r.getTeamHomeNamePk() + "\" and teamAwayNamePk=\"" + r.getTeamAwayNamePk() + "\"";
                setRs(getStmt().executeQuery(query));
                while (getRs().next()) {
                    //System.out.println(i);
                    lastIndex = i;
                    break outer;
                }
            }
            getConn().setAutoCommit(false);
            for (int i = 0; i < lastIndex; i++) {
                r = list[i];
                query = "insert into result_league (date,teamHomeNamePk,teamAwayNamePk,competitionType,competitionNamePk,leagueSeason,subLeagueNamePk,leagueNamePk,hn,an,hcomPk,acomPk,r,r1,r2,score,link,subLeagueName,datetime,leagueId,subLeagueId,leagueName)"
                        + " values('" + r.getDate() + "',\"" + r.getTeamHomeNamePk() + "\",\"" + r.getTeamAwayNamePk() + "\",'" + r.getCompetitionType() + "',\"" + r.getCompetitionNamePk() + "\",'"
                        + r.getLeagueSeasson() + "',\"" + r.getSubLeagueNamePk() + "\",\"" + r.getLeagueNamePk() + "\",\"" + r.getHn() + "\",\"" + r.getAn() + "\",\"" + r.getHcomPk() + "\",\""
                        + r.getAcomPk() + "\",'','','',\"" + r.getScore() + "\",\"" + r.getLink() + "\",'" + r.getSubLeagueName() + "','" + r.getDatetime() + "'," + r.getLeagueId() + "," + r.getSubLeagueId() + ",\"" + r.getLeagueName() + "\")";
                int j = getStmt().executeUpdate(query);
                if (j != 0) {
                }
            }
            getConn().commit();
            if (list.length == lastIndex) {
                returnValue = true;
            }

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();

        } finally {
            closeDB();
        }
        return returnValue;
    }

}
