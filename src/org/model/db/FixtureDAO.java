/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.SQLException;
import java.util.Vector;
import org.model.FixtureM;

/**
 *
 * @author mrsyrop
 */
public interface FixtureDAO {
    public boolean add(FixtureM[] vt, String tnPk);
     public boolean addLeagueFixture(Vector<FixtureM[]> list); 
}
