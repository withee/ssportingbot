/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import org.model.TeamM;
import net.sf.json.JSONObject;

/**
 *
 * @author mrsyrop
 */
public interface TeamDAO {

    public TeamM getBytid(int tid);

    public boolean addSingle(TeamM t);

    public void writeFile(TeamM home, TeamM guest, int mid);
    public JSONObject getLogo(int tid);

}
