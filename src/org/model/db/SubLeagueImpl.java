/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.SQLException;

/**
 *
 * @author mrsyrop
 */
public class SubLeagueImpl extends DBConnection implements SubLeagueDAO {

    private String table = "sub_league";

    @Override
    public SubLeagueM getBySubleagueName(String subleagueName) {
        SubLeagueM l = new SubLeagueM();
        connectDB();
        String query = "select *from " + table + " where subleagueName=\"" + subleagueName + "\"";
        //System.out.println(query);
        try {
            setRs(getStmt().executeQuery(query));
            while (getRs().next()) {

                l.setLeagueId(getRs().getInt("leagueId"));
                //l.setLeagueSeason(rs.getString("leagueSeason"));
                //l.setCompetitionNamePk(rs.getString("competitionNamePk"));
                //l.setCompetitionType(rs.getString("competitionType"));
                l.setCompetitionId(getRs().getInt("competitionId"));
                l.setSubLeagueName(getRs().getString("subLeagueName"));
                l.setSubLeagueNamePk(getRs().getString("subLeagueNamePk"));
                //l.setSubLeagueNameTh(rs.getString("subLeagueNameTh"));
                //l.setLeagueNamePk(rs.getString("leagueNamePk"));
            }

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();

        } finally {
            closeDB();
        }
        return l;
    }

}
