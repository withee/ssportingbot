/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.SQLException;
import java.util.Vector;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.model.LiveLeagueM;
import org.utility.Constants;

/**
 *
 * @author superx
 */
public interface LiveLeagueDAO {

    public void getLiveLeague2JSONArrayStatus(JSONArray liveLeagueArray,
            JSONArray[] jsonArrLl,
            int status);

    public void getLiveLeague2JSONArrayStatus(JSONArray liveLeagueArray,
            JSONArray[] jsonArrLl,
            String query);

    public boolean insertLiveLeague(LiveLeagueM liveLeagueM);

    public boolean delOldrecord(String dateStr);

    public boolean setOldrecord(String dateStr);

    public Vector<LiveLeagueM> retrieveByChecked();

    public void writeFile();

}
