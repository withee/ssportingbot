/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import org.model.LiveFileM;

/**
 *
 * @author superx
 */
public interface LiveFileDAO {

    public LiveFileM getLiveFile();
    public boolean insertLiveFile(LiveFileM liveFileM);

}
