/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import static java.lang.System.exit;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.manager.FileManager;
import org.model.LeagueM;
import org.model.LiveLeagueM;
import org.utility.Constants;

/**
 *
 * @author superx
 */
public class LiveLeagueImpl extends DBConnection implements LiveLeagueDAO {

    private String table = "league";
    private String nation[] = {"Th", "En"};

    @Override
    public void getLiveLeague2JSONArrayStatus(JSONArray liveLeagueArray, JSONArray[] jsonArrLl, int status) {

        String query = "";

        switch (status) {

            case Constants.LIVE_LEAGUE_STATUS_NONE:
                query = Constants.LIVE_LEAGUE_GET_DATA_WITH_TEAMS;
                break;
            case Constants.LIVE_LEAGUE_STATUS_1:
                query = Constants.LIVE_LEAGUE_GET_DATA_WITH_TEAMS_STATUS_1;
                break;
            case Constants.LIVE_LEAGUE_STATUS_5UP:
                query = Constants.LIVE_LEAGUE_GET_DATA_WITH_TEAMS_STATUS_5UP;
                break;

        }

        liveLeagueArray = (null == liveLeagueArray) ? new JSONArray() : liveLeagueArray;
        jsonArrLl = (null == jsonArrLl) ? new JSONArray[Constants.NATION.length] : jsonArrLl;

        for (int i = 0; i < jsonArrLl.length; i++) {
            jsonArrLl[i] = new JSONArray();
        }

        String name = new Exception("is not thrown").getStackTrace()[0].getMethodName();
        name = this.toString() + name;
        this.connectDB(name);

        try {

            this.setRs(this.getStmt().executeQuery(query));
            while (this.getRs().next()) {
                JSONObject obj = new JSONObject();
                obj.put("competitionId", this.getRs().getString("competitionId"));
                obj.put("leagueId", this.getRs().getString("leagueId"));
                obj.put("subleagueId", this.getRs().getString("subleagueId"));
                obj.put("subleagueName", this.getRs().getString("subleagueName"));
                obj.put("ln", this.getRs().getString("ln"));
                obj.put("lnk", this.getRs().getString("lnk"));
                obj.put("fg", this.getRs().getString("fg"));
                obj.put("bg", this.getRs().getString("bg"));
                obj.put("kn", this.getRs().getString("kn"));
                obj.put("kkod", this.getRs().getString("kkod"));
                obj.put("priority", this.getRs().getString("priority") + "");
                liveLeagueArray.add(obj);
                int index = 0;
                for (String national : Constants.NATION) {
//                    obj.put("ln", this.getRs().getString("leagueName" + national) != null ? this.getRs().getString("leagueName" + national) : this.getRs().getString("ln"));
//                    obj.put("kn", this.getRs().getString("comName" + national) != null ? this.getRs().getString("comName" + national) : this.getRs().getString("kn"));

                    obj.put("ln", this.getRs().getString("ln"));
                    obj.put("kn", this.getRs().getString("kn"));
                    jsonArrLl[index++].add(obj);
                }

            }//end while (this.rs.next()) {
        } catch (SQLException ex) {
            Logger.getLogger(LiveMatchImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }
    }

    @Override
    public boolean insertLiveLeague(LiveLeagueM liveLeagueM) {
        boolean success = false;
        try {
            this.connectDB();
            String query = liveLeagueM.getSqlInsertValue();
            System.out.println(query);
            int i = this.stmt.executeUpdate(query);
            //System.out.println(i);
            if (0 != i) {
                success = true;
                //System.out.println("Success:" + query);
                //exit(0);
            } else {
                //System.out.println("Fail:" + query);
            }

        } catch (SQLException ex) {
            Logger.getLogger(LiveFileImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }
        return success;
    }

    @Override
    public void getLiveLeague2JSONArrayStatus(JSONArray liveLeagueArray, JSONArray[] jsonArrLl, String query) {
        liveLeagueArray = (null == liveLeagueArray) ? new JSONArray() : liveLeagueArray;
        jsonArrLl = (null == jsonArrLl) ? new JSONArray[Constants.NATION.length] : jsonArrLl;

        for (int i = 0; i < jsonArrLl.length; i++) {
            jsonArrLl[i] = new JSONArray();
        }

        String name = new Exception("is not thrown").getStackTrace()[0].getMethodName();
        name = this.toString() + name;
        this.connectDB();

        try {

            this.setRs(this.getStmt().executeQuery(query));
            while (this.getRs().next()) {
                JSONObject obj = new JSONObject();
                obj.put("competitionId", this.getRs().getString("competitionId"));
                obj.put("leagueId", this.getRs().getString("leagueId"));
                obj.put("subleagueId", this.getRs().getString("subleagueId"));
                obj.put("subleagueName", this.getRs().getString("subleagueName"));
                obj.put("ln", this.getRs().getString("ln"));
                obj.put("lnk", this.getRs().getString("lnk"));
                obj.put("fg", this.getRs().getString("fg"));
                obj.put("bg", this.getRs().getString("bg"));
                obj.put("kn", this.getRs().getString("kn"));
                obj.put("kkod", this.getRs().getString("kkod"));
                obj.put("priority", this.getRs().getString("priority") + "");
                liveLeagueArray.add(obj);
                int index = 0;
                for (String national : Constants.NATION) {
//                    obj.put("ln", this.getRs().getString("leagueName" + national) != null ? this.getRs().getString("leagueName" + national) : this.getRs().getString("ln"));
//                    obj.put("kn", this.getRs().getString("comName" + national) != null ? this.getRs().getString("comName" + national) : this.getRs().getString("kn"));

                    obj.put("ln", this.getRs().getString("ln"));
                    obj.put("kn", this.getRs().getString("kn"));
                    jsonArrLl[index++].add(obj);
                }

            }//end while (this.rs.next()) {
        } catch (SQLException ex) {
            Logger.getLogger(LiveMatchImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }
    }

    @Override
    public boolean delOldrecord(String dateStr) {
        this.connectDB();
        try {

            String query = "delete from live_league where new='N'";
            if (this.stmt.execute(query)) {
                System.out.println(query);
            }
//            String sql1 = "update live_league set new='N' where date='" + dateStr + "'";
//            //System.out.println(sql1);
//            if (this.stmt.execute(sql1)) {
//                System.out.println("update live_league " + dateStr + " to N");
//            }
        } catch (Exception ex) {
            Logger.getLogger(LiveLeagueImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }

        return true;
    }

    @Override
    public boolean setOldrecord(String dateStr) {
        this.connectDB();
        try {

//            String query = "delete from live_league where new='N'";
//            if (this.stmt.execute(query)) {
//                System.out.println(query);
//            }
            String sql1 = "update live_league set new='N' where date='" + dateStr + "'";
            //System.out.println(sql1);
            if (this.stmt.execute(sql1)) {
                System.out.println("update live_league " + dateStr + " to N");
            }
        } catch (Exception ex) {
            Logger.getLogger(LiveLeagueImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }

        return true;
    }

    @Override
    public Vector<LiveLeagueM> retrieveByChecked() {
        Vector<LiveLeagueM> vl = new Vector<LiveLeagueM>();
        LiveLeagueM l;
        connectDB();
        //connectServDB();
        String query = "select live_league.date,live_league.ln as ln ,live_league.subleagueId as subleagueId, live_league.subleagueName as subleagueName,live_league.competitionId as competitionId,live_league.leagueId as leagueId,competitions.competitionId as competitionId,competitions.competitionNamePk as competitionNamePk,competitions.competitionType as competitionType,league.leagueSeason as season,league.leagueNamePk as leagueNamePk"
                + " from  league "
                + " left join live_league on (live_league.leagueId=league.leagueId) "
                + " left join competitions on (competitions.competitionId=league.competitionId) "
                + "  left join live_league_checked on (live_league.leagueId = live_league_checked.leagueId and live_league.date = live_league_checked.date)"
                + " where live_league.date=current_date and live_league_checked.leagueId is null"
                + " order by(CASE WHEN league.priority IS NULL then 1 ELSE 0 END),league.priority ASC,league.leagueId"
                + " limit 20";
        //System.out.println(query);
        try {
            setRs(getStmt().executeQuery(query));
            while (getRs().next()) {
                l = new LiveLeagueM();
                l.setCompetitionId(getRs().getInt("competitionId"));
                l.setLeagueId(getRs().getInt("leagueId"));
                l.setCnPk(getRs().getString("competitionNamePk"));
                l.setCt(getRs().getString("competitionType"));
                l.setLs(getRs().getString("season"));
                l.setLnPk(getRs().getString("leagueNamePk"));
                l.setSubleagueName(getRs().getString("subleagueName"));
                l.setSubleagueId(getRs().getInt("subleagueId"));
                l.setLn(getRs().getString("ln"));
                l.setDate(getRs().getString("date"));
                vl.add(l);
            }

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();

        } finally {
            closeDB();
        }
        return vl;
    }

    @Override
    public void writeFile() {
        Vector<LeagueM> vc = new Vector<LeagueM>();
        LeagueM l = new LeagueM();
        connectDB();
        String query = "select competitionId from competitions";
        //System.out.println(query);
        Vector<String> vt = new Vector<String>();
        FileManager fileman = new FileManager();
        try {
            setRs(getStmt().executeQuery(query));
            while (getRs().next()) {
                vt.add(getRs().getString("competitionId"));
            }
            for (String s : vt) {
                String sql = "select league.leagueId,league.rid,league.competitionId,league.leagueNamePk,league.leagueName,league.leagueSeason,league.priority,lang_league.* from league "
                        + " left join stat_table on stat_table.leagueId = league.leagueId"
                        + " left join lang_league on lang_league.leagueId = league.leagueId"
                        + " where league.competitionId =" + s + " and stat_table.leagueId is not null"
                        + " group by league.leagueId";
                //System.out.println(sql);
                setRs(getStmt().executeQuery(sql));
                JSONObject json = new JSONObject();
                JSONArray array = new JSONArray();
                JSONArray jsonArray[] = new JSONArray[nation.length];
                for (int i = 0; i < jsonArray.length; i++) {
                    jsonArray[i] = new JSONArray();
                }
                while (getRs().next()) {
                    JSONObject obj = new JSONObject();
                    obj.put("leagueId", getRs().getString("leagueId"));
                    obj.put("rid", getRs().getString("rid"));
                    obj.put("competitionId", getRs().getString("competitionId"));
                    obj.put("leagueNamePk", getRs().getString("leagueNamePk"));
                    obj.put("leagueName", getRs().getString("leagueName"));
                    obj.put("leagueSeason", getRs().getString("leagueSeason"));
                    obj.put("priority", getRs().getString("priority"));
                    //array.add(obj);
                    int index = 0;
                    for (String national : nation) {
                        obj.put("leagueName", getRs().getString("leagueName" + national) != null ? getRs().getString("leagueName" + national) : getRs().getString("leagueName"));

                        jsonArray[index++].add(obj);

                    }

                }

                for (int i = 0; i < jsonArray.length; i++) {
                    json.put("list", jsonArray[i]);
                    //FileController.writeFile("../../leagueByCid/" + nation[i].toLowerCase() + "/" + s + ".json", json.toString());
                    fileman.setPath("../../leagueByCid/" + nation[i].toLowerCase());
                    fileman.setFilename(s + ".json");
                    fileman.setContent(json.toString());
                    fileman.write();
                    if (nation[i].toLowerCase().equals("en")) {
                        json.put("list", jsonArray[i]);
                        //FileController.writeFile("../../leagueByCid/" + s + ".json", json.toString());
                        fileman.setPath("../../leagueByCid");
                        fileman.setFilename(s + ".json");
                        fileman.setContent(json.toString());
                        fileman.write();
                    }
                }
                //
            }
        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();
        } finally {
            closeDB();
        }

    }

}
