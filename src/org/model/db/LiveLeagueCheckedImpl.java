/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.SQLException;
import org.model.LiveLeagueCheckedM;

/**
 *
 * @author mrsyrop
 */
public class LiveLeagueCheckedImpl extends DBConnection implements LiveLeagueCheckedDAO {

    @Override
    public boolean add(LiveLeagueCheckedM l) {
        boolean returnValue = false;
        connectDB();
        try {

            String sql = "insert into live_league_checked (leagueId,date) values(" + l.getLeagueId() + ",'" + l.getDate() + "')";
            int i = getStmt().executeUpdate(sql);
            if (0 != i) {
                returnValue = true;
            }

        } catch (Exception ex) {

            ex.printStackTrace();

        } finally {
            closeDB();
        }
        return returnValue;
    }

}
