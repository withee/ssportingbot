/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.manager.FileManager;
import org.manager.LogManager;
import org.model.CompetitionM;
import org.model.LiveMatchM;
import org.utility.Constants;

/**
 *
 * @author superx
 */
public class LiveMatchImpl extends DBConnection implements LiveMatchDAO {

    private static final LogManager log = new LogManager();
    private String table = "live_match";
    private String nation[] = {"Th", "En"};

    @Override
    public void getLiveMatch2JSONArrayStatus(JSONArray liveMatchArray,
            JSONArray[] jsonArrLm,
            int status) {

        String query = "";

        switch (status) {

            case Constants.LIVE_LEAGUE_STATUS_NONE:
                query = Constants.LIVE_MATCH_GET_DATA_WITH_TEAMS;
                break;
            case Constants.LIVE_MATCH_STATUS_1:
                query = Constants.LIVE_MATCH_GET_DATA_WITH_TEAMS_STATUS_1;
                break;
            case Constants.LIVE_MATCH_STATUS_5UP:
                query = Constants.LIVE_MATCH_GET_DATA_WITH_TEAMS_STATUS_5UP;
                break;

        }

        liveMatchArray = (null == liveMatchArray) ? new JSONArray() : liveMatchArray;
        jsonArrLm = (null == jsonArrLm) ? new JSONArray[Constants.NATION.length] : jsonArrLm;

        for (int i = 0; i < jsonArrLm.length; i++) {
            jsonArrLm[i] = new JSONArray();
        }

        String name = new Exception("is not thrown").getStackTrace()[0].getMethodName();
        name = this.toString() + name;
        this.connectDB(name);

        try {

            this.setRs(this.getStmt().executeQuery(query));
            log.write("info", "getLiveMatch2JSONArrayStatus query:" + query);
            while (this.getRs().next()) {
                JSONObject obj = new JSONObject();

                obj.put("mid", this.getRs().getString("mid"));
                obj.put("lid", this.getRs().getString("lid"));
                obj.put("_lid", this.getRs().getString("_lid"));
                obj.put("sid", this.getRs().getString("sid"));
                obj.put("kid", this.getRs().getString("kid"));
                obj.put("lnr", this.getRs().getString("lnr"));
                obj.put("c0", this.getRs().getString("c0"));
                obj.put("c1", this.getRs().getString("c1"));
                obj.put("c2", this.getRs().getString("c2"));
                obj.put("hid", this.getRs().getString("hid"));
                obj.put("gid", this.getRs().getString("gid"));
                obj.put("hn", this.getRs().getString("hn"));
                obj.put("gn", this.getRs().getString("gn"));
                obj.put("hrci", this.getRs().getString("hrci"));
                obj.put("grci", this.getRs().getString("grci"));
                obj.put("s1", this.getRs().getString("s1").replaceAll("w.o.", "").replaceAll("p\\.", ""));
                obj.put("s2", this.getRs().getString("s2").replaceAll("w.o.", "").replaceAll("p\\.", ""));
                obj.put("date", this.getRs().getString("date"));
                obj.put("cx", this.getRs().getString("cx"));
                liveMatchArray.add(obj);
                int index = 0;

                for (String national : Constants.NATION) {

                    //obj.put("gn", this.getRs().getString("gn" + national) != null ? this.getRs().getString("gn" + national) : this.getRs().getString("gn"));
                    //obj.put("hn", this.getRs().getString("hn" + national) != null ? this.getRs().getString("hn" + national) : this.getRs().getString("hn"));
                    obj.put("gn", this.getRs().getString("gn"));
                    obj.put("hn", this.getRs().getString("hn"));
                    jsonArrLm[index++].add(obj);

                }
            }//end while (this.rs.next()) {
        } catch (SQLException ex) {
            Logger.getLogger(LiveMatchImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }
    }

    @Override
    public boolean insertLiveMatch(LiveMatchM liveMatchM) {
        boolean success = false;
        try {
            this.connectDB();
            //String query = liveMatchM.getSqlInsertValue();
            PreparedStatement stm = liveMatchM.getSqlInsertValue(this.getConn());
            //System.out.println(stm.toString());
            success = stm.execute();
            String delOldmatch = "DELETE FROM timelines_game \n"
                    + "WHERE DATE(show_date) <> DATE(NOW())\n"
                    + "AND mid=" + liveMatchM.getMid();
            if(this.stmt.execute(delOldmatch)){
                log.write("nfo", "Deleted suspend match.");
            }
            
            String sql = "INSERT "
                    + "INTO `timelines_game` ("
                    + "mid,"
                    + "_lid,"
                    + "hid,"
                    + "gid,"
                    + "sid,"
                    + "show_date,"
                    + "score,"
                    + "homeName,"
                    + "awayName) values ("
                    + "?,"
                    + "?,"
                    + "?,"
                    + "?,"
                    + "?,"
                    + "?,"
                    + "?,"
                    + "?,"
                    + "?) "
                    + "ON DUPLICATE KEY UPDATE  "
                    + "sid=?,"
                    + "_lid=?";

            stm = this.getConn().prepareStatement(sql);
            stm.setInt(1, liveMatchM.getMid());
            stm.setInt(2, liveMatchM.getLid());
            stm.setInt(3, liveMatchM.getHid());
            stm.setInt(4, liveMatchM.getGid());
            stm.setInt(5, liveMatchM.getSid());
            stm.setString(6, liveMatchM.getShowDate());
            stm.setString(7, liveMatchM.getS1());
            stm.setString(8, liveMatchM.getHn());
            stm.setString(9, liveMatchM.getGn());
            stm.setInt(10, liveMatchM.getSid());
            stm.setInt(11, liveMatchM.getLid());
            boolean addtl = stm.execute();
            if (addtl) {
                //log.write("info", stm.toString());
            } else {
                log.write("info", "failed to execute:" + stm.toString());
            }
            ///System.out.println(stm.toString());
            //this.stmt.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(LiveFileImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }
        return success;
    }

    @Override
    public void getLiveMatch2JSONArrayStatus(JSONArray liveMatchArray, JSONArray[] jsonArrLm, String query) {
        liveMatchArray = (null == liveMatchArray) ? new JSONArray() : liveMatchArray;
        jsonArrLm = (null == jsonArrLm) ? new JSONArray[Constants.NATION.length] : jsonArrLm;

        for (int i = 0; i < jsonArrLm.length; i++) {
            jsonArrLm[i] = new JSONArray();
        }

        String name = new Exception("is not thrown").getStackTrace()[0].getMethodName();
        name = this.toString() + name;
        this.connectDB();
        //System.out.println("=========================================");
        //System.out.println(query);
        try {

            this.setRs(this.getStmt().executeQuery(query));
            while (this.getRs().next()) {
                JSONObject obj = new JSONObject();

                obj.put("mid", this.getRs().getString("mid"));
                obj.put("lid", this.getRs().getString("lid"));
                obj.put("_lid", this.getRs().getString("_lid"));
                obj.put("sid", this.getRs().getString("sid"));
                obj.put("kid", this.getRs().getString("kid"));
                obj.put("lnr", this.getRs().getString("lnr"));
                obj.put("c0", this.getRs().getString("c0"));
                obj.put("c1", this.getRs().getString("c1"));
                obj.put("c2", this.getRs().getString("c2"));
                obj.put("hid", this.getRs().getString("hid"));
                obj.put("gid", this.getRs().getString("gid"));
                obj.put("hn", this.getRs().getString("hn"));
                obj.put("gn", this.getRs().getString("gn"));
                obj.put("hrci", this.getRs().getString("hrci"));
                obj.put("grci", this.getRs().getString("grci"));
                obj.put("s1", this.getRs().getString("s1").replaceAll("w.o.", "").replaceAll("p\\.", ""));
                obj.put("s2", this.getRs().getString("s2").replaceAll("w.o.", "").replaceAll("p\\.", ""));
                obj.put("date", this.getRs().getString("date"));
                obj.put("cx", this.getRs().getString("cx"));
                //System.out.println("_lid=====>"+obj.getString("_lid"));
                liveMatchArray.add(obj);
                int index = 0;

                for (String national : Constants.NATION) {

                    //obj.put("gn", this.getRs().getString("gn" + national) != null ? this.getRs().getString("gn" + national) : this.getRs().getString("gn"));
                    //obj.put("hn", this.getRs().getString("hn" + national) != null ? this.getRs().getString("hn" + national) : this.getRs().getString("hn"));
                    obj.put("gn", this.getRs().getString("gn"));
                    obj.put("hn", this.getRs().getString("hn"));
                    jsonArrLm[index++].add(obj);

                }
            }//end while (this.rs.next()) {
            //System.out.println("================================");
        } catch (SQLException ex) {
            Logger.getLogger(LiveMatchImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }
    }

    @Override
    public Vector<LiveMatchM> getLiveMatchWithout11() {
        Vector<LiveMatchM> liveMatchMvt = new Vector();
        connectDB();

        //System.out.println(query);
        try {
            rs = stmt.executeQuery(Constants.LIVE_MATCH_WITHOUT_11);
            while (rs.next()) {
                LiveMatchM l = new LiveMatchM();
                l.setMid(rs.getInt("mid"));
                l.setAo(rs.getInt("ao"));
                l.setEvent_id(rs.getString("event_id"));
                liveMatchMvt.add(l);
            }
//            closeDB();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            this.closeDB();
            log.write("info", "Found " + liveMatchMvt.size() + " event");
        }
        return liveMatchMvt;
    }

    @Override
    public Vector<LiveMatchM> getLiveMatchLastEvent() {
        Vector<LiveMatchM> liveMatchMvt = new Vector();
        connectDB();

        //System.out.println(query);
        try {
            rs = stmt.executeQuery(Constants.LIVE_MATCH_LAST_EVENT);
            while (rs.next()) {
                LiveMatchM l = new LiveMatchM();
                l.setMid(rs.getInt("mid"));
                l.setAo(rs.getInt("ao"));
                l.setEvent_id(rs.getString("event_id"));
                liveMatchMvt.add(l);
            }
//            closeDB();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            this.closeDB();
        }
        return liveMatchMvt;
    }

    @Override
    public Vector<LiveMatchM> getLiveMatchResultEvent() {
        Vector<LiveMatchM> liveMatchMvt = new Vector();
        connectDB();

        //System.out.println(query);
        try {
            rs = stmt.executeQuery(Constants.LIVE_MATCH_RESULT_EVENT);
            while (rs.next()) {
                LiveMatchM l = new LiveMatchM();
                l.setMid(rs.getInt("mid"));
                l.setAo(rs.getInt("ao"));
                liveMatchMvt.add(l);
            }
//            closeDB();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            this.closeDB();
        }
        return liveMatchMvt;
    }

    @Override
    public boolean delOldlivematch(String dateStr) {
        this.connectDB();
        try {
            String query = "delete from live_match where new='N'";
            this.stmt.execute(query);
//            String sql2 = "update live_match set new='N' where showDate='" + dateStr + "'";
//            this.stmt.execute(sql2);

        } catch (SQLException ex) {
            Logger.getLogger(LiveMatchImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }
        return true;
    }

    @Override
    public boolean setOldlivematch(String dateStr) {
        this.connectDB();
        try {
//            String query = "delete from live_match where new='N'";
//            this.stmt.execute(query);
            String sql2 = "update live_match set new='N' where showDate='" + dateStr + "'";
            this.stmt.execute(sql2);

        } catch (SQLException ex) {
            Logger.getLogger(LiveMatchImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }
        return true;
    }

    @Override
    public Vector<LiveMatchM> retrieveByLiveMatchChecked() {
        Vector<LiveMatchM> liveMatchvtr = new Vector<LiveMatchM>();
        LiveMatchM l;
        this.connectDB();
        //this.connectServDB();
        String query = "select live_match.* from live_match"
                + " left join live_match_checked on live_match.showDate=live_match_checked.date and live_match.mid=live_match_checked.mid"
                //                + " left join league on league.leagueId=live_match.lid"
                + " left join live_league_checked on live_match.lid = live_league_checked.leagueId and live_league_checked.date = current_date "
                + " where live_match.showDate=current_date  and live_match_checked.mid is null and live_league_checked.leagueId is not null\n"
                //                + " order by (CASE WHEN league.priority IS NULL then 1 ELSE 0 END),league.priority ASC\n"
                + "limit 20";
        System.out.println(query);
        try {
            this.rs = this.stmt.executeQuery(query);
            while (this.rs.next()) {
                l = new LiveMatchM();
                l.setMid(this.rs.getInt("mid"));
                l.setLid(this.rs.getInt("lid"));
                l.setSlid(this.rs.getInt("_lid"));
                l.setOid(this.rs.getInt("oid"));
                l.setSid(this.rs.getInt("sid"));
                l.setKid(this.rs.getInt("kid"));
                l.setLnr(this.rs.getInt("lnr"));
                l.setC0(this.rs.getInt("c0"));
                l.setC1(this.rs.getInt("c1"));
                l.setC2(this.rs.getInt("c2"));
                l.setMl(this.rs.getInt("ml"));
                l.setHid(this.rs.getInt("hid"));
                l.setGid(this.rs.getInt("gid"));
                l.setHn(this.rs.getString("hn"));
                l.setGn(this.rs.getString("gn"));
                l.setGrci(this.rs.getInt("grci"));
                l.setHrci(this.rs.getInt("hrci"));
                l.setGrc(this.rs.getString("grc"));
                l.setHrc(this.rs.getString("hrc"));
                l.setS1(this.rs.getString("s1"));
                l.setS2(this.rs.getString("s2"));
                l.setMstan(this.rs.getInt("mstan"));
                l.setOtv(this.rs.getString("otv"));
                l.setHstan(this.rs.getInt("hstan"));
                l.setGstan(this.rs.getInt("gstan"));
                l.setGoli(this.rs.getInt("golI"));
                l.setL(this.rs.getInt("l"));
                l.setA(this.rs.getInt("a"));
                l.setAo(this.rs.getInt("ao"));
                l.setCx(this.rs.getInt("cx"));
                l.setInfo(this.rs.getString("info"));
                l.setRek_h(this.rs.getInt("rek_h"));
                l.setDate(this.rs.getString("date"));
                l.setShowDate(this.rs.getString("showDate"));
                if (l.getLid() != l.getSlid()) {
                    int leageuId = l.getSlid();
                    l.setSlid(l.getLid());
                    l.setLid(leageuId);
                }
                liveMatchvtr.add(l);
            }

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();
        } finally {
            this.closeDB();
        }

        return liveMatchvtr;
    }

    @Override
    public Vector<LiveMatchM> getLiveToday() {
        Vector<LiveMatchM> liveMatchvtr = new Vector<LiveMatchM>();
        LiveMatchM l;
        this.connectDB();
        String query = "SELECT * FROM live_match lm\n"
                + "WHERE lm.showDate>=DATE(NOW())";

        try {
            this.rs = this.stmt.executeQuery(query);
            while (this.rs.next()) {
                l = new LiveMatchM();
                l.setMid(this.rs.getInt("mid"));
                l.setLid(this.rs.getInt("lid"));
                l.setSlid(this.rs.getInt("_lid"));
                l.setOid(this.rs.getInt("oid"));
                l.setSid(this.rs.getInt("sid"));
                l.setKid(this.rs.getInt("kid"));
                l.setLnr(this.rs.getInt("lnr"));
                l.setC0(this.rs.getInt("c0"));
                l.setC1(this.rs.getInt("c1"));
                l.setC2(this.rs.getInt("c2"));
                l.setMl(this.rs.getInt("ml"));
                l.setHid(this.rs.getInt("hid"));
                l.setGid(this.rs.getInt("gid"));
                l.setHn(this.rs.getString("hn"));
                l.setGn(this.rs.getString("gn"));
                l.setGrci(this.rs.getInt("grci"));
                l.setHrci(this.rs.getInt("hrci"));
                l.setGrc(this.rs.getString("grc"));
                l.setHrc(this.rs.getString("hrc"));
                l.setS1(this.rs.getString("s1"));
                l.setS2(this.rs.getString("s2"));
                l.setMstan(this.rs.getInt("mstan"));
                l.setOtv(this.rs.getString("otv"));
                l.setHstan(this.rs.getInt("hstan"));
                l.setGstan(this.rs.getInt("gstan"));
                l.setGoli(this.rs.getInt("golI"));
                l.setL(this.rs.getInt("l"));
                l.setA(this.rs.getInt("a"));
                l.setAo(this.rs.getInt("ao"));
                l.setCx(this.rs.getInt("cx"));
                l.setInfo(this.rs.getString("info"));
                l.setRek_h(this.rs.getInt("rek_h"));
                l.setDate(this.rs.getString("date"));
                l.setShowDate(this.rs.getString("showDate"));
                if (l.getLid() != l.getSlid()) {
                    int leageuId = l.getSlid();
                    l.setSlid(l.getLid());
                    l.setLid(leageuId);
                }
                liveMatchvtr.add(l);
            }

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();
        } finally {
            this.closeDB();
        }

        return liveMatchvtr;
    }

    @Override
    public String[] getInfo(int mid, int team) {
        //team = 0 when home team=1 when guest 
        String[] info = new String[2];
        FileManager fileman = new FileManager();
        fileman.setFilename(mid + ".json");
        fileman.setPath(Constants.BALL24_TEAM_PK);
        //System.out.println(fileman.getFullpath());
        if (FileManager.isFile(fileman)) {
            try {
                JSONParser parser = new JSONParser();
                org.json.simple.JSONObject jsonObj = new org.json.simple.JSONObject();
                Object obj = parser.parse(fileman.read());
                jsonObj = (org.json.simple.JSONObject) obj;
                //System.out.println(jsonObj.get("cnPk"));
                info[0] = (String) jsonObj.get("cnPk");
                if (0 == team) {
                    info[1] = (String) jsonObj.get("hnPk");
                } else {
                    info[1] = (String) jsonObj.get("gnPk");
                }
            } catch (ParseException ex) {
                Logger.getLogger(CompetitionImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            //System.out.println(fileman.getFullpath() + " not found");
            info[0] = null;
        }

        return info;

    }

    @Override
    public Vector<LiveMatchM> retrieve() {
        Vector<LiveMatchM> vc = new Vector<LiveMatchM>();
        LiveMatchM l;
        connectDB();
        String query = "select *from " + table + " where showDate=current_date";
        //System.out.println(query);
        try {
            setRs(getStmt().executeQuery(query));
            while (getRs().next()) {
                l = new LiveMatchM();
                l.setMid(getRs().getInt("mid"));
                l.setLid(getRs().getInt("lid"));
                l.setSlid(getRs().getInt("_lid"));
                l.setOid(getRs().getInt("oid"));
                l.setSid(getRs().getInt("sid"));
                l.setKid(getRs().getInt("kid"));
                l.setLnr(getRs().getInt("lnr"));
                l.setC0(getRs().getInt("c0"));
                l.setC1(getRs().getInt("c1"));
                l.setC2(getRs().getInt("c2"));
                l.setMl(getRs().getInt("ml"));
                l.setHid(getRs().getInt("hid"));
                l.setGid(getRs().getInt("gid"));
                l.setHn(getRs().getString("hn"));
                l.setGn(getRs().getString("gn"));
                l.setGrci(getRs().getInt("grci"));
                l.setHrci(getRs().getInt("hrci"));
                l.setGrc(getRs().getString("grc"));
                l.setHrc(getRs().getString("hrc"));
                l.setS1(getRs().getString("s1"));
                l.setS2(getRs().getString("s2"));
                l.setMstan(getRs().getInt("mstan"));
                l.setOtv(getRs().getString("otv"));
                l.setHstan(getRs().getInt("hstan"));
                l.setGstan(getRs().getInt("gstan"));
                l.setGoli(getRs().getInt("golI"));
                l.setL(getRs().getInt("l"));
                l.setA(getRs().getInt("a"));
                l.setAo(getRs().getInt("ao"));
                l.setCx(getRs().getInt("cx"));
                l.setInfo(getRs().getString("info"));
                l.setRek_h(getRs().getInt("rek_h"));
                l.setDate(getRs().getString("date"));
                l.setShowDate(getRs().getString("showDate"));
                if (l.getLid() != l.getSlid()) {
                    int leageuId = l.getSlid();
                    l.setSlid(l.getLid());
                    l.setLid(leageuId);
                }
                vc.add(l);
            }

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();

        } finally {
            closeDB();
        }
        return vc;
    }

}
