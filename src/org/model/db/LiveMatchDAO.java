/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.SQLException;
import java.util.Vector;
import net.sf.json.JSONArray;
import org.model.LiveMatchM;
import org.utility.Constants;

/**
 *
 * @author superx
 */
public interface LiveMatchDAO {

    public void getLiveMatch2JSONArrayStatus(JSONArray liveMatchArray,
            JSONArray[] jsonArrLm,
            int status);

    public void getLiveMatch2JSONArrayStatus(JSONArray liveMatchArray,
            JSONArray[] jsonArrLm,
            String query);

    public boolean insertLiveMatch(LiveMatchM liveMatchM);

    public boolean delOldlivematch(String dateStr);

    public boolean setOldlivematch(String dateStr);

    public Vector<LiveMatchM> getLiveMatchWithout11();

    public Vector<LiveMatchM> getLiveMatchLastEvent();

    public Vector<LiveMatchM> getLiveMatchResultEvent();

    public Vector<LiveMatchM> retrieveByLiveMatchChecked();

    public Vector<LiveMatchM> getLiveToday();

    public String[] getInfo(int mid, int team);

    public Vector<LiveMatchM> retrieve();

}
