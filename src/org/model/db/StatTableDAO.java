/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.db;

import java.sql.SQLException;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.model.StatTableM;

/**
 *
 * @author mrsyrop
 */
public interface StatTableDAO {

    public boolean add(StatTableM st[]);

    public boolean add(StatTableM st[], String side);
    public void writeFile(int leagueId);
}
