/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

/**
 *
 * @author mrsyrop
 */
public class StatTableM {
    private int no;
    private int gp;
    private int pts;
    private int w;
    private int d;
    private int l;
    private int gf;
    private int ga;
    private int plusminus;
    private int cid;
    private String tnPk;
    private String ml;
    private String leagueNamePk;
    private int leagueId;
    private String subLeagueNamePk;
    private String subLeagueId;
    private int tid;

    /**
     * @return the no
     */
    public int getNo() {
        return no;
    }

    /**
     * @param no the no to set
     */
    public void setNo(int no) {
        this.no = no;
    }

    /**
     * @return the gp
     */
    public int getGp() {
        return gp;
    }

    /**
     * @param gp the gp to set
     */
    public void setGp(int gp) {
        this.gp = gp;
    }

    /**
     * @return the pts
     */
    public int getPts() {
        return pts;
    }

    /**
     * @param pts the pts to set
     */
    public void setPts(int pts) {
        this.pts = pts;
    }

    /**
     * @return the w
     */
    public int getW() {
        return w;
    }

    /**
     * @param w the w to set
     */
    public void setW(int w) {
        this.w = w;
    }

    /**
     * @return the d
     */
    public int getD() {
        return d;
    }

    /**
     * @param d the d to set
     */
    public void setD(int d) {
        this.d = d;
    }

    /**
     * @return the l
     */
    public int getL() {
        return l;
    }

    /**
     * @param l the l to set
     */
    public void setL(int l) {
        this.l = l;
    }

    /**
     * @return the gf
     */
    public int getGf() {
        return gf;
    }

    /**
     * @param gf the gf to set
     */
    public void setGf(int gf) {
        this.gf = gf;
    }

    /**
     * @return the ga
     */
    public int getGa() {
        return ga;
    }

    /**
     * @param ga the ga to set
     */
    public void setGa(int ga) {
        this.ga = ga;
    }

    /**
     * @return the plusminus
     */
    public int getPlusminus() {
        return plusminus;
    }

    /**
     * @param plusminus the plusminus to set
     */
    public void setPlusminus(int plusminus) {
        this.plusminus = plusminus;
    }

    /**
     * @return the cid
     */
    public int getCid() {
        return cid;
    }

    /**
     * @param cid the cid to set
     */
    public void setCid(int cid) {
        this.cid = cid;
    }

    /**
     * @return the tnPk
     */
    public String getTnPk() {
        return tnPk;
    }

    /**
     * @param tnPk the tnPk to set
     */
    public void setTnPk(String tnPk) {
        this.tnPk = tnPk;
    }

    /**
     * @return the ml
     */
    public String getMl() {
        return ml;
    }

    /**
     * @param ml the ml to set
     */
    public void setMl(String ml) {
        this.ml = ml;
    }

    /**
     * @return the leagueNamePk
     */
    public String getLeagueNamePk() {
        return leagueNamePk;
    }

    /**
     * @param leagueNamePk the leagueNamePk to set
     */
    public void setLeagueNamePk(String leagueNamePk) {
        this.leagueNamePk = leagueNamePk;
    }

    /**
     * @return the leagueId
     */
    public int getLeagueId() {
        return leagueId;
    }

    /**
     * @param leagueId the leagueId to set
     */
    public void setLeagueId(int leagueId) {
        this.leagueId = leagueId;
    }

    /**
     * @return the subLeagueNamePk
     */
    public String getSubLeagueNamePk() {
        return subLeagueNamePk;
    }

    /**
     * @param subLeagueNamePk the subLeagueNamePk to set
     */
    public void setSubLeagueNamePk(String subLeagueNamePk) {
        this.subLeagueNamePk = subLeagueNamePk;
    }

    /**
     * @return the subLeagueId
     */
    public String getSubLeagueId() {
        return subLeagueId;
    }

    /**
     * @param subLeagueId the subLeagueId to set
     */
    public void setSubLeagueId(String subLeagueId) {
        this.subLeagueId = subLeagueId;
    }

    /**
     * @return the tid
     */
    public int getTid() {
        return tid;
    }

    /**
     * @param tid the tid to set
     */
    public void setTid(int tid) {
        this.tid = tid;
    }
}
