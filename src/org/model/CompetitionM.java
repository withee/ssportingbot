/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mrsyrop
 */
public class CompetitionM {

    private int competitionId = 0;
    private String competitionType;
    private String competitionName;
    private String competitionNamePk;
    private String leagueNamePk;
    private String season;
    private String competitionNameTh;
    private String array[];

    public PreparedStatement getSqlInsertValue(Connection conn) {

        PreparedStatement stm = null;
        String query = "INSERT INTO `competitions` (`competitionId`, `competitionType`, `competitionName`, `competitionNamePk`, `leagueNamePk`, `season`) \n"
                + "VALUES (?, ?, ?, ?, ?, ?)\n"
                + "ON DUPLICATE KEY UPDATE    \n"
                + "`competitionType`=?, "
                + "`competitionName`=?, "
                + "`competitionNamePk`=?, "
                + "`leagueNamePk`=?, "
                + "`season`=?";
        try {
            stm = conn.prepareStatement(query);
            stm.setInt(1, this.getCompetitionId());
            stm.setString(2, this.getCompetitionType());
            stm.setString(3, this.getCompetitionName());
            stm.setString(4, this.getCompetitionNamePk());
            stm.setString(5, this.getLeagueNamePk());
            stm.setString(6, this.getSeason());
            stm.setString(7, this.getCompetitionType());
            stm.setString(8, this.getCompetitionName());
            stm.setString(9, this.getCompetitionNamePk());
            stm.setString(10, this.getLeagueNamePk());
            stm.setString(11, this.getSeason());

        } catch (SQLException ex) {
            Logger.getLogger(LiveMatchM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stm;
    }

    public CompetitionM() {
        this.competitionId = 0;
    }

    /**
     * @return the competitionId
     */
    public int getCompetitionId() {
        return competitionId;
    }

    /**
     * @param competitionId the competitionId to set
     */
    public void setCompetitionId(int competitionId) {
        this.competitionId = competitionId;
    }

    /**
     * @return the competitionType
     */
    public String getCompetitionType() {
        return competitionType;
    }

    /**
     * @param competitionType the competitionType to set
     */
    public void setCompetitionType(String competitionType) {
        this.competitionType = competitionType;
    }

    /**
     * @return the competitionName
     */
    public String getCompetitionName() {
        return competitionName;
    }

    /**
     * @param competitionName the competitionName to set
     */
    public void setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
    }

    /**
     * @return the competitionNamePk
     */
    public String getCompetitionNamePk() {
        return competitionNamePk;
    }

    /**
     * @param competitionNamePk the competitionNamePk to set
     */
    public void setCompetitionNamePk(String competitionNamePk) {
        this.competitionNamePk = competitionNamePk;
    }

    /**
     * @return the leagueNamePk
     */
    public String getLeagueNamePk() {
        return leagueNamePk;
    }

    /**
     * @param leagueNamePk the leagueNamePk to set
     */
    public void setLeagueNamePk(String leagueNamePk) {
        this.leagueNamePk = leagueNamePk;
    }

    /**
     * @return the season
     */
    public String getSeason() {
        return season;
    }

    /**
     * @param season the season to set
     */
    public void setSeason(String season) {
        this.season = season;
    }

    /**
     * @return the competitionNameTh
     */
    public String getCompetitionNameTh() {
        return competitionNameTh;
    }

    /**
     * @param competitionNameTh the competitionNameTh to set
     */
    public void setCompetitionNameTh(String competitionNameTh) {
        this.competitionNameTh = competitionNameTh;
    }

    /**
     * @return the array
     */
    public String[] getArray() {
        return array;
    }

    /**
     * @param array the array to set
     */
    public void setArray(String[] array) {
        this.array = array;
    }

}
