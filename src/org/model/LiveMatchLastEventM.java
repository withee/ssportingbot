/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author superx
 */
public class LiveMatchLastEventM {

    private String id = "";
    private String zdid = "";
    private String zdrid = "";
    private String mid = "";
    private String zdz = "";
    private String a = "";
    private String hn = "";
    private String gn = "";
    private String cm = "";
    private String s = "";
    private String datetime = "";

    public void setValueByKey(String key, String value) {

        switch (key) {
            case "id":
                this.id = value;
                break;
            case "zdid":
                this.zdid = value;
                break;
            case "zdrid":
                this.zdrid = value;
                break;
            case "mid":
                this.mid = value;
                break;
            case "zdz":
                this.zdz = value;
                break;
            case "a":
                this.a = value;
                break;
            case "hn":
                this.hn = value;
                break;
            case "gn":
                this.gn = value;
                break;
            case "cm":
                this.cm = value;
                break;
            case "s":
                this.s = value;
                break;
            case "datetime":
                this.datetime = value;
                break;
        }

    }

    public void setValueByKey(HashMap<String, String> map) {
        for (Map.Entry<String, String> attr : map.entrySet()) {
            this.setValueByKey(attr.getKey(), attr.getValue());
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getZdid() {
        return zdid;
    }

    public void setZdid(String zdid) {
        this.zdid = zdid;
    }

    public String getZdrid() {
        return zdrid;
    }

    public void setZdrid(String zdrid) {
        this.zdrid = zdrid;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getZdz() {
        return zdz;
    }

    public void setZdz(String zdz) {
        this.zdz = zdz;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getHn() {
        return hn;
    }

    public void setHn(String hn) {
        this.hn = hn;
    }

    public String getGn() {
        return gn;
    }

    public void setGn(String gn) {
        this.gn = gn;
    }

    public String getCm() {
        return cm;
    }

    public void setCm(String cm) {
        this.cm = cm;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

}
