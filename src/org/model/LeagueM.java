/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

/**
 *
 * @author mrsyrop
 */
public class LeagueM {
    private int rid;
    private int leagueId;
    private int competitionId;
    private String leagueNamePk;
    private String leagueName;
    private String leagueNameTh;
    private String leagueSeason;
    private String competitionType;
    private String competitionNamePk;
    private String leaguePriority;
    private String priority;

    /**
     * @return the rid
     */
    public int getRid() {
        return rid;
    }

    /**
     * @param rid the rid to set
     */
    public void setRid(int rid) {
        this.rid = rid;
    }

    /**
     * @return the leagueId
     */
    public int getLeagueId() {
        return leagueId;
    }

    /**
     * @param leagueId the leagueId to set
     */
    public void setLeagueId(int leagueId) {
        this.leagueId = leagueId;
    }

    /**
     * @return the competitionId
     */
    public int getCompetitionId() {
        return competitionId;
    }

    /**
     * @param competitionId the competitionId to set
     */
    public void setCompetitionId(int competitionId) {
        this.competitionId = competitionId;
    }

    /**
     * @return the leagueNamePk
     */
    public String getLeagueNamePk() {
        return leagueNamePk;
    }

    /**
     * @param leagueNamePk the leagueNamePk to set
     */
    public void setLeagueNamePk(String leagueNamePk) {
        this.leagueNamePk = leagueNamePk;
    }

    /**
     * @return the leagueName
     */
    public String getLeagueName() {
        return leagueName;
    }

    /**
     * @param leagueName the leagueName to set
     */
    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    /**
     * @return the leagueNameTh
     */
    public String getLeagueNameTh() {
        return leagueNameTh;
    }

    /**
     * @param leagueNameTh the leagueNameTh to set
     */
    public void setLeagueNameTh(String leagueNameTh) {
        this.leagueNameTh = leagueNameTh;
    }

    /**
     * @return the leagueSeason
     */
    public String getLeagueSeason() {
        return leagueSeason;
    }

    /**
     * @param leagueSeason the leagueSeason to set
     */
    public void setLeagueSeason(String leagueSeason) {
        this.leagueSeason = leagueSeason;
    }

    /**
     * @return the competitionType
     */
    public String getCompetitionType() {
        return competitionType;
    }

    /**
     * @param competitionType the competitionType to set
     */
    public void setCompetitionType(String competitionType) {
        this.competitionType = competitionType;
    }

    /**
     * @return the competitionNamePk
     */
    public String getCompetitionNamePk() {
        return competitionNamePk;
    }

    /**
     * @param competitionNamePk the competitionNamePk to set
     */
    public void setCompetitionNamePk(String competitionNamePk) {
        this.competitionNamePk = competitionNamePk;
    }

    /**
     * @return the leaguePriority
     */
    public String getLeaguePriority() {
        return leaguePriority;
    }

    /**
     * @param leaguePriority the leaguePriority to set
     */
    public void setLeaguePriority(String leaguePriority) {
        this.leaguePriority = leaguePriority;
    }

    /**
     * @return the priority
     */
    public String getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(String priority) {
        this.priority = priority;
    }
}
