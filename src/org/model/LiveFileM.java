/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

import org.utility.DateUtility;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.utility.IntegerUtility;

/**
 *
 * @author superx
 */
public class LiveFileM {

    private String date = "";
    private String time = "";
    private String plinkXML = "";
    private int ml00 = 0;
    private int ml01 = 0;
    private int ml02 = 0;
    private int ml10 = 0;
    private int ml11 = 0;
    private int ml12 = 0;
    private int ma = 0;

    public void setValueByKey(String key, String value) {
        switch (key) {
            case "d":
                this.date = DateUtility.convertYYYYMMDD2yyyy_MM_dd(value);
                break;
            case "dcal":
                this.date = DateUtility.convertYYYYMMDD2yyyy_MM_dd(value);
                break;
            case "m":
                this.time = value;
                break;
            case "plikxml":
                this.plinkXML = value;
                break;
            case "ml00":
                this.ml00 = IntegerUtility.string2Int(value);
                break;
            case "ml01":
                this.ml01 = IntegerUtility.string2Int(value);
                break;
            case "ml02":
                this.ml02 = IntegerUtility.string2Int(value);
                break;
            case "ml10":
                this.ml10 = IntegerUtility.string2Int(value);
                break;
            case "ml11":
                this.ml11 = IntegerUtility.string2Int(value);
                break;
            case "ml12":
                this.ml12 = IntegerUtility.string2Int(value);
                break;
            case "ma":
                this.ma = IntegerUtility.string2Int(value);
                break;
        }

    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getPlinkXML() {
        return plinkXML;
    }

    public int getMl00() {
        return ml00;
    }

    public int getMl01() {
        return ml01;
    }

    public int getMl02() {
        return ml02;
    }

    public int getMl10() {
        return ml10;
    }

    public int getMl11() {
        return ml11;
    }

    public int getMl12() {
        return ml12;
    }

    public int getMa() {
        return ma;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setPlinkXML(String plinkXML) {
        this.plinkXML = plinkXML;
    }

    public void setMl00(int ml00) {
        this.ml00 = ml00;
    }

    public void setMl01(int ml01) {
        this.ml01 = ml01;
    }

    public void setMl02(int ml02) {
        this.ml02 = ml02;
    }

    public void setMl10(int ml10) {
        this.ml10 = ml10;
    }

    public void setMl11(int ml11) {
        this.ml11 = ml11;
    }

    public void setMl12(int ml12) {
        this.ml12 = ml12;
    }

    public void setMa(int ma) {
        this.ma = ma;
    }

    public void displayData() {

        System.out.println("Object Name : LiveFileB");
        System.out.println("date : " + this.date);
        System.out.println("time : " + this.time);
        System.out.println("plinkXML : " + this.plinkXML);
        System.out.println("ml00 : " + this.ml00);
        System.out.println("ml01 : " + this.ml01);
        System.out.println("ml02 : " + this.ml02);
        System.out.println("ml10 : " + this.ml10);
        System.out.println("ml11 : " + this.ml11);
        System.out.println("ml12 : " + this.ml12);
        System.out.println("ma : " + this.ma);

    }

    public String displayData2String() {

        String str = "\n";
        str += "Object Name : LiveFileB\n";;
        str += "date : " + this.date + "\n";
        str += "time : " + this.time + "\n";
        str += "plinkXML : " + this.plinkXML + "\n";
        str += "ml00 : " + this.ml00 + "\n";
        str += "ml01 : " + this.ml01 + "\n";
        str += "ml02 : " + this.ml02 + "\n";
        str += "ml10 : " + this.ml10 + "\n";
        str += "ml11 : " + this.ml11 + "\n";
        str += "ml12 : " + this.ml12 + "\n";
        str += "ma : " + this.ma + "\n";

        return str;
    }

}
