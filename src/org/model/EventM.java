/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mrsyrop
 */
public class EventM {

    private int aid;
    private String m;
    private String m2;
    private int mr;
    private String pz;
    private String ph;
    private String pg;
    private String pw;
    private String h;
    private String g;
    private int mid;

    public EventM() {
        this.aid = 0;
    }

    public void setValueByAttr(String key, String value) {
        switch (key) {
            case "aid":
                this.aid = Integer.parseInt(value);
                break;
            case "m":
                this.m = value;
                break;
            case "m2":
                this.m2 = value;
                break;
            case "mr":
                this.mr = Integer.parseInt(value);
                break;
            case "pz":
                this.pz = value;
                break;
            case "ph":
                this.ph = value;
                break;
            case "pg":
                this.pg = value;
                break;
            case "pw":
                this.pw = value;
                break;
            case "h":
                this.h = value;
                break;
            case "g":
                this.g = value;
                break;
            default:
                this.mid = 0;
                break;
        }

    }

    public void setValueByAttr(HashMap<String, String> map) {
        for (Map.Entry<String, String> attr : map.entrySet()) {
            this.setValueByAttr(attr.getKey(), attr.getValue());
        }
    }

    /**
     * @return the aid
     */
    public int getAid() {
        return aid;
    }

    /**
     * @param aid the aid to set
     */
    public void setAid(int aid) {
        this.aid = aid;
    }

    /**
     * @return the m
     */
    public String getM() {
        return m;
    }

    /**
     * @param m the m to set
     */
    public void setM(String m) {
        this.m = m;
    }

    /**
     * @return the m2
     */
    public String getM2() {
        return m2;
    }

    /**
     * @param m2 the m2 to set
     */
    public void setM2(String m2) {
        this.m2 = m2;
    }

    /**
     * @return the mr
     */
    public int getMr() {
        return mr;
    }

    /**
     * @param mr the mr to set
     */
    public void setMr(int mr) {
        this.mr = mr;
    }

    /**
     * @return the pz
     */
    public String getPz() {
        return pz;
    }

    /**
     * @param pz the pz to set
     */
    public void setPz(String pz) {
        this.pz = pz;
    }

    /**
     * @return the ph
     */
    public String getPh() {
        return ph;
    }

    /**
     * @param ph the ph to set
     */
    public void setPh(String ph) {
        this.ph = ph;
    }

    /**
     * @return the pg
     */
    public String getPg() {
        return pg;
    }

    /**
     * @param pg the pg to set
     */
    public void setPg(String pg) {
        this.pg = pg;
    }

    /**
     * @return the pw
     */
    public String getPw() {
        return pw;
    }

    /**
     * @param pw the pw to set
     */
    public void setPw(String pw) {
        this.pw = pw;
    }

    /**
     * @return the h
     */
    public String getH() {
        return h;
    }

    /**
     * @param h the h to set
     */
    public void setH(String h) {
        this.h = h;
    }

    /**
     * @return the g
     */
    public String getG() {
        return g;
    }

    /**
     * @param g the g to set
     */
    public void setG(String g) {
        this.g = g;
    }

    /**
     * @return the mid
     */
    public int getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(int mid) {
        this.mid = mid;
    }
}
