/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.manager;

import java.sql.SQLException;
import java.util.Vector;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.model.LiveMatchLastEventM;
import org.model.db.DBConnection;

/**
 *
 * @author superx
 */
public class JSonManager extends DBConnection {

    public boolean addLastEvent(Vector<LiveMatchLastEventM> vl) {
        boolean returnValue = false;
        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();
        java.util.Date date = new java.util.Date();
        String name = new Exception("is not thrown").getStackTrace()[0].getMethodName();
        name = this.toString() + name;
        connectDB(name);
        try {

//            connectDB();
            for (LiveMatchLastEventM l : vl) {

                String querylm = "SELECT * FROM live_match \n"
                        + "left join live_league on live_league.leagueId =live_match._lid\n"
                        + "WHERE mid='" + l.getMid() + "'";
                this.rs = this.stmt.executeQuery(querylm);
                int lid = 0;
                int _lid = 0;
                String kkod = "";
                if (this.rs.next()) {
                    _lid = this.rs.getInt("_lid");
                    lid = this.rs.getInt("lid");
                    kkod = this.rs.getString("kkod");
                    //System.out.println(_lid + ":" + kkod);
                }
                returnValue = true;
                JSONObject obj = new JSONObject();
                obj.put("zdid", l.getZdid());
                obj.put("zdrid", l.getZdrid());
                obj.put("mid", l.getMid());
                obj.put("zdz", l.getZdz());
                obj.put("a", l.getA());
                obj.put("hn", l.getHn());
                obj.put("gn", l.getGn());
                obj.put("cm", l.getCm());
                obj.put("s", l.getS());
                obj.put("kkod", kkod);
                array.add(obj);
                //System.out.println(kkod);
            }
//            closeDB();
            json.put("c3", (date.getTime() / 1000));
            json.put("list", array);
            //FileController.writeFile("../../cronjob_gen_file/files/liveMatchLastEvent.json", json.toString());
            FileManager fileManager = new FileManager();
            fileManager.setPath("../../cronjob_gen_file/files");
            fileManager.setFilename("liveMatchLastEvent.json");
            fileManager.setContent(json.toString());
            fileManager.write();

        } catch (Exception ex) {
        } finally {
            closeDB(name);
        }
        return returnValue;
    }
}
