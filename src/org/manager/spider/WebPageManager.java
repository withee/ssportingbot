/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.manager.spider;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.manager.FileManager;
import org.manager.LogManager;
import org.utility.DateUtility;

/**
 *
 * @author superx
 */
public class WebPageManager {

    public Document getDocXMLByUrl(String url) {
        Document document = null;

        try {
            document = Jsoup.connect(url).get();

        } catch (Exception ex) {
            Logger.getLogger(WebPageManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        return document;
    }

}
