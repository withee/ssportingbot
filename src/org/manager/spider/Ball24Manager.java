/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.manager.spider;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.manager.FileManager;
import org.manager.LogManager;
import org.model.EventM;
import org.model.RequestHeaderM;
import org.model.ResponseHeaderM;
import org.model.db.LanguageLeagueDAO;
import org.model.db.LanguageLeagueImpl;
import org.utility.Constants;
import org.utility.DateUtility;
import org.utility.XMLUtility;

/**
 *
 * @author superx
 */
public class Ball24Manager extends WebPageManager {

    int ZdId = 0;
    private int zid = 0;
    private static final LogManager log = new LogManager();

    public void setZdid(int zdid) {
        this.ZdId = zdid;
    }

    public boolean getLiveMatchXML() {

        // Variable declearation
        FileManager fileman = new FileManager();
        boolean getdata = true;
        String url = Constants.BALL24_LIVE_MATCH_URL + DateUtility.getDateNow();
        String filename = "newfile.xml";
        //System.out.println(url);
        log.write("info", url);
        Document doc = this.getDocXMLByUrl(url);
        //System.out.println(doc.child(0).attr("plikxml"));
        if (!doc.child(0).attr("plikxml").isEmpty()) {
            //System.out.println("ZdId:" + doc.child(0).child(2).attr("ZdId"));
            //int zdid = Integer.parseInt(doc.child(0).child(2).attr("ZdId"));
            int zid = Integer.parseInt(doc.child(0).attr("zid"));
            this.setZid(zid);
            filename = this.getZid() + ".xml";
            fileman.init();
            fileman.setPath(Constants.LIVE_MATCH_NEW_ENTRY);
            fileman.setFilename(filename);
            if (!FileManager.isFile(fileman)) {
                fileman.setContent(doc.toString());
                fileman.write();
            }

        } else {
            //LogManager log = new LogManager();
            log.write("error", "attribute \'plikxml\' has been disappear.");
        }

        return getdata;
    }

    public boolean getLiveXMLDate() {

        // Variable declearation
        FileManager fileman = new FileManager();
        boolean getdata = true;
        String url = Constants.BALL24_LIVE_MATCH_YESTERDAY_URL + DateUtility.getDateYesterday();
        String filename = "0.xml";
        //System.out.println(url);
        log.write("info", url);
        fileman.setPath(Constants.LIVE_MATCH_YESTERDAY);
        fileman.setFilename(filename);
        if (!FileManager.isFile(fileman)) {
            Document doc = this.getDocXMLByUrl(url);
            //System.out.println(doc.child(0).attr("plikxml"));
            if (!doc.child(0).attr("plikxml").isEmpty()) {
                int zid = Integer.parseInt(doc.child(0).attr("zid"));
                this.setZid(zid);
                filename = this.getZid() + ".xml";
                fileman.setFilename(filename);
                if (!FileManager.isFile(fileman)) {
                    fileman.setContent(doc.toString());
                    fileman.write();
                }

            } else {
                //LogManager log = new LogManager();
                log.write("error", "attribute \'plikxml\' has been disappear.");
            }
        }

        return getdata;
    }

    public boolean getLiveEventXML(int mid) {

        // Variable declearation
        FileManager fileman = new FileManager();
        boolean getdata = true;
        String url = Constants.MATCH_ACTION_XML + mid + "&_=" + DateUtility.getTimeNow();
        String filename = "newfile.xml";
        System.out.println(url);
        //LogManager log = new LogManager();
        log.write("info", "get action from: " + url);
        Document doc = this.getDocXMLByUrl(url);
        System.out.println(doc.toString());
        //System.out.println(doc.child(0).child(0).toString());
        if (0 != doc.child(0).childNodeSize()) {
            //filename = doc.child(0).attr("plikxml");          
            filename = mid + ".xml";
            fileman.init();
            fileman.setPath(Constants.LIVE_MATCH_NEW_ACTION);
            fileman.setFilename(filename);
            fileman.setContent(doc.toString());
            fileman.write();
            getdata = true;
            log.write("info", mid + " has attribute \'akcja\'.");
        } else {

            log.write("info", "attribute \'akcja\' has been disappear.");
        }

        return getdata;
    }

    public boolean getLiveEventAction(Vector<EventM> eventMvt, int mid) {

        // Variable declearation
        FileManager fileman = new FileManager();
        boolean getdata = true;
        String path = Constants.LIVE_MATCH_NEW_ACTION;
        String filename = mid + ".xml";
        LogManager log = new LogManager();
        HashMap<String, String> allAttr = null;

        System.out.println(path);
        fileman.setPath(path);
        fileman.setFilename(filename);
        String content = fileman.read();
        //System.out.println("content:" + content);
        Document doc = Jsoup.parse(content);
        //System.out.println(doc.toString());
        //System.out.println(doc.child(0).child(0).toString());
        //System.out.println(doc.child(0).outerHtml());
        Elements allChild = doc.getElementsByTag("akcja");
        if (0 != allChild.size()) {
            log.write("info", mid + " action found.");
            for (Element child : allChild) {
                EventM eventM = new EventM();
                allAttr = XMLUtility.attribute2HashMap(child);
                eventM.setValueByAttr(allAttr);
                eventM.setMid(mid);
                eventMvt.add(eventM);
                //System.out.println(child.toString());
            }

        } else {

            log.write("info", mid + " action not found.");
        }

        return getdata;
    }

    public boolean getCompetitions() {

        // Variable declearation
        FileManager fileman = new FileManager();
        boolean getdata = true;
        String path = Constants.BALL24_COMPETITION;
        String filename = "competition.json";
        String url = Constants.BALL24_MAIN_PAGE;
        LogManager log = new LogManager();
        //String content = "";
        fileman.setPath(path);
        fileman.setFilename(filename);
        Element content = null;
        if (!FileManager.isFile(fileman)) {
            Document doc = this.getDocXMLByUrl(url);
            //System.out.println(doc.toString());
            JSONObject obj = new JSONObject();

            Elements allChild = doc.getElementsByClass("countries");
            int round = 0;
            for (Element child : allChild) {
                int cindex = 0;
                for (Element subElement : child.children()) {
                    JSONObject list = new JSONObject();

                    //System.out.println(subElement.outerHtml());
                    int cid = Integer.parseInt(subElement.attr("data-id"));
                    String link = subElement.child(cindex).attr("href");
                    String competitionName = subElement.child(cindex).text();
                    String[] linkflag = link.split("/");
                    String type = linkflag[1];
                    String namepk = linkflag[2];
                    String leaguenamepk = linkflag[3];
                    String season = linkflag[4];

                    list.put("competitioId", cid);
                    list.put("competitionType", type);
                    list.put("competitionName", competitionName);
                    list.put("competitionNamePk", namepk);
                    list.put("leagueNamePk", leaguenamepk);
                    list.put("season", season);
                    obj.put(cid, list);
                }

            }
            fileman.setContent(obj.toString());
            fileman.write();
        }

        return getdata;
    }

    public void writeLanguageLeague2JsonFile() {

        JSONObject jsonObject = null;

        LanguageLeagueDAO languageLeagueDAO = new LanguageLeagueImpl();

        // Get language league data to json
        jsonObject = languageLeagueDAO.getJsonFromAll();

        // Write json to file
        // File name : "../../domains/ssporting.com/api/public_html/league_data/"+key+".json",json
        String[] strLanguage = {"En", "Big", "Gb", "Kr", "Vn", "La", "Th"};
        String strFilename = "";

        for (int i = 0; i < strLanguage.length; ++i) {

            JSONArray jsonArr = null;
            strFilename = "../../domains/ssporting.com/api/public_html/league_data/" + strLanguage[i] + ".json";

            jsonArr = jsonObject.getJSONArray(strLanguage[i]);

            // Write   jsonArr to strFilename
        }

    }

    public void writeLanguageTeam2JsonFile() {

        JSONObject jsonObject = null;

        LanguageLeagueDAO languageLeagueDAO = new LanguageLeagueImpl();

        // Get language league data to json
        jsonObject = languageLeagueDAO.getJsonFromAll();

        // Write json to file
        // File name : "../../domains/ssporting.com/api/public_html/team_data/"+key+".json",json
        String[] strLanguage = {"En", "Big", "Gb", "Kr", "Vn", "La", "Th"};
        String strFilename = "";

        for (int i = 0; i < strLanguage.length; ++i) {

            JSONArray jsonArr = null;
            strFilename = "../../domains/ssporting.com/api/public_html/team_data/" + strLanguage[i] + ".json";

            jsonArr = jsonObject.getJSONArray(strLanguage[i]);

            // Write   jsonArr to strFilename
        }

    }

    public void writeInfo(int mid, int typeId, int ao) {
        //This function move from tDb.getInfo(l.getMid(), 1, l.getAo());
        //System.out.println("###### GET Team Info #########");
        log.write("info", "###### GET Team Info #########");
        //String array[] = new String[2];
        String[] tnPk = new String[2];
        String cnPk = "";
        String cType = "";
        //http://www.futbol24.com/ml/matchRedirect/?TypeId=2&TypeHash=6ee7c&MId=950431&Ao=1
        //http://www.futbol24.com/ml/matchRedirect/?TypeId=1&TypeHash=cc2a5&MId=950431&Ao=1
        try {
            FileManager fileman = new FileManager();
            fileman.setPath(Constants.BALL24_TEAM_PK);
            fileman.setFilename(mid + ".json");
            if (!FileManager.isFile(fileman)) {
                String urlString1 = "http://www.futbol24.com/ml/matchRedirect/?TypeId=1&TypeHash=cc2a5&MId=" + mid + "&Ao=" + ao;
                String urlString2 = "http://www.futbol24.com/ml/matchRedirect/?TypeId=2&TypeHash=6ee7c&MId=" + mid + "&Ao=" + ao;
                String[] urlList = new String[2];
                urlList[0] = urlString1;
                urlList[1] = urlString2;
                int round = 0;
                for (String urlString : urlList) {
                    //System.out.println(urlString);
                    log.write("info", urlString);
                    URL url = new URL(urlString);
                    URLConnection conn = url.openConnection();
                    ((HttpURLConnection) conn).setInstanceFollowRedirects(false);
                    String location = conn.getHeaderField("Location");
                    //System.out.println(location);
                    String split[] = location.split("/");
                    tnPk[round] = split[3];
                    cnPk = split[2];
                    //array[0] = cnPk;
                    //array[1] = tnPk;
                    //System.out.println(location);
                    //System.out.println("#### CNPK= " + cnPk + " ####");
                    //System.out.println("#### TNPK= " + tnPk[round] + " #####");
                    round++;
                }
                JSONObject obj = new JSONObject();
                obj.put("cnPk", cnPk);
                obj.put("hnPk", tnPk[0]);
                obj.put("gnPk", tnPk[1]);
                fileman.setContent(obj.toString());
                fileman.write();
                log.write("info", obj.toString());
            }else{
                log.write("info", "Already get match "+mid+" info");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public ResponseHeaderM getContent(RequestHeaderM reqh) {
        ResponseHeaderM resh = new ResponseHeaderM();
        URL ur;
        //HttpURLConnection.setFollowRedirects(false);
        HttpURLConnection connect;
        Vector<String> contentVector = new Vector<String>();
        String content = "";
        Map headerContent = new HashMap();
        int status = 0;
        try {
            //System.out.println(reqh.getUrl());
            ur = new URL(reqh.getUrl());
            connect = (HttpURLConnection) ur.openConnection();
            ((HttpURLConnection) connect).setInstanceFollowRedirects(false);
            connect.setRequestMethod(reqh.getMethod());
            connect.setRequestProperty("Host", reqh.getHost());
            connect.setRequestProperty("User-Agent", reqh.getUserAgent());
            connect.setRequestProperty("Accept", reqh.getAccept());
            connect.setRequestProperty("Accept-Language", reqh.getAcceptLanguage());
            connect.setRequestProperty("Accept-Encoding", reqh.getAcceptEncoding());
            connect.setRequestProperty("Connection", reqh.getConnection());;
            if (reqh.getAcceptCharset() != null) {
                connect.setRequestProperty("Accept-Charset", reqh.getAcceptCharset());
            }
            if (reqh.getReferer() != null) {
                connect.setRequestProperty("Referer", reqh.getReferer());
            }
            if (reqh.getCookie() != null) {
                connect.setRequestProperty("Cookie", reqh.getCookie());
            }
            if (reqh.getContentType() != null) {
                connect.setRequestProperty("Content-Type", reqh.getContentType());
            }
            if (reqh.getXrequestWith() != null) {
                connect.setRequestProperty("X-Requested-With", reqh.getXrequestWith());
            }
            connect.setUseCaches(false);
            connect.setDoInput(true);
            connect.setDoOutput(true);
            connect.setConnectTimeout(1000 * 3);
            connect.setReadTimeout(1000 * 5);
            DataOutputStream wr = new DataOutputStream(
                    connect.getOutputStream());
            wr.writeBytes(reqh.getParam() != null ? reqh.getParam() : "");
            wr.flush();
            wr.close();
            int k = 0;

            try {
                InputStream is = connect.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                try {
                    GZIPInputStream gzip = new GZIPInputStream(is);
                    rd = new BufferedReader(new InputStreamReader(gzip));
                    status = 1;
                } catch (IOException ex) {
                    rd = new BufferedReader(new InputStreamReader(is));
                    status = 1;
                } catch (Exception ex) {
                    //ex.printStackTrace();
                    //ErrorLog.writeLog(ex.getStackTrace(), ex.toString());
                } finally {
                    String thisLine;

                    while ((thisLine = rd.readLine()) != null) {
                        contentVector.addElement(thisLine);
                        content += thisLine;
                        //System.out.println("H" + thisLine);
                    }
                    status = 1;
                }
            } catch (IOException ex) {
                status = 0;
                //ex.printStackTrace();
                //ErrorLog.writeLog(ex.getStackTrace(), ex.toString());
            } catch (Exception ex) {
                status = 0;
                //ex.printStackTrace();
                //ErrorLog.writeLog(ex.getStackTrace(), ex.toString());
            }

            try {
                Map responseMap = connect.getHeaderFields();

                for (Iterator iterator = responseMap.keySet().iterator(); iterator.hasNext();) {
                    String key = (String) iterator.next();
                    if (key != null) {

                        //System.out.print(key + "=");
//                    if (key.equals("Set-Cookie")) {
//                        if (k == 0) {
//                            //cook = "";
//                        }
//                        k++;
//                        List values = (List) responseMap.get(key);
//                        for (int i = 0; i < values.size(); i++) {
//                            Object o = values.get(i);
//                            String st = (String) o;
//                            String ar[] = st.split(";");
//                            //cook += ar[0] + "; ";
//                        }
//                        //System.out.println("cooking:"+cook);
//                    }
                        String value = "";
                        List values = (List) responseMap.get(key);
                        for (int i = 0; i < values.size(); i++) {
                            Object o = values.get(i);
                            String st = (String) o;
                            String split[] = st.split(";");
                            value += split[0] + ";";
                            //System.out.print(st + " \n");
                        }
                        //System.out.println(value);
                        //System.out.println();
                        headerContent.put(key, value);
                    }
                }
            } catch (Exception ex) {
                status = 0;
                //ex.printStackTrace();
            }
        } catch (Exception ex) {
            status = 0;
            //ex.printStackTrace();
        }

        resh.setStatus(status);
        resh.setHeaderContent(headerContent);
        resh.setContent(content);
        resh.setContentVector(contentVector);
        //System.out.println(content);
        return resh;
    }

    /**
     * @return the zid
     */
    public int getZid() {
        return zid;
    }

    /**
     * @param zid the zid to set
     */
    public void setZid(int zid) {
        this.zid = zid;
    }

}
