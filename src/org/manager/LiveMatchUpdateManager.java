/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.manager;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.model.LiveFileM;
import org.model.LiveLeagueM;
import org.model.LiveMatchLastEventM;
import org.model.LiveMatchM;
import org.utility.Constants;
import org.utility.DateUtility;
import org.utility.XMLUtility;

/**
 *
 * @author superx
 */
public class LiveMatchUpdateManager {

    private static final LogManager log = new LogManager();

    public String getLiveMatchEventXMLFileName() {

        log.write(Constants.LOG_LEVEL_INFO, "in  public String getLiveMatchEventXMLFileName() {");

        File folder = new File(Constants.LIVE_MATCH_XML_PATH);
        File[] listOfFiles = folder.listFiles();
        int olderFileIndex = 0;
        long olderFileTime = 0;
        boolean bChk = false;

        //log.write( Constants.LOG_LEVEL_INFO, "File path -> " + Constants.LIVE_MATCH_XML_PATH);
        for (int i = 0; i < listOfFiles.length; i++) {

            File destination = new File(Constants.LIVE_MATCH_XML_PATH + listOfFiles[i].getName());
            if (!destination.isDirectory()) {

                //log.write( Constants.LOG_LEVEL_INFO, i + " File Name -> " + listOfFiles[i].getName());
                bChk = true;
                if (0 == i) {
                    olderFileTime = listOfFiles[i].lastModified();
                } else if (olderFileTime > listOfFiles[i].lastModified()) {
                    olderFileTime = listOfFiles[i].lastModified();
                    olderFileIndex = i;
                }
            }
        }
        //log.write( Constants.LOG_LEVEL_INFO, " File name -> " + listOfFiles[olderFileIndex].getName());
        log.write(Constants.LOG_LEVEL_INFO, "out  public String getLiveMatchEventXMLFileName() {");

        if (true == bChk) {
            //System.out.print(" File " + listOfFiles[olderFileIndex].getName());
            //System.out.println(" Time " + listOfFiles[olderFileIndex].lastModified());
            return listOfFiles[olderFileIndex].getName();
        } else {
            //System.out.println("Has not found the file");
            return "";
        }

    }

    public void excuiteLiveMatchEvent(String fileName) {
        
        log.write(Constants.LOG_LEVEL_INFO, "in  public void excuiteLiveMatchEvent(String fileName) {");

        // Root
        Element root = null;
        HashMap<String, String> rootAttr = null;
        LiveFileM liveFileM = new LiveFileM();
        // League
        Element leagueE = null;
        HashMap<String, String> leagueAttr = null;
        Elements countrysE = null;
        HashMap<String, String> countryAttr = null;
        Elements leagueincountrysE = null;
        HashMap<String, String> leagueincountryAttr = null;
        Elements groupleagueincountrysE = null;
        HashMap<String, String> groupleagueincountryAttr = null;

        LiveLeagueM liveLeagueM = null;
        Vector<LiveLeagueM> liveLeagueVt = new Vector<LiveLeagueM>();
        Vector<LiveMatchM> liveMatchVt = new Vector<LiveMatchM>();
        Vector<LiveMatchLastEventM> liveMatchEventVt = new Vector<LiveMatchLastEventM>();
        // Match
        Element matchRootE = null;
        HashMap<String, String> matchRootAttr = null;
        Elements matchsE = null;

        HashMap<String, String> matchAttr = null;

        // Event
        Element eventRootE = null;
        HashMap<String, String> eventRootAttr = null;
        Elements eventsE = null;

        HashMap<String, String> eventAttr = null;
        
        // 1. Excuite root element --> f24 get attribute to LiveFileB bean
        root = XMLUtility.getElementFromXML(fileName, Constants.LIVE_MATCH_XML_ROOT_NODE);
        rootAttr = XMLUtility.attribute2HashMap(root);

        log.write(Constants.LOG_LEVEL_INFO, " 1. Excuite root element --> f24 get attribute to LiveFileB bean");
        log.write(Constants.LOG_LEVEL_INFO, " root -> " + root.nodeName());
        log.write(Constants.LOG_LEVEL_INFO, " root attrib size -> " + rootAttr.size());

        for (Map.Entry<String, String> attr : rootAttr.entrySet()) {
            liveFileM.setValueByKey(attr.getKey(), attr.getValue());
            log.write(Constants.LOG_LEVEL_INFO, " key -> " + attr.getKey() + " | value -> " + attr.getValue());
        }
        
        //liveFileM.setDate(DateUtility.getDateNowyyyy_MM_dd());
        log.write(Constants.LOG_LEVEL_INFO, " LiveFileM attrib -> " +  liveFileM.displayData2String());
        
        
        log.write(Constants.LOG_LEVEL_INFO, "out  public void excuiteLiveMatchEvent(String fileName) {");

    }

}
