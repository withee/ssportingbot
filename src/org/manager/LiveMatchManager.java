/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.manager;

import java.io.File;
import static java.lang.System.exit;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.model.LiveFileM;
import org.model.LiveLeagueM;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.model.LiveMatchLastEventM;
import org.model.LiveMatchM;
import org.model.db.LiveFileDAO;
import org.model.db.LiveFileImpl;
import org.model.db.LiveLeagueDAO;
import org.model.db.LiveLeagueImpl;
import org.model.db.LiveMatchDAO;
import org.model.db.LiveMatchImpl;
import org.utility.Constants;
import org.utility.DateUtility;
import org.utility.IntegerUtility;
import org.utility.XMLUtility;

/**
 *
 * @author superx
 */
public class LiveMatchManager {

    private static final LogManager log = new LogManager();

    public String getLiveMatchXMLFileName() {

        File folder = new File(Constants.LIVE_MATCH_NEW_ENTRY);
        File[] listOfFiles = folder.listFiles();
        int olderFileIndex = 0;
        long olderFileTime = 0;
        boolean bChk = false;

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                bChk = true;
                if (0 == i) {
                    olderFileTime = listOfFiles[i].lastModified();
                } else if (olderFileTime > listOfFiles[i].lastModified()) {
                    olderFileTime = listOfFiles[i].lastModified();
                    olderFileIndex = i;
                }
            }
        }

        if (true == bChk) {
            //System.out.print(" File " + listOfFiles[olderFileIndex].getName());
            //System.out.println(" Time " + listOfFiles[olderFileIndex].lastModified());
            return listOfFiles[olderFileIndex].getName();
        } else {
            //System.out.println("Has not found the file");
            return "";
        }
    }

    public String getLiveMatchUpdatedXMLFileName() {

        File folder = new File(Constants.LIVE_MATCH_EXCUITED);
        File[] listOfFiles = folder.listFiles();
        int olderFileIndex = 0;
        long olderFileTime = 0;
        boolean bChk = false;

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                bChk = true;
                if (0 == i) {
                    olderFileTime = listOfFiles[i].lastModified();
                } else if (olderFileTime > listOfFiles[i].lastModified()) {
                    olderFileTime = listOfFiles[i].lastModified();
                    olderFileIndex = i;
                }
            }
        }

        if (true == bChk) {
            //System.out.print(" File " + listOfFiles[olderFileIndex].getName());
            //System.out.println(" Time " + listOfFiles[olderFileIndex].lastModified());
            return listOfFiles[olderFileIndex].getName();
        } else {
            //System.out.println("Has not found the file");
            return "";
        }
    }

    public String getOldestFile(String dir) {

        File folder = new File(dir);
        File[] listOfFiles = folder.listFiles();
        int olderFileIndex = 0;
        long olderFileTime = 0;
        boolean bChk = false;

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                bChk = true;
                if (0 == i) {
                    olderFileTime = listOfFiles[i].lastModified();
                } else if (olderFileTime > listOfFiles[i].lastModified()) {
                    olderFileTime = listOfFiles[i].lastModified();
                    olderFileIndex = i;
                }
            }
        }

        if (true == bChk) {
            //System.out.print(" File " + listOfFiles[olderFileIndex].getName());
            //System.out.println(" Time " + listOfFiles[olderFileIndex].lastModified());
            return listOfFiles[olderFileIndex].getName();
        } else {
            //System.out.println("Has not found the file");
            return "";
        }
    }

    public void excuiteLMXML2LMData(String xmlFile) {

        log.write(Constants.LOG_LEVEL_INFO, " in  public void excuiteLMXML2LMData(String xmlFile) {");
        // Root
        Element root = null;
        HashMap<String, String> rootAttr = null;
        LiveFileM liveFileM = new LiveFileM();
        // League
        Element leagueE = null;
        HashMap<String, String> leagueAttr = null;
        Elements countrysE = null;
        HashMap<String, String> countryAttr = null;
        Elements leagueincountrysE = null;
        HashMap<String, String> leagueincountryAttr = null;
        Elements groupleagueincountrysE = null;
        HashMap<String, String> groupleagueincountryAttr = null;

        LiveLeagueM liveLeagueM = null;
        Vector<LiveLeagueM> liveLeagueVt = new Vector<LiveLeagueM>();
        Vector<LiveMatchM> liveMatchVt = new Vector<LiveMatchM>();
        Vector<LiveMatchLastEventM> liveMatchEventVt = new Vector<LiveMatchLastEventM>();
        // Match
        Element matchRootE = null;
        HashMap<String, String> matchRootAttr = null;
        Elements matchsE = null;

        HashMap<String, String> matchAttr = null;

        // Event
        Element eventRootE = null;
        HashMap<String, String> eventRootAttr = null;
        Elements eventsE = null;

        HashMap<String, String> eventAttr = null;

        LiveLeagueDAO liveLeagueDAO = new LiveLeagueImpl();
        LiveFileDAO liveFileDAO = new LiveFileImpl();
        LiveMatchDAO liveMatchDAO = new LiveMatchImpl();

        // 1. Excuite root element --> f24 get attribute to LiveFileB bean
        root = XMLUtility.getElementFromXML(xmlFile, Constants.LIVE_MATCH_XML_ROOT_NODE);
        rootAttr = XMLUtility.attribute2HashMap(root);

        log.write(Constants.LOG_LEVEL_INFO, " 1. Excuite root element --> f24 get attribute to LiveFileB bean");
        log.write(Constants.LOG_LEVEL_INFO, " root -> " + root.nodeName());
        log.write(Constants.LOG_LEVEL_INFO, " root attrib size -> " + rootAttr.size());

        for (Map.Entry<String, String> attr : rootAttr.entrySet()) {
            liveFileM.setValueByKey(attr.getKey(), attr.getValue());
            log.write(Constants.LOG_LEVEL_INFO, " key -> " + attr.getKey() + " | value -> " + attr.getValue());
        }

        //Insert or update record in DB
        if (liveFileDAO.insertLiveFile(liveFileM)) {
            //System.out.println("new live file updated");
            log.write("info", "update livefile success");
        } else {
            log.write("info", "update livefile fail");
        }

        // 2. Excuite league element --> kraje
        leagueE = XMLUtility.getElementByName(root, Constants.LIVE_MATCH_XML_LEAGUE_NODE);
        // 2.1 Get attribute i = 36
        leagueAttr = XMLUtility.attribute2HashMap(leagueE);

        //liveLeagueB.setCountryData(leagueAttr);
        // 2.2 Get country in league
        countrysE = XMLUtility.getChildren(leagueE);
        log.write(Constants.LOG_LEVEL_INFO, " league size -> " + countrysE.size());
        //set old live_league
        //liveLeagueDAO.setOldrecord(liveFileM.getDate());
        //

        //Attribute k
        for (Element country : countrysE) {
            countryAttr = XMLUtility.attribute2HashMap(country);

            // Set liveLeagueB
            // 2.3 Get league in country
            leagueincountrysE = XMLUtility.getChildren(country);

            ///Attribute l
            for (Element leagueincountry : leagueincountrysE) {
                leagueincountryAttr = XMLUtility.attribute2HashMap(leagueincountry);
                groupleagueincountrysE = XMLUtility.getChildren(leagueincountry);
//                System.out.println("LEAGUE SIZE:"+leagueincountry.childNodeSize());
//                exit(0);
                // Set league
                if (0 < leagueincountry.childNodeSize()) {
                    // In case has data
                    //Attribute pl
                    for (Element groupleagueincountry : groupleagueincountrysE) {
                        groupleagueincountryAttr = XMLUtility.attribute2HashMap(groupleagueincountry);

                        //liveLeagueB
                        liveLeagueM = new LiveLeagueM();
                        // Set country
                        liveLeagueM.setValueByCountry(countryAttr);
                        liveLeagueM.setValueByLeague(leagueincountryAttr);
                        liveLeagueM.setValueBySubLeague(groupleagueincountryAttr);
                        // Set group in league
                        liveLeagueM.setDate(liveFileM.getDate());
                        liveLeagueVt.add(liveLeagueM);

                        if (liveLeagueDAO.insertLiveLeague(liveLeagueM)) {
                            //System.out.println(liveLeagueM.getLn());
                        }

                        //XMLUtility.displayHMAttribute(groupleagueincountryAttr);               
                    }//end for(Element groupleagueincountry : groupleagueincountrysE){
                } else {    // Sdd to vector
                    liveLeagueM = new LiveLeagueM();
                    // Set country
                    liveLeagueM.setValueByCountry(countryAttr);
                    liveLeagueM.setValueByLeague(leagueincountryAttr);
                    liveLeagueM.setDate(liveFileM.getDate());
                    liveLeagueVt.add(liveLeagueM);
                    if (liveLeagueDAO.insertLiveLeague(liveLeagueM)) {
                        //System.out.println(liveLeagueM.getLn());
                    }
                }
            }//end for (Element leagueincountry : leagueincountrysE ){   

        }//end for (Element country : countrysE){
        //delete old live league
        //liveLeagueDAO.delOldrecord(liveFileM.getDate());
        //

        // 3. Excuite match element --> mecze
        matchRootE = XMLUtility.getElementByName(root, Constants.LIVE_MATCH_XML_MATCH_NODE);
        // 3.1 Get attribute xxx = 9999
        matchRootAttr = XMLUtility.attribute2HashMap(matchRootE);

        matchsE = XMLUtility.getChildren(matchRootE);
        log.write(Constants.LOG_LEVEL_INFO, " macth size -> " + matchsE.size());

        int sumWait = 0;
        int sumLive = 0;
        int sumResult = 0;

        ///set old live_match
        //liveMatchDAO.setOldlivematch(liveFileM.getDate());
        //exit(0);
        //
        for (Element match : matchsE) {
            matchAttr = XMLUtility.attribute2HashMap(match);
///            XMLUtility.displayHMAttribute(matchAttr);
            LiveMatchM liveMatchM = new LiveMatchM();
            liveMatchM.setValueByKey(matchAttr);
            liveMatchM.setDate(DateUtility.getGMT0(liveMatchM.getC0()));
            liveMatchM.setShowDate(liveFileM.getDate());
            //System.out.println(liveMatchM.getRek_h());
            if (liveMatchM.getSid() == 1) {
                sumWait++;
            } else if (liveMatchM.getSid() > 1 && liveMatchM.getLnr() == 1) {
                sumLive++;
            } else if (liveMatchM.getSid() > 4 && liveMatchM.getLnr() == 2) {
                sumResult++;
            }
            liveMatchVt.add(liveMatchM);
            if (liveMatchDAO.insertLiveMatch(liveMatchM)) {
                //System.out.println(liveMatchM.getHn() + "-" + liveMatchM.getGn());
                log.write("info", "Add new  game:" + liveMatchM.getHn() + "-" + liveMatchM.getGn());
            } else {
                log.write("info", "FAIL to add new  game:" + liveMatchM.getHn() + "-" + liveMatchM.getGn());
            }

        }
        ///del old live_match
        //liveMatchDAO.delOldlivematch(liveFileM.getDate());
        //exit(0);
        //

        FileManager fileManager = new FileManager();
        fileManager.setPath("file");
        fileManager.setFilename("sumWait.txt");
        fileManager.setContent(String.valueOf(sumWait));
        fileManager.write();
        fileManager.setFilename("sumLive.txt");
        fileManager.setContent(String.valueOf(sumLive));
        fileManager.write();
        fileManager.setFilename("sumResult.txt");
        fileManager.setContent(String.valueOf(sumResult));
        fileManager.write();
        //System.out.println("live:" + sumLive + ",wait:" + sumWait + ",result:" + sumResult);
        log.write("info", "live:" + sumLive + ",wait:" + sumWait + ",result:" + sumResult);
        fileManager.writeFile();
        // 4. Excuite event element --> zd
        eventRootE = XMLUtility.getElementByName(root, Constants.LIVE_MATCH_XML_EVENT_NODE);
        // 3.1 Get attribute xxx = 9999
        eventRootAttr = XMLUtility.attribute2HashMap(eventRootE);

        eventsE = XMLUtility.getChildren(eventRootE);
        log.write(Constants.LOG_LEVEL_INFO, " event size -> " + eventsE.size());

        for (Element event : eventsE) {
            eventAttr = XMLUtility.attribute2HashMap(event);
            //XMLUtility.displayHMAttribute(eventAttr);
            LiveMatchLastEventM liveMatchLastEventM = new LiveMatchLastEventM();
            liveMatchLastEventM.setValueByKey(eventAttr);
            liveMatchEventVt.add(liveMatchLastEventM);
        }
        JSonManager jsonManager = new JSonManager();
        jsonManager.addLastEvent(liveMatchEventVt);

    }

    public void excuiteYesterdayLMXML2LMData(String xmlFile) {

        log.write(Constants.LOG_LEVEL_INFO, " in  public void excuiteLMXML2LMData(String xmlFile) {");
        // Root
        Element root = null;
        HashMap<String, String> rootAttr = null;
        LiveFileM liveFileM = new LiveFileM();
        // League
        Element leagueE = null;
        HashMap<String, String> leagueAttr = null;
        Elements countrysE = null;
        HashMap<String, String> countryAttr = null;
        Elements leagueincountrysE = null;
        HashMap<String, String> leagueincountryAttr = null;
        Elements groupleagueincountrysE = null;
        HashMap<String, String> groupleagueincountryAttr = null;

        LiveLeagueM liveLeagueM = null;
        Vector<LiveLeagueM> liveLeagueVt = new Vector<LiveLeagueM>();
        Vector<LiveMatchM> liveMatchVt = new Vector<LiveMatchM>();
        Vector<LiveMatchLastEventM> liveMatchEventVt = new Vector<LiveMatchLastEventM>();
        // Match
        Element matchRootE = null;
        HashMap<String, String> matchRootAttr = null;
        Elements matchsE = null;

        HashMap<String, String> matchAttr = null;

        // Event
        Element eventRootE = null;
        HashMap<String, String> eventRootAttr = null;
        Elements eventsE = null;

        HashMap<String, String> eventAttr = null;

        LiveLeagueDAO liveLeagueDAO = new LiveLeagueImpl();
        LiveFileDAO liveFileDAO = new LiveFileImpl();
        LiveMatchDAO liveMatchDAO = new LiveMatchImpl();

        // 1. Excuite root element --> f24 get attribute to LiveFileB bean
        root = XMLUtility.getElementFromXML(xmlFile, Constants.LIVE_MATCH_XML_ROOT_NODE);
        rootAttr = XMLUtility.attribute2HashMap(root);

        log.write(Constants.LOG_LEVEL_INFO, " 1. Excuite root element --> f24 get attribute to LiveFileB bean");
        log.write(Constants.LOG_LEVEL_INFO, " root -> " + root.nodeName());
        log.write(Constants.LOG_LEVEL_INFO, " root attrib size -> " + rootAttr.size());

        for (Map.Entry<String, String> attr : rootAttr.entrySet()) {
            liveFileM.setValueByKey(attr.getKey(), attr.getValue());
            log.write(Constants.LOG_LEVEL_INFO, " key -> " + attr.getKey() + " | value -> " + attr.getValue());
        }

        //System.out.println(liveFileM.getDate());
        //exit(0);
        //Insert or update record in DB
        if (liveFileDAO.insertLiveFile(liveFileM)) {
            //System.out.println("new live file updated");
            log.write("info", "update livefile success");
        } else {
            log.write("info", "update livefile fail");
        }

        // 2. Excuite league element --> kraje
        leagueE = XMLUtility.getElementByName(root, Constants.LIVE_MATCH_XML_LEAGUE_NODE);
        // 2.1 Get attribute i = 36
        leagueAttr = XMLUtility.attribute2HashMap(leagueE);

        //liveLeagueB.setCountryData(leagueAttr);
        // 2.2 Get country in league
        countrysE = XMLUtility.getChildren(leagueE);
        log.write(Constants.LOG_LEVEL_INFO, " league size -> " + countrysE.size());
        //set old live_league
        liveLeagueDAO.setOldrecord(liveFileM.getDate());
        //

        //Attribute k
        for (Element country : countrysE) {
            countryAttr = XMLUtility.attribute2HashMap(country);

            // Set liveLeagueB
            // 2.3 Get league in country
            leagueincountrysE = XMLUtility.getChildren(country);

            ///Attribute l
            for (Element leagueincountry : leagueincountrysE) {
                leagueincountryAttr = XMLUtility.attribute2HashMap(leagueincountry);
                groupleagueincountrysE = XMLUtility.getChildren(leagueincountry);
//                System.out.println("LEAGUE SIZE:"+leagueincountry.childNodeSize());
//                exit(0);
                // Set league
                if (0 < leagueincountry.childNodeSize()) {
                    // In case has data
                    //Attribute pl
                    for (Element groupleagueincountry : groupleagueincountrysE) {
                        groupleagueincountryAttr = XMLUtility.attribute2HashMap(groupleagueincountry);

                        //liveLeagueB
                        liveLeagueM = new LiveLeagueM();
                        // Set country
                        liveLeagueM.setValueByCountry(countryAttr);
                        liveLeagueM.setValueByLeague(leagueincountryAttr);
                        liveLeagueM.setValueBySubLeague(groupleagueincountryAttr);
                        // Set group in league
                        liveLeagueM.setDate(liveFileM.getDate());
                        liveLeagueVt.add(liveLeagueM);

                        if (liveLeagueDAO.insertLiveLeague(liveLeagueM)) {
                            //System.out.println(liveLeagueM.getLn());
                        }

                        //XMLUtility.displayHMAttribute(groupleagueincountryAttr);               
                    }//end for(Element groupleagueincountry : groupleagueincountrysE){
                } else {    // Sdd to vector
                    liveLeagueM = new LiveLeagueM();
                    // Set country
                    liveLeagueM.setValueByCountry(countryAttr);
                    liveLeagueM.setValueByLeague(leagueincountryAttr);
                    liveLeagueM.setDate(liveFileM.getDate());
                    liveLeagueVt.add(liveLeagueM);
                    if (liveLeagueDAO.insertLiveLeague(liveLeagueM)) {
                        //System.out.println(liveLeagueM.getLn());
                    }
                }
            }//end for (Element leagueincountry : leagueincountrysE ){   

        }//end for (Element country : countrysE){
        //delete old live league
        liveLeagueDAO.delOldrecord(liveFileM.getDate());
        //

        // 3. Excuite match element --> mecze
        matchRootE = XMLUtility.getElementByName(root, Constants.LIVE_MATCH_XML_MATCH_NODE);
        // 3.1 Get attribute xxx = 9999
        matchRootAttr = XMLUtility.attribute2HashMap(matchRootE);

        matchsE = XMLUtility.getChildren(matchRootE);
        log.write(Constants.LOG_LEVEL_INFO, " macth size -> " + matchsE.size());

        int sumWait = 0;
        int sumLive = 0;
        int sumResult = 0;

        ///set old live_match
        liveMatchDAO.setOldlivematch(liveFileM.getDate());
        //exit(0);
        //
        for (Element match : matchsE) {
            matchAttr = XMLUtility.attribute2HashMap(match);
///            XMLUtility.displayHMAttribute(matchAttr);
            LiveMatchM liveMatchM = new LiveMatchM();
            liveMatchM.setValueByKey(matchAttr);
            liveMatchM.setDate(DateUtility.getGMT0(liveMatchM.getC0()));
            liveMatchM.setShowDate(liveFileM.getDate());
            //System.out.println(liveMatchM.getRek_h());
//            if (liveMatchM.getSid() == 1) {
//                sumWait++;
//            } else if (liveMatchM.getSid() > 1 && liveMatchM.getLnr() == 1) {
//                sumLive++;
//            } else if (liveMatchM.getSid() > 4 && liveMatchM.getLnr() == 2) {
//                sumResult++;
//            }
            liveMatchVt.add(liveMatchM);
            if (liveMatchDAO.insertLiveMatch(liveMatchM)) {
                //System.out.println(liveMatchM.getHn() + "-" + liveMatchM.getGn());
                log.write("info", "Add new  game:" + liveMatchM.getHn() + "-" + liveMatchM.getGn());
            } else {
                log.write("info", "FAIL to add new  game:" + liveMatchM.getHn() + "-" + liveMatchM.getGn());
            }

        }
        ///del old live_match
        liveMatchDAO.delOldlivematch(liveFileM.getDate());
        //exit(0);
        //

        FileManager fileManager = new FileManager();
        fileManager.setPath("file");
        fileManager.setFilename("sumYesterday.txt");
        fileManager.setContent(String.valueOf(matchsE.size()));
        fileManager.write();
        //System.out.println("live:" + sumLive + ",wait:" + sumWait + ",result:" + sumResult);
        log.write("info", "yesterday:" + matchsE.size());
        fileManager.writeFileYesterday();
        // 4. Excuite event element --> zd
//        eventRootE = XMLUtility.getElementByName(root, Constants.LIVE_MATCH_XML_EVENT_NODE);
//        // 3.1 Get attribute xxx = 9999
//        eventRootAttr = XMLUtility.attribute2HashMap(eventRootE);
//
//        eventsE = XMLUtility.getChildren(eventRootE);
//        log.write(Constants.LOG_LEVEL_INFO, " event size -> " + eventsE.size());
//
//        for (Element event : eventsE) {
//            eventAttr = XMLUtility.attribute2HashMap(event);
//            //XMLUtility.displayHMAttribute(eventAttr);
//            LiveMatchLastEventM liveMatchLastEventM = new LiveMatchLastEventM();
//            liveMatchLastEventM.setValueByKey(eventAttr);
//            liveMatchEventVt.add(liveMatchLastEventM);
//        }
//        JSonManager jsonManager = new JSonManager();
//        jsonManager.addLastEvent(liveMatchEventVt);

    }

    public void excuiteLMUpdatedXML2LMData(String xmlFile) {

        log.write(Constants.LOG_LEVEL_INFO, "in public void excuiteLMUpdatedXML2LMData(String xmlFile) {");
        // Variable declaration
        // Root
        Element root = null;
        HashMap<String, String> rootAttr = null;
        LiveFileM liveFileM = new LiveFileM();
        // Match
        Element matchRootE = null;
        HashMap<String, String> matchRootAttr = null;
        Elements matchsE = null;
        HashMap<String, String> matchAttr = null;
        Vector<LiveMatchM> liveMatchVt = new Vector<LiveMatchM>();

        // 1. Excuite root element --> f24 get attribute to LiveFileB bean
        root = XMLUtility.getElementFromXML(xmlFile, Constants.LIVE_MATCH_XML_ROOT_NODE);

        log.write(Constants.LOG_LEVEL_INFO, "root->" + root.nodeName());

        rootAttr = XMLUtility.attribute2HashMap(root);
        log.write(Constants.LOG_LEVEL_INFO, "root attribute size ->" + rootAttr.size());
        int index = 0;
        for (Map.Entry<String, String> attr : rootAttr.entrySet()) {
            liveFileM.setValueByKey(attr.getKey(), attr.getValue());
            log.write(Constants.LOG_LEVEL_INFO, ++index + " key -> " + attr.getKey() + " | value -> " + attr.getValue());
        }
        log.write(Constants.LOG_LEVEL_INFO, "root[zdid]->" + rootAttr.get("zdid"));
        //System.out.println("1. Excuite root element --> f24 get attribute to LiveFileB bean");
        // 2. Excuite match element --> mecze
        matchRootE = XMLUtility.getElementByName(root, Constants.LIVE_MATCH_XML_MATCH_NODE);

        log.write(Constants.LOG_LEVEL_INFO, "matchRootE->" + matchRootE.nodeName());
        // 2.1 Get attribute xxx = 9999
        matchRootAttr = XMLUtility.attribute2HashMap(matchRootE);

        log.write(Constants.LOG_LEVEL_INFO, "matchRootE attribute size ->" + matchRootAttr.size());

        matchsE = XMLUtility.getChildren(matchRootE);
        log.write(Constants.LOG_LEVEL_INFO, "matchRootE match size ->" + matchsE.size());

        index = 0;
        for (Element match : matchsE) {
            matchAttr = XMLUtility.attribute2HashMap(match);
            LiveMatchM liveMatchM = new LiveMatchM();
            liveMatchM.setUpdatedValueByKey(matchAttr);

            log.write(Constants.LOG_LEVEL_INFO, ++index + " match  ->" + XMLUtility.displayAttribute2String(match));
            liveMatchVt.add(liveMatchM);
        }

        //System.out.println("2. Excuite match element --> mecze");
        // 3. Convert live match vector to json
        JSONObject jsonObject = this.convertLMUpdated2JSON(liveMatchVt, rootAttr.get("zdid"));

        log.write(Constants.LOG_LEVEL_INFO, " 3. Convert live match vector to json");

        if (null != jsonObject) {
            // Write json to file

//            JSONObject json = new JSONObject();
//            json.put("live_match_update", jsonObject);
//            json.put("update_id", liveMatchVt.lastElement().getZid());
//            json.put("c3", (DateUtility.getTimeNow() / 1000));
            //log.write(Constants.LOG_LEVEL_INFO, "<<<<<<<<<<<<<<<<<<< json for liveMatchUpdate.json ->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> \n" 
            //        + json.toString());
            //log.write(Constants.LOG_LEVEL_INFO, "<<<<<<<<<<<<<<<<<<  json for liveMatchUpdate.json ->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
            // Write liveMatchUpdate.json with json
            //FileController.writeFile("../../cronjob_gen_file/files/liveMatchUpdate.json", json.toString());
            FileManager fileManager = new FileManager();
            fileManager.setPath(Constants.LIVE_MATCH_UPDATED_PATH);
            fileManager.setFilename("liveMatchUpdate.json");
            fileManager.setContent(jsonObject.toString());
            fileManager.write();
            log.write(Constants.LOG_LEVEL_INFO, "Write json for liveMatchUpdate.json  ");
        }

        log.write(Constants.LOG_LEVEL_INFO, "out public void excuiteLMUpdatedXML2LMData(String xmlFile) {");

    }

    public JSONObject convertLMUpdated2JSON(Vector<LiveMatchM> liveMatchVt) {

        //System.out.println("in convertLMUpdated2JSON");
        //System.out.println("liveMatchVt.size()->"+liveMatchVt.size());
        JSONArray arrJSON = new JSONArray();
        JSONObject returnJSON = new JSONObject();

        log.write(Constants.LOG_LEVEL_INFO, "liveMatchVt.size() ->" + liveMatchVt.size());
        if (liveMatchVt.size() > 0) {
            //JSONArray arrJSON = new JSONArray();
            for (LiveMatchM liveMatchM : liveMatchVt) {

                //System.out.println("liveMatchM" + liveMatchM.getZid());  
                JSONObject obj = new JSONObject();

                obj.put("zid", liveMatchM.getZid());
                obj.put("mid", liveMatchM.getMid());
                obj.put("lid", (liveMatchM.getLid() > 0 ? liveMatchM.getLid() : null) + "");
                obj.put("_lid", (liveMatchM.getSlid() > 0 ? liveMatchM.getSlid() : null) + "");
                obj.put("sid", (liveMatchM.getSid() > 0 ? liveMatchM.getSid() : null) + "");
                obj.put("kid", (liveMatchM.getKid() > 0 ? liveMatchM.getKid() : null) + "");
                obj.put("lnr", (liveMatchM.getLnr()) > 0 ? liveMatchM.getLnr() : null + "");
                obj.put("c0", (liveMatchM.getC0() > 0 ? liveMatchM.getC0() : null) + "");
                obj.put("c1", (liveMatchM.getC1() > 0 ? liveMatchM.getC1() : null) + "");
                obj.put("c2", (liveMatchM.getC2() > 0 ? liveMatchM.getC2() : null) + "");
                obj.put("hid", (liveMatchM.getHid() > 0 ? liveMatchM.getHid() : null) + "");
                obj.put("gid", (liveMatchM.getGid() > 0 ? liveMatchM.getGid() : null) + "");
                obj.put("hn", (liveMatchM.getHn()) + "");
                obj.put("gn", (liveMatchM.getGn()) + "");
                obj.put("hrci", (liveMatchM.getHrci() > 0 ? liveMatchM.getHrci() : null) + "");
                obj.put("grci", (liveMatchM.getGrci() > 0 ? liveMatchM.getGrci() : null) + "");
                obj.put("hrc", liveMatchM.getHrc() + "");
                obj.put("grc", liveMatchM.getGrc() + "");
                obj.put("s1", liveMatchM.getS1() + "");
                obj.put("s2", liveMatchM.getS2() + "");
                obj.put("cx", (liveMatchM.getCx() > 0 ? liveMatchM.getCx() : null) + "");
                obj.put("x", (liveMatchM.getX() >= 0 ? liveMatchM.getX() : null) + "");

                //System.out.println("obj->"+obj.toString());
                arrJSON.add(obj);
            }//end for (LiveMatchM liveMatchM : liveMatchVt) {

            returnJSON = new JSONObject();
            returnJSON.put("live_match_update", arrJSON);
            returnJSON.put("update_id", liveMatchVt.lastElement().getZid());
            returnJSON.put("c3", (DateUtility.getTimeNow() / 1000));
            log.write(Constants.LOG_LEVEL_INFO, "update_id->" + liveMatchVt.lastElement().getZid());
        }//end if (liveMatchVt.size() > 0) {

        return returnJSON;
    }

    public JSONObject convertLMUpdated2JSON(Vector<LiveMatchM> liveMatchVt, String zdid) {

        //System.out.println("in convertLMUpdated2JSON");
        //System.out.println("liveMatchVt.size()->"+liveMatchVt.size());
        JSONArray arrJSON = new JSONArray();
        JSONObject returnJSON = new JSONObject();

        log.write(Constants.LOG_LEVEL_INFO, "liveMatchVt.size() ->" + liveMatchVt.size());
        if (liveMatchVt.size() > 0) {
            //JSONArray arrJSON = new JSONArray();
            for (LiveMatchM liveMatchM : liveMatchVt) {

                //System.out.println("liveMatchM" + liveMatchM.getZid());  
                JSONObject obj = new JSONObject();

                obj.put("zid", liveMatchM.getZid());
                obj.put("mid", liveMatchM.getMid());
                obj.put("lid", (liveMatchM.getLid() > 0 ? liveMatchM.getLid() : null) + "");
                obj.put("_lid", (liveMatchM.getSlid() > 0 ? liveMatchM.getSlid() : null) + "");
                obj.put("sid", (liveMatchM.getSid() > 0 ? liveMatchM.getSid() : null) + "");
                obj.put("kid", (liveMatchM.getKid() > 0 ? liveMatchM.getKid() : null) + "");
                obj.put("lnr", (liveMatchM.getLnr()) > 0 ? liveMatchM.getLnr() : null + "");
                obj.put("c0", (liveMatchM.getC0() > 0 ? liveMatchM.getC0() : null) + "");
                obj.put("c1", (liveMatchM.getC1() > 0 ? liveMatchM.getC1() : null) + "");
                obj.put("c2", (liveMatchM.getC2() > 0 ? liveMatchM.getC2() : null) + "");
                obj.put("hid", (liveMatchM.getHid() > 0 ? liveMatchM.getHid() : null) + "");
                obj.put("gid", (liveMatchM.getGid() > 0 ? liveMatchM.getGid() : null) + "");
                obj.put("hn", (liveMatchM.getHn()) + "");
                obj.put("gn", (liveMatchM.getGn()) + "");
                obj.put("hrci", (liveMatchM.getHrci() > 0 ? liveMatchM.getHrci() : null) + "");
                obj.put("grci", (liveMatchM.getGrci() > 0 ? liveMatchM.getGrci() : null) + "");
                obj.put("hrc", liveMatchM.getHrc() + "");
                obj.put("grc", liveMatchM.getGrc() + "");
                obj.put("s1", liveMatchM.getS1() + "");
                obj.put("s2", liveMatchM.getS2() + "");
                obj.put("cx", (liveMatchM.getCx() > 0 ? liveMatchM.getCx() : null) + "");
                obj.put("x", (liveMatchM.getX() >= 0 ? liveMatchM.getX() : null) + "");

                //System.out.println("obj->"+obj.toString());
                arrJSON.add(obj);
            }//end for (LiveMatchM liveMatchM : liveMatchVt) {

            returnJSON = new JSONObject();
            returnJSON.put("live_match_update", arrJSON);
            returnJSON.put("update_id", IntegerUtility.string2Int(zdid));
            returnJSON.put("c3", (DateUtility.getTimeNow() / 1000));
            log.write(Constants.LOG_LEVEL_INFO, "update_id->" + zdid);
        }//end if (liveMatchVt.size() > 0) {

        return returnJSON;
    }   

}
