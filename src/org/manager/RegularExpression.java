/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.sf.json.JSONObject;
import java.io.InputStream;

import net.sf.json.JSON;
import net.sf.json.xml.XMLSerializer;

/**
 *
 * @author indpendents
 */
public class RegularExpression {

    public static JSONObject getDataFromURL(String urlString) {

        JSONObject json = new JSONObject();
        try {
            URL url = new URL(urlString);
            URLConnection uc = url.openConnection();
            uc.setConnectTimeout(1000 * 3);
            uc.setReadTimeout(1000 * 5);
            Scanner s = new Scanner(new InputStreamReader(
                    uc.getInputStream()));
            String response = "";
            String str = "";
            while (s.hasNextLine()) {
                str = s.nextLine();
                response += str;
                //System.out.println(str);
            }
            json.put("status", true);
            json.put("data", response);
        } catch (MalformedURLException ex) {
            json.put("status", false);
            json.put("description", "Exception at URL");
        } catch (IOException ex) {
            ex.printStackTrace();
            json.put("status", false);
            json.put("description", "Exception at IO");
        } catch (Exception ex) {
            json.put("status", false);
            json.put("description", "Exception ");
        }

        return json;
    }

    public static JSONObject getDataFromURLDev(String urlString) {

        JSONObject json = new JSONObject();
        try {
            URL url = new URL(urlString);
            URLConnection uc = url.openConnection();
            uc.setConnectTimeout(1000 * 3);
            uc.setReadTimeout(1000 * 5);
            Scanner s = new Scanner(new InputStreamReader(
                    uc.getInputStream()));
            String response = "";
            String str = "";
            while (s.hasNextLine()) {
                str = s.nextLine();
                response += str;
                System.out.println(str);
            }
            json.put("status", true);
            json.put("data", response);
        } catch (MalformedURLException ex) {
            json.put("status", false);
            json.put("description", "Exception at URL");
        } catch (IOException ex) {
            json.put("status", false);
            json.put("description", "Exception at IO");
        }

        return json;
    }

    public static JSONObject getDataFromURL(String urlString, String parameter) {

        JSONObject json = new JSONObject();
        try {
            URL url = new URL(urlString + "?" + parameter);
            URLConnection uc = url.openConnection();
                        uc.setConnectTimeout(1000*3);
            uc.setReadTimeout(1000*5);
            Scanner s = new Scanner(new InputStreamReader(
                    uc.getInputStream()));
            String response = "";
            while (s.hasNextLine()) {
                response += s.nextLine();
            }
            json.put("status", true);
            json.put("data", response);
        } catch (MalformedURLException ex) {
            json.put("status", false);
            json.put("description", "Exception at URL");
        } catch (IOException ex) {
            json.put("status", false);
            json.put("description", "Exception at IO");
        }

        return json;
    }

    public static Vector<String> getRegularExpression(String rexp, String str) {
        Pattern p = Pattern.compile(rexp);
        Matcher m = p.matcher(str);
        Vector<String> vs = new Vector<String>();
        while (m.find()) {
            vs.addElement(m.group());
            //System.out.println(m.group());

        }
        return vs;

    }

    public static Vector<String> getRegularExpressionDev(String rexp, String str) {
        Pattern p = Pattern.compile(rexp);
        Matcher m = p.matcher(str);
        Vector<String> vs = new Vector<String>();
        while (m.find()) {
            vs.addElement(m.group());
            System.out.println(m.group());

        }
        return vs;

    }

    public static boolean getRegularExpressionMatch(String rexp, String str) {
        boolean value = false;
        Pattern regex = Pattern.compile("\\[([^\\]]*)\\]");
        Matcher m = regex.matcher(str);
        if (m.find()) {
            value = m.matches();
        }

        return value;
    }

    public static JSON convertXMLToJSON(String xml) {
        JSON json = null;
        try {
            XMLSerializer xmlSerializer = new XMLSerializer();
            json = xmlSerializer.read(xml);
            System.out.println(json.toString(2));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return json;

    }
}
