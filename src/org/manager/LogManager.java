/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.manager;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.utility.DateUtility;

/**
 *
 * @author mrsyrop
 */
public class LogManager {

    private String store = "log";
    private String logfile = "over_all.log";
    private Logger logger;

    public LogManager() {
        FileManager file = new FileManager();
        file.setPath(this.getStore());
        this.setLogger("over_all");
    }

    public void init() {
        FileManager file = new FileManager();
        file.setPath(this.getStore());
        this.setLogger("over_all");
    }

    public void write(String type, String content) {
        FileHandler fh = null;
        try {
            fh = new FileHandler(this.getStore() + "/" + this.getLogfile(), true);
            SimpleFormatter formatterTxt = new SimpleFormatter() {
                @Override
                public String format(LogRecord record) {
                    return "[" + DateUtility.millis2DateTimeString(record.getMillis()) + "]" + record.getLevel() + " : " + record.getMessage() + "\n";
                }
            };
            fh.setFormatter(formatterTxt);
            this.logger.addHandler(fh);
            switch (type) {
                case "fine":
                    this.logger.log(Level.FINE, content);
                    break;
                case "info":
                    this.logger.log(Level.INFO, content);
                    break;
                case "warning":
                    this.logger.log(Level.WARNING, content);
                    break;
                case "error":
                    this.logger.log(Level.SEVERE, content);
                    break;
                default:
                    this.logger.log(Level.INFO, content);
                    break;
            }

        } catch (IOException ex) {
            Logger.getLogger(LogManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(LogManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            fh.close();
        }

    }

    /**
     * @return the store
     */
    public String getStore() {
        return store;
    }

    /**
     * @param store the store to set
     */
    public void setStore(String store) {
        this.store = store;
        this.init();
    }

    /**
     * @return the logfile
     */
    public String getLogfile() {
        return logfile;
    }

    /**
     * @param logfile the logfile to set
     */
    public void setLogfile(String logfile) {
        this.logfile = logfile;
    }

    /**
     * @return the logger
     */
    public Logger getLogger() {
        return logger;
    }

    /**
     * @param logger the logger to set
     */
    public void setLogger(String logger) {
        this.logger = Logger.getLogger(logger);
    }
}
