/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.manager;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.model.EventM;
import org.model.db.DBConnection;
import org.model.db.LiveLeagueDAO;
import org.model.db.LiveLeagueImpl;
import org.model.db.LiveMatchDAO;
import org.model.db.LiveMatchImpl;
import org.model.db.TeamDAO;
import org.model.db.TeamImpl;
import org.utility.Constants;
import org.utility.DateUtility;

/**
 *
 * @author superx
 */
public class FileManager extends DBConnection {

    private String path = "files";
    private String content = "";
    private String filename = "emptyfile.txt";
    private String fullpath = "";

    private static final LogManager log = new LogManager();

    public void init() {
        File destination = new File(this.getPath());
        if (!destination.isDirectory()) {
            System.out.println("not dir");
            if (destination.mkdirs()) {
                System.out.println("But created");
            } else {
                System.out.println("And not created");
            }

        }
    }

    public static boolean move(FileManager source, FileManager target) {
        boolean success = false;
        target.setContent(source.read());
        target.write();
        if (isFile(target)) {
            source.delete();
            success = true;
        }
        return success;
    }

    public static boolean isFile(FileManager source) {
        boolean success = false;
        File tmp = new File(source.getFullpath());
        if (tmp.isFile()) {
            success = true;
        }
        return success;
    }

    public void setPath(String dir) {
        this.path = dir;
        File destination = new File(this.getPath());
        if (!destination.isDirectory()) {
            //System.out.println("not dir");
            //log.write("info", this.getPath() + " is not directory");
            if (destination.mkdirs()) {
                //System.out.println("But created");
                //log.write("info", "But created");
            } else {
                //System.out.println("And not created");
                //log.write("info", "But can't create.");
            }
        } else {
            //System.out.println("found Directory " + dir);
            //log.write("info", "found Directory " + dir);
        }
        this.fullpath = this.getPath() + "/" + this.getFilename();

    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setFilename(String filename) {
        this.filename = filename;
        this.fullpath = this.getPath() + "/" + this.getFilename();
    }

    public static void writeFile(String fileName, String content) {
        FileWriter fstream = null;
        try {

            fstream = new FileWriter(fileName, false);
            BufferedWriter out = new BufferedWriter(fstream);

            out.write(content);
            out.close();
        } catch (IOException ex) {
            //Logger.getLogger(this.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fstream.close();
            } catch (IOException ex) {

                //Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static String readFile(String fileName) {
        String content = "";
        String sCurrentLine = "";
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(fileName));
            while ((sCurrentLine = br.readLine()) != null) {
                content = sCurrentLine;
            }
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            //Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return content;
    }

    public void write() {
        String file = this.getPath() + "/" + this.getFilename();
        this.setFullpath(file);
        File destination = new File(file);
        System.out.println(destination.getAbsolutePath());
        try {
            Files.write(this.getContent(), destination, Charset.forName("UTF-8"));
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    public String read() {
        String file = this.getPath() + "/" + this.getFilename();
        File destination = new File(file);
        //System.out.println(destination.getAbsolutePath());
        log.write("info", "Read file from :" + file);
        if (destination.isFile()) {
            try {
                this.content = Files.toString(destination, Charsets.UTF_8);
            } catch (IOException ex) {
                Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return this.getContent();
    }

    public boolean delete() {
        boolean success = false;
        String file = this.getPath() + "/" + this.getFilename();
        File destination = new File(file);
        success = destination.delete();
        if (success) {
            //System.out.println("Deleted :" + file);
            log.write("info", "Deleted :" + file);
        }
        return success;
    }

    public void writeFile() {
        int countLiveMatchOnly = 0;
        int countLiveMatchWaitOnly = 0;
        int countLiveMatchResultOnly = 0;
        int countYesterdayResultOnly = 0;

//        String name = new Exception("is not thrown").getStackTrace()[0].getMethodName();
//        name = this.toString() + name;
        this.connectDB();
        JSONObject json = new JSONObject();

        JSONArray liveMatchArray = new JSONArray();
        JSONArray liveLeagueArray = new JSONArray();
        TeamDAO teamDAO = new TeamImpl();

        countLiveMatchWaitOnly = Integer.parseInt(this.readFile("file/sumWait.txt"));
        countLiveMatchOnly = Integer.parseInt(this.readFile("file/sumLive.txt"));
        countLiveMatchResultOnly = Integer.parseInt(this.readFile("file/sumResult.txt"));
        countYesterdayResultOnly = Integer.parseInt(this.readFile("file/sumYesterday.txt"));

        JSONObject count = new JSONObject();
        count.put("live", countLiveMatchOnly);
        count.put("wait", countLiveMatchWaitOnly);
        count.put("result", countLiveMatchResultOnly);
        count.put("yesterday", countYesterdayResultOnly);
        JSONObject teamlogos = new JSONObject();

        // Write livematch liveleague with json format to json file ====================================
        json = new JSONObject();

        liveMatchArray = new JSONArray();
        liveLeagueArray = new JSONArray();

        JSONArray liveMatchArrayTh = new JSONArray();
        JSONArray liveLeagueArrayTh = new JSONArray();
        JSONArray[] jsonArrLm = new JSONArray[Constants.NATION.length];
        JSONArray[] jsonArrLl = new JSONArray[Constants.NATION.length];

        // Get live match to JSON Array  
        String liveOnlysql = "select  live_match.*,"
                + "gt.teamNameTh as gnTh,gt.teamNameEn as gnEn,"
                + "ht.teamNameTh as hnTh,ht.teamNameEn as hnEn\n"
                + "FROM live_match "
                + "        left join lang_team  as gt on gt.tid= live_match.gid"
                + "        left join lang_team  as ht on ht.tid = live_match.hid"
                + "        where showDate=current_date and new='Y' and sid>1 and lnr=1 group by mid order by date ASC,mid ASC";
        LiveMatchDAO liveMatchDAO = new LiveMatchImpl();
        liveMatchDAO.getLiveMatch2JSONArrayStatus(liveMatchArray, jsonArrLm, liveOnlysql);

        // Get live league to JSON Array   
        String liveleagueOnly = "select live_league.*,lang_league.*,lang_competition.*,league.priority from live_league  \n"
                + "	left join lang_league on lang_league.leagueId = live_league.leagueId\n"
                + "   left join lang_competition on  lang_competition.comName = live_league.kn\n"
                + "   left join live_match on live_match.showDate = live_league.date and live_match._lid = live_league.leagueId\n"
                + "   left join league on league.leagueId = live_league.leagueId\n"
                + "   where live_league.date=current_date \n"
                + "		and live_league.new='Y' \n"
                + "		and (live_match.sid>=1) \n"
                + "		and live_match.lnr=1 \n"
                + "   group by live_league.subleagueId \n"
                + "   order by (CASE WHEN league.priority IS NULL then 1 ELSE 0 END),league.priority ASC,live_league.competitionId ASC,live_league.leagueId ASC,live_league.subleagueId ASC";
        LiveLeagueDAO liveLeagueDAO = new LiveLeagueImpl();
        liveLeagueDAO.getLiveLeague2JSONArrayStatus(liveLeagueArray, jsonArrLl, liveleagueOnly);

        teamlogos = new JSONObject();
        for (Object lma : liveMatchArray) {
            String s = lma.toString();
            JSONObject match = (JSONObject) JSONSerializer.toJSON(s);
            int hid = match.getInt("hid");
            JSONObject home = teamDAO.getLogo(hid);
            int gid = match.getInt("gid");
            JSONObject guest = teamDAO.getLogo(gid);

            teamlogos.put(hid, home);
            teamlogos.put(gid, guest);
        }

        json.put("Teamlogos", teamlogos);
        json.put("live_match", liveMatchArray);
        json.put("live_league", liveLeagueArray);
        json.put("count", count);
        json.put("c3", (DateUtility.getTimeNow() / 1000));

        log.write("info", "xxxx live->" + countLiveMatchOnly);
        log.write("info", "xxxx wait->" + countLiveMatchWaitOnly);
        log.write("info", "xxxx result->" + countLiveMatchResultOnly);
        log.write("info", "xxxx yesterday->" + countYesterdayResultOnly);

        log.write("info", "xxxx Teamlogos size->" + teamlogos.size());
        //log.write("info", "xxxx Teamlogos->"+ teamlogos.toString());
        for (int i = 0; i < Constants.NATION.length; i++) {
            json.put("live_match", jsonArrLm[i]);
            json.put("live_league", jsonArrLl[i]);
            //this.writeFile("../../cronjob_gen_file/files/" + Constants.NATION[i].toLowerCase() + "/liveMatchOnly.json", json.toString());
            this.setPath("../../cronjob_gen_file/files/" + Constants.NATION[i].toLowerCase());
            this.setFilename("liveMatchOnly.json");
            this.setContent(json.toString());
            this.write();
        }

        // Write livematch liveleague with json format to json file by status 1 ===========================
        json = new JSONObject();

        liveMatchArray = new JSONArray();
        liveLeagueArray = new JSONArray();

        liveMatchArrayTh = new JSONArray();
        liveLeagueArrayTh = new JSONArray();
        jsonArrLm = new JSONArray[Constants.NATION.length];
        jsonArrLl = new JSONArray[Constants.NATION.length];

        // Get live match to JSON Array               
        // Get live match to JSON Array   
        String livematchWaitOnly = "select live_match.*,gt.teamNameTh as gnTh,gt.teamNameEn as gnEn,ht.teamNameTh as hnTh,ht.teamNameEn as hnEn\n"
                + "      FROM live_match \n"
                + "      left join lang_team  as gt on gt.tid= live_match.gid\n"
                + "      left join lang_team  as ht on ht.tid = live_match.hid\n"
                + "      where showDate=current_date and new='Y' and (sid=1) group by mid order by date ASC,mid ASC";
        liveMatchDAO = new LiveMatchImpl();
        liveMatchDAO.getLiveMatch2JSONArrayStatus(liveMatchArray, jsonArrLm, livematchWaitOnly);

        // Get live league to JSON Array
        String liveleagueWaitOnly = "select live_league.*,lang_league.*,lang_competition.*,league.priority from live_league  \n"
                + "     left join lang_league on lang_league.leagueId = live_league.leagueId\n"
                + "     left join lang_competition on  lang_competition.comName = live_league.kn\n"
                + "     left join live_match on live_match.showDate = live_league.date and live_match._lid = live_league.leagueId\n"
                + "     left join league on league.leagueId = live_league.leagueId\n"
                + "     where live_league.date=current_date \n"
                + "		  and live_league.new='Y' \n"
                + "		  and (live_match.sid>=1) \n"
                + "		  and live_match.lnr=1 \n"
                + "     group by live_league.subleagueId \n"
                + "     order by (CASE WHEN league.priority IS NULL then 1 ELSE 0 END),league.priority ASC,live_league.competitionId ASC,live_league.leagueId ASC,live_league.subleagueId ASC";
        liveLeagueDAO = new LiveLeagueImpl();
        liveLeagueDAO.getLiveLeague2JSONArrayStatus(liveLeagueArray, jsonArrLl, liveleagueWaitOnly);

        teamlogos = new JSONObject();
        for (Object lma : liveMatchArray) {
            String s = lma.toString();
            JSONObject match = (JSONObject) JSONSerializer.toJSON(s);
            int hid = match.getInt("hid");
            JSONObject home = teamDAO.getLogo(hid);
            int gid = match.getInt("gid");
            JSONObject guest = teamDAO.getLogo(gid);

            teamlogos.put(hid, home);
            teamlogos.put(gid, guest);
        }

        json.put("Teamlogos", teamlogos);
        json.put("live_match", liveMatchArray);
        json.put("live_league", liveLeagueArray);
        json.put("count", count);
        json.put("c3", (DateUtility.getTimeNow() / 1000));

        for (int i = 0; i < Constants.NATION.length; i++) {
            json.put("live_match", jsonArrLm[i]);
            json.put("live_league", jsonArrLl[i]);
            //this.writeFile("../../cronjob_gen_file/files/" + Constants.NATION[i].toLowerCase() + "/liveMatchWaitOnly.json", json.toString());
            this.setPath("../../cronjob_gen_file/files/" + Constants.NATION[i].toLowerCase());
            this.setFilename("liveMatchWaitOnly.json");
            this.setContent(json.toString());
            this.write();
        }

        // Write livematch liveleague with json format to json file by status 5 and more ======================
        json = new JSONObject();

        liveMatchArray = new JSONArray();
        liveLeagueArray = new JSONArray();

        liveMatchArrayTh = new JSONArray();
        liveLeagueArrayTh = new JSONArray();
        jsonArrLm = new JSONArray[Constants.NATION.length];
        jsonArrLl = new JSONArray[Constants.NATION.length];

        // Get live match to JSON Array               
        // Get live match to JSON Array               
        liveMatchDAO = new LiveMatchImpl();
        liveMatchDAO.getLiveMatch2JSONArrayStatus(liveMatchArray, jsonArrLm, Constants.LIVE_MATCH_STATUS_5UP);

        // Get live league to JSON Array                            
        liveLeagueDAO = new LiveLeagueImpl();
        liveLeagueDAO.getLiveLeague2JSONArrayStatus(liveLeagueArray, jsonArrLl, Constants.LIVE_LEAGUE_STATUS_5UP);

        teamlogos = new JSONObject();
        for (Object lma : liveMatchArray) {
            String s = lma.toString();
            JSONObject match = (JSONObject) JSONSerializer.toJSON(s);
            int hid = match.getInt("hid");
            JSONObject home = teamDAO.getLogo(hid);
            int gid = match.getInt("gid");
            JSONObject guest = teamDAO.getLogo(gid);

            teamlogos.put(hid, home);
            teamlogos.put(gid, guest);
        }

        json.put("Teamlogos", teamlogos);
        json.put("live_match", liveMatchArray);
        json.put("live_league", liveLeagueArray);
        json.put("count", count);
        json.put("c3", (DateUtility.getTimeNow() / 1000));

        for (int i = 0; i < Constants.NATION.length; i++) {
            json.put("live_match", jsonArrLm[i]);
            json.put("live_league", jsonArrLl[i]);
            this.writeFile("../../cronjob_gen_file/files/" + Constants.NATION[i].toLowerCase() + "/liveMatchResultOnly.json", json.toString());
        }
        this.closeDB();

    }

    public void writeFileYesterday() {
        int countLiveMatchOnly = 0;
        int countLiveMatchWaitOnly = 0;
        int countLiveMatchResultOnly = 0;
        int countYesterdayResultOnly = 0;

//        String name = new Exception("is not thrown").getStackTrace()[0].getMethodName();
//        name = this.toString() + name;
        this.connectDB();
        JSONObject json = new JSONObject();

        JSONArray liveMatchArray = new JSONArray();
        JSONArray liveLeagueArray = new JSONArray();
        TeamDAO teamDAO = new TeamImpl();

        countLiveMatchWaitOnly = Integer.parseInt(this.readFile("file/sumWait.txt"));
        countLiveMatchOnly = Integer.parseInt(this.readFile("file/sumLive.txt"));
        countLiveMatchResultOnly = Integer.parseInt(this.readFile("file/sumResult.txt"));
        countYesterdayResultOnly = Integer.parseInt(this.readFile("file/sumYesterday.txt"));

        JSONObject count = new JSONObject();
        count.put("live", countLiveMatchOnly);
        count.put("wait", countLiveMatchWaitOnly);
        count.put("result", countLiveMatchResultOnly);
        count.put("yesterday", countYesterdayResultOnly);
        JSONObject teamlogos = new JSONObject();

        // Write livematch liveleague with json format to json file ====================================
        json = new JSONObject();

        liveMatchArray = new JSONArray();
        liveLeagueArray = new JSONArray();

        JSONArray liveMatchArrayTh = new JSONArray();
        JSONArray liveLeagueArrayTh = new JSONArray();
        JSONArray[] jsonArrLm = new JSONArray[Constants.NATION.length];
        JSONArray[] jsonArrLl = new JSONArray[Constants.NATION.length];

        // Get live match to JSON Array  
        String liveOnlysql = "select  live_match.*,"
                + "gt.teamNameTh as gnTh,gt.teamNameEn as gnEn,"
                + "ht.teamNameTh as hnTh,ht.teamNameEn as hnEn\n"
                + "FROM live_match "
                + "        left join lang_team  as gt on gt.tid = live_match.gid"
                + "        left join lang_team  as ht on ht.tid = live_match.hid"
                + "        where showDate=current_date -interval 1 day and new='Y' and (sid>=5 and lnr=2) group by mid order by date ASC,mid ASC";
        LiveMatchDAO liveMatchDAO = new LiveMatchImpl();
        liveMatchDAO.getLiveMatch2JSONArrayStatus(liveMatchArray, jsonArrLm, liveOnlysql);

        // Get live league to JSON Array   
        String liveleagueOnly = "select live_league.*,"
                + "lang_league.*,"
                + "lang_competition.*,"
                + "league.priority from live_league  "
                + "            left join lang_league on lang_league.leagueId = live_league.leagueId"
                + "            left join lang_competition on  lang_competition.comName = live_league.kn"
                + "            left join live_match on live_match.showDate = live_league.date and live_match._lid = live_league.leagueId"
                + "            left join league on league.leagueId = live_league.leagueId"
                + "            where live_league.date=current_date -interval 1 day and live_league.new='Y' and (live_match.sid>=5 and lnr=2) "
                + "            group by live_league.subleagueId "
                + "            order by (CASE WHEN league.priority IS NULL then 1 ELSE 0 END),league.priority ASC,live_league.competitionId ASC,live_league.leagueId ASC,live_league.subleagueId ASC";
        LiveLeagueDAO liveLeagueDAO = new LiveLeagueImpl();
        liveLeagueDAO.getLiveLeague2JSONArrayStatus(liveLeagueArray, jsonArrLl, liveleagueOnly);

        teamlogos = new JSONObject();
        for (Object lma : liveMatchArray) {
            String s = lma.toString();
            JSONObject match = (JSONObject) JSONSerializer.toJSON(s);
            int hid = match.getInt("hid");
            JSONObject home = teamDAO.getLogo(hid);
            int gid = match.getInt("gid");
            JSONObject guest = teamDAO.getLogo(gid);

            teamlogos.put(hid, home);
            teamlogos.put(gid, guest);
        }

        json.put("Teamlogos", teamlogos);
        json.put("live_match", liveMatchArray);
        json.put("live_league", liveLeagueArray);
        json.put("count", count);
        json.put("c3", (DateUtility.getTimeNow() / 1000));

        log.write("info", "xxxx live->" + countLiveMatchOnly);
        log.write("info", "xxxx wait->" + countLiveMatchWaitOnly);
        log.write("info", "xxxx result->" + countLiveMatchResultOnly);
        log.write("info", "xxxx yesterday->" + countYesterdayResultOnly);

        log.write("info", "xxxx Teamlogos size->" + teamlogos.size());
        //log.write("info", "xxxx Teamlogos->"+ teamlogos.toString());
        for (int i = 0; i < Constants.NATION.length; i++) {
            json.put("live_match", jsonArrLm[i]);
            json.put("live_league", jsonArrLl[i]);
            //this.writeFile("../../cronjob_gen_file/files/" + Constants.NATION[i].toLowerCase() + "/liveMatchOnly.json", json.toString());
            this.setPath("../../cronjob_gen_file/files/" + Constants.NATION[i].toLowerCase());
            this.setFilename("liveMatchYesterdayResultOnly.json");
            this.setContent(json.toString());
            this.write();
        }
        this.closeDB();

    }

    public void writeFileLiveMatchOnly() {
        int countLiveMatchOnly = 0;
        int countLiveMatchWaitOnly = 0;
        int countLiveMatchResultOnly = 0;
        int countYesterdayResultOnly = 0;

        JSONObject json = new JSONObject();
        JSONArray liveMatchArray = new JSONArray();
        JSONArray liveLeagueArray = new JSONArray();
        TeamDAO teamDAO = new TeamImpl();
        try {

            countLiveMatchWaitOnly = Integer.parseInt(this.readFile("file/sumWait.txt"));
            countLiveMatchOnly = Integer.parseInt(this.readFile("file/sumLive.txt"));
            countLiveMatchResultOnly = Integer.parseInt(this.readFile("file/sumResult.txt"));
            countYesterdayResultOnly = Integer.parseInt(this.readFile("file/sumYesterday.txt"));
            JSONObject count = new JSONObject();
            count.put("live", countLiveMatchOnly);
            count.put("wait", countLiveMatchWaitOnly);
            count.put("result", countLiveMatchResultOnly);
            count.put("yesterday", countYesterdayResultOnly);

            JSONArray liveMatchArrayTh = new JSONArray();
            JSONArray liveLeagueArrayTh = new JSONArray();
            JSONArray[] jsonArrLm = new JSONArray[Constants.NATION.length];
            JSONArray[] jsonArrLl = new JSONArray[Constants.NATION.length];
            for (int i = 0; i < jsonArrLm.length; i++) {
                jsonArrLm[i] = new JSONArray();
                jsonArrLl[i] = new JSONArray();
            }
            JSONObject teamlogos = new JSONObject();

            if (countLiveMatchOnly > 0) {
                LiveMatchImpl liveMatchDAO = new LiveMatchImpl();
                liveMatchDAO.getLiveMatch2JSONArrayStatus(liveMatchArray, jsonArrLm, Constants.LIVE_MATCH_GET_LIVE_ONLY);
                LiveLeagueImpl liveLeagueDAO = new LiveLeagueImpl();
                liveLeagueDAO.getLiveLeague2JSONArrayStatus(liveLeagueArray, jsonArrLl, Constants.LIVE_LEAGUE_LIVE_MATCH_ONLY);
            }

            teamlogos = new JSONObject();
            for (Object lma : liveMatchArray) {
                String s = lma.toString();
                JSONObject match = (JSONObject) JSONSerializer.toJSON(s);
                int hid = match.getInt("hid");
                JSONObject home = teamDAO.getLogo(hid);
                int gid = match.getInt("gid");
                JSONObject guest = teamDAO.getLogo(gid);

                teamlogos.put(hid, home);
                teamlogos.put(gid, guest);
            }
            
            json.put("Teamlogos", teamlogos);
            json.put("live_match", liveMatchArray);
            json.put("live_league", liveLeagueArray);
            json.put("count", count);
            json.put("c3", (DateUtility.getTimeNow() / 1000));

            log.write("info", "xxxx live->" + countLiveMatchOnly);
            log.write("info", "xxxx wait->" + countLiveMatchWaitOnly);
            log.write("info", "xxxx result->" + countLiveMatchResultOnly);
            log.write("info", "xxxx yesterday->" + countYesterdayResultOnly);

            for (int i = 0; i < Constants.NATION.length; i++) {
                json.put("live_match", jsonArrLm[i]);
                json.put("live_league", jsonArrLl[i]);
                this.writeFile("../../cronjob_gen_file/files/" + Constants.NATION[i].toLowerCase() + "/liveMatchOnly.json", json.toString());
            }

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();

        } finally {

        }

    }

    public boolean writeLiveEventJson(Vector<EventM> eventMvt, int mid, String event_id) {
        boolean returnValue = false;
        connectDB();
        try {
            //
            //System.out.println("oonh");

            JSONArray array = new JSONArray();
            java.util.Date date = new java.util.Date();
            for (EventM l : eventMvt) {

                JSONObject obj = new JSONObject();
                obj.put("aid", l.getAid());
                obj.put("m", l.getM() + "");
                obj.put("m2", l.getM2() + "");
                obj.put("mr", l.getMr() + "");
                obj.put("pz", l.getPz() + "");
                obj.put("ph", l.getPh() + "");
                obj.put("pg", l.getPg() + "");
                obj.put("pw", l.getPw() + "");
                obj.put("h", l.getH() + "");
                obj.put("g", l.getG() + "");
                obj.put("mid", l.getMid() + "");
                array.add(obj);

            }
            if (eventMvt.size() > 0) {
//                connectDB();
                stmt.executeUpdate("update event set data_checked='Y' where event_id =" + event_id);
                //closeDB();
                JSONObject object = new JSONObject();
                object.put("live_match_event", array);
                long time = date.getTime() / 1000L;
                object.put("c3", time);
                //System.out.println("../../gen_file_events/" + mid + ".json");
                // FileController.writeFile("../../gen_file_events/" + mid + ".json", object.toString());
                FileManager fileManager = new FileManager();
                fileManager.setPath(Constants.LIVE_EVENT_DIR);
                fileManager.setFilename(mid + ".json");
                fileManager.setContent(object.toString());
                fileManager.write();
                //System.out.println(object.toString());

            }

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();

        } finally {
            closeDB();
        }
        return returnValue;

    }

    public boolean writeLiveResultEventJson(Vector<EventM> eventMvt, int mid) {
        boolean returnValue = false;
        this.connectDB();
        try {
            //
            //System.out.println("oonh");
            String query = "delete from live_match_event where mid=" + mid;
            int j = this.stmt.executeUpdate(query);
            if (j != 0) {
                returnValue = true;
            }

            JSONArray array = new JSONArray();
            java.util.Date date = new java.util.Date();
            for (EventM l : eventMvt) {
                query = "insert into live_match_event (aid,m,m2,mr,pz,ph,pg,pw,h,g,mid) values("
                        + l.getAid() + ",\"" + l.getM() + "\",\"" + l.getM2() + "\"," + l.getMr() + ",\"" + l.getPz() + "\",\"" + l.getPh() + "\",\"" + l.getPg() + "\",\"" + l.getPw() + "\",\"" + l.getH() + "\",\"" + l.getG() + "\"," + l.getMid() + ")";
                int i = stmt.executeUpdate(query);
                if (i != 0) {
                    returnValue = true;
                    JSONObject obj = new JSONObject();
                    obj.put("aid", (l.getAid() > 0 ? l.getAid() : null) + "");
                    obj.put("m", l.getM() + "");
                    obj.put("m2", l.getM2() + "");
                    obj.put("mr", l.getMr() + "");
                    obj.put("pz", l.getPz() + "");
                    obj.put("ph", l.getPh() + "");
                    obj.put("pg", l.getPg() + "");
                    obj.put("pw", l.getPw() + "");
                    obj.put("h", l.getH() + "");
                    obj.put("g", l.getG() + "");
                    obj.put("mid", l.getMid() + "");
                    array.add(obj);
                }
            }

            if (eventMvt.size() > 0) {
                JSONObject object = new JSONObject();
                object.put("live_match_event", array);
                long time = date.getTime() / 1000L;
                object.put("c3", time);
                //System.out.println("../../gen_file_events/" + vl.get(0).getMid() + ".json");
                //FileController.writeFile("../../gen_file_events/" + vl.get(0).getMid() + ".json", object.toString());
                FileManager fileManager = new FileManager();
                fileManager.setPath(Constants.LIVE_EVENT_DIR);
                fileManager.setFilename(mid + ".json");
                fileManager.setContent(object.toString());
                fileManager.write();

            }

        } catch (Exception ex) {
            //closeDB();
            ex.printStackTrace();

        } finally {
            closeDB();
        }
        return returnValue;

    }

    public static String getOldestFile(String dir) {

        File folder = new File(dir);
        File[] listOfFiles = folder.listFiles();
        int olderFileIndex = 0;
        long olderFileTime = 0;
        boolean bChk = false;

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                bChk = true;
                if (0 == i) {
                    olderFileTime = listOfFiles[i].lastModified();
                } else if (olderFileTime > listOfFiles[i].lastModified()) {
                    olderFileTime = listOfFiles[i].lastModified();
                    olderFileIndex = i;
                }
            }
        }

        if (true == bChk) {
            //System.out.print(" File " + listOfFiles[olderFileIndex].getName());
            //System.out.println(" Time " + listOfFiles[olderFileIndex].lastModified());
            return listOfFiles[olderFileIndex].getName();
        } else {
            //System.out.println("Has not found the file");
            return "";
        }
    }

    void setContent(int sumWait) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the fullpath
     */
    public String getFullpath() {
        return fullpath;
    }

    /**
     * @param fullpath the fullpath to set
     */
    public void setFullpath(String fullpath) {
        this.fullpath = fullpath;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

}
