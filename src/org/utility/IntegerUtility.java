/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.utility;

/**
 *
 * @author superx
 */
public final class IntegerUtility {
    
    public static int string2Int(String strNumber){
        
        int nReturn = 0;
        
        nReturn = ("".equals(strNumber)) ? 0 : Integer.parseInt(strNumber);
        
        return nReturn;
    }
    
    
}
