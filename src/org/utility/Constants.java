/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.utility;

import org.manager.LogManager;

/**
 *
 * @author superx
 */
public final class Constants {

    /*===== Log  ======*/
    public static final String LOG_LEVEL_INFO = "info";
    /*===== Application ======*/
    public static final String XML_ENCODE = "utf-8";
    public static final String NATION[] = {"Th", "En"};

    /*===== Live Match ======*/
    public static final String LIVE_MATCH_XML_PATH = "/media/superx/DATA/project/baldata";
    public static final String LIVE_MATCH_NEW_ENTRY = "spiderbotdoc/newfile";
    public static final String LIVE_MATCH_NEW_ACTION = "spiderbotdoc/action";
    public static final String LIVE_MATCH_EXCUITED = "spiderbotdoc/excuited";
    public static final String LIVE_MATCH_YESTERDAY = "spiderbotdoc/live_yesterday";
    public static final String LIVE_MATCH_UPDATED_PATH = "../../cronjob_gen_file/files";
    public static final String BALL24_COMPETITION = "spiderbotdoc/competition";
    public static final String BALL24_TEAM_PK = "spiderbotdoc/teamPK";

    public static final String LIVE_MATCH_XML_ROOT_NODE = "f24";
    public static final String LIVE_MATCH_XML_LEAGUE_NODE = "kraje";
    public static final String LIVE_MATCH_XML_MATCH_NODE = "mecze";
    public static final String LIVE_MATCH_XML_EVENT_NODE = "zd";

    public static final int LIVE_MATCH_STATUS_NONE = 0;
    public static final int LIVE_MATCH_STATUS_1 = 1;
    public static final int LIVE_MATCH_STATUS_5UP = 5;

    /*===== Live League ======*/
    public static final int LIVE_LEAGUE_STATUS_NONE = 0;
    public static final int LIVE_LEAGUE_STATUS_1 = 1;
    public static final int LIVE_LEAGUE_STATUS_5UP = 5;

    /*===== Ball24 Website ======*/
    public static final String BALL24_LIVE_MATCH_URL = "http://www.futbol24.com/f24/u/liveNow_15.xml?timestamp=";
    public static final String BALL24_LIVE_MATCH_YESTERDAY_URL = "http://www.futbol24.com/matchDayXml/?Day=";
    public static final String MATCH_ACTION_XML = "http://www.futbol24.com/ml/matchActionXml/?MId=";
    public static final String BALL24_MAIN_PAGE="http://www.futbol24.com/";
    /*===== Query for live data ======*/
 /*===== Live match ======*/
    public static final String LIVE_MATCH_GET_SUM_VALUE = ""
            + "select "
            + "     sum(if(sid=1,1,0)) as  wait, "
            + "     sum(if(sid>1 and lnr=1,1,0)) as live,"
            + "     sum(if(sid>4 and lnr=2,1,0)) as result "
            + "from  "
            + "     live_match "
            + "where "
            + "     showDate=current_date "
            + "     and new='Y' ";

    public static final String LIVE_MATCH_GET_YESTERDAY_COUNT = ""
            + "select "
            + "     count(*) as count "
            + "from "
            + "     live_match "
            + "where "
            + "     showDate=current_date - interval 1 day "
            + "     and new='Y' ";
    public static final String LIVE_MATCH_GET_DATA_WITH_TEAMS = ""
            + "select "
            + "     live_match.*, "
            + "     gt.teamNameTh as gnTh, "
            + "     ht.teamNameTh as hnTh "
            + "FROM "
            + "     live_match "
            + "     left join  "
            + "     lang_team  as gt "
            + "         on gt.tid = live_match.gid "
            + "         left join "
            + "         lang_team  as ht "
            + "             on ht.tid = live_match.hid "
            + "where "
            + "     showDate=current_date and "
            + "     new='Y' "
            + "group by "
            + "     mid "
            + "order by "
            + "     date ASC,"
            + "     mid ASC ";
    public static final String LIVE_MATCH_GET_DATA_WITH_TEAMS_STATUS_1 = ""
            + "select  "
            + "     live_match.*,"
            + "     gt.teamNameTh as gnTh,"
            + "     gt.teamNameEn as gnEn,"
            + "     ht.teamNameTh as hnTh,"
            + "     ht.teamNameEn as hnEn "
            + "FROM "
            + "     live_match "
            + "     left join "
            + "     lang_team  as gt "
            + "         on gt.tid = live_match.gid "
            + "         left join "
            + "         lang_team  as ht "
            + "             on ht.tid = live_match.hid "
            + "where "
            + "     showDate=current_date "
            + "     and new='Y' "
            + "     and (sid=1) "
            + "group by "
            + "     mid "
            + "order by "
            + "     date ASC,"
            + "     mid ASC";

    public static final String LIVE_MATCH_GET_DATA_WITH_TEAMS_STATUS_5UP = ""
            + "select  "
            + "     live_match.*,"
            + "     gt.teamNameTh as gnTh,"
            + "     gt.teamNameEn as gnEn,"
            + "     ht.teamNameTh as hnTh,"
            + "     ht.teamNameEn as hnEn "
            + "FROM "
            + "     live_match "
            + "     left join "
            + "     lang_team  as gt "
            + "         on gt.tid = live_match.gid "
            + "         left join "
            + "         lang_team  as ht "
            + "             on ht.tid = live_match.hid "
            + "where "
            + "     showDate=current_date "
            + "     and new='Y' "
            + "     and (sid>=5) "
            + "     and lnr=2 "
            + "group by "
            + "     mid "
            + "order by "
            + "     date ASC,"
            + "     mid ASC";

    public static final String LIVE_MATCH_GET_LIVE_ONLY = "select  live_match.*,"
            + "gt.teamNameTh as gnTh,gt.teamNameEn as gnEn,"
            + "ht.teamNameTh as hnTh,ht.teamNameEn as hnEn\n"
            + "FROM live_match "
            + "        left join lang_team  as gt on gt.tid= live_match.gid"
            + "        left join lang_team  as ht on ht.tid = live_match.hid"
            + "        where showDate=current_date and new='Y' and sid>1 and lnr=1 group by mid order by date ASC,mid ASC";
    //System.out.println(query);

    public static final String LIVE_MATCH_WITHOUT_11 = "select event.event_id,event.mid,ao from  live_match left join event  on event.mid = live_match.mid  where data_checked='N' and showDate =current_date and sid!=11";
    public static final String LIVE_MATCH_LAST_EVENT = "select event.event_id,event.mid,ao from  event left join live_match  on event.mid = live_match.mid  order by event.event_id DESC limit 5";
    public static final String LIVE_MATCH_RESULT_EVENT = "select mid,ao,s1,s2,hrci,grci,hrc,grci from live_match where showDate=current_date and  (live_match.sid>=5 or live_match.sid=3)";
    /*===== Live league ======*/
    public static final String LIVE_LEAGUE_GET_DATA_WITH_TEAMS = ""
            + "select "
            + "     live_league.*,"
            + "     lang_league.leagueNameTh, "
            + "     lang_competition.comNameTh, "
            + "     lang_league.priority "
            + "from "
            + "     live_league  "
            + "            left join "
            + "     lang_league "
            + "         on lang_league.leagueName = live_league.ln "
            + "         left join "
            + "         lang_competition "
            + "             on  lang_competition.comName = live_league.kn "
            + "where "
            + "     date=current_date "
            + "     and new='Y' "
            + "order by "
            + "     competitionId ASC,"
            + "     leagueId ASC,"
            + "     subleagueId ASC ";
    public static final String LIVE_LEAGUE_GET_DATA_WITH_TEAMS_STATUS_1 = ""
            + "select "
            + "     live_league.*,"
            + "     lang_league.*,"
            + "     lang_competition.*,"
            + "     league.priority "
            + "from "
            + "     live_league  "
            + "     left join lang_league on lang_league.leagueId = live_league.leagueId "
            + "     left join lang_competition on  lang_competition.comName = live_league.kn "
            + "     left join live_match on live_match.showDate = live_league.date and live_match._lid = live_league.leagueId "
            + "     left join league on league.leagueId = live_league.leagueId "
            + "where "
            + "     live_league.date=current_date "
            + "     and live_league.new='Y' "
            + "     and (live_match.sid=1) "
            + "group by "
            + "     live_league.subleagueId "
            + "order by "
            + "     (CASE WHEN league.priority IS NULL then 1 ELSE 0 END),"
            + "     league.priority ASC,"
            + "     live_league.competitionId ASC,"
            + "     live_league.leagueId ASC,"
            + "     live_league.subleagueId ASC";
    public static final String LIVE_LEAGUE_GET_DATA_WITH_TEAMS_STATUS_5UP = ""
            + "select "
            + "     live_league.*,"
            + "     lang_league.*,"
            + "     lang_competition.*,"
            + "     league.priority "
            + "from "
            + "     live_league  "
            + "     left join lang_league on lang_league.leagueId = live_league.leagueId "
            + "     left join lang_competition on  lang_competition.comName = live_league.kn "
            + "     left join live_match on live_match.showDate = live_league.date and live_match._lid = live_league.leagueId "
            + "     left join league on league.leagueId = live_league.leagueId "
            + "where "
            + "     live_league.date=current_date "
            + "     and live_league.new='Y' "
            + "     and (live_match.sid>=5) "
            + "     and lnr=2 "
            + "group by "
            + "     live_league.subleagueId "
            + "order by "
            + "     (CASE WHEN league.priority IS NULL then 1 ELSE 0 END),"
            + "     league.priority ASC,"
            + "     live_league.competitionId ASC,"
            + "     live_league.leagueId ASC,"
            + "     live_league.subleagueId ASC";
    public static final String LIVE_LEAGUE_LIVE_MATCH_ONLY = "select live_league.*,"
            + "lang_league.*,"
            + "lang_competition.*,"
            + "league.priority from live_league  "
            + "            left join lang_league on lang_league.leagueId = live_league.leagueId"
            + "            left join lang_competition on  lang_competition.comName = live_league.kn"
            + "            left join live_match on live_match.showDate = live_league.date and live_match._lid = live_league.leagueId"
            + "            left join league on league.leagueId = live_league.leagueId"
            + "            where live_league.date=current_date and live_league.new='Y' and (live_match.sid>=1) and live_match.lnr=1 "
            + "            group by live_league.subleagueId "
            + "            order by (CASE WHEN league.priority IS NULL then 1 ELSE 0 END),league.priority ASC,live_league.competitionId ASC,live_league.leagueId ASC,live_league.subleagueId ASC";
    /*===== Live file ======*/
    public static final String LIVE_FILE_GET_LAST_ROW = ""
            + "select"
            + "     * "
            + "from "
            + "     live_file "
            + "where "
            + "     plinkXML!='0' "
            + "order by "
            + "     date desc "
            + "limit "
            + "     1";

    /*==== Last Update ====*/
 /*==== Live Event ====*/
    public static final String LIVE_EVENT_DIR = "./../gen_file_events";
    
    
    /*======== Competition ========*/
    public static final String GET_COMPETITION_BY_ID= "select *from competitions  where competitionId=";
}
