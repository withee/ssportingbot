/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.utility;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author superx
 */
public final class DateUtility {

    public static Date getDateNow() {
        return new java.util.Date();
    }

    public static String getDateYesterday() {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        System.out.println("Today's date is " + dateFormat.format(cal.getTime()));

        cal.add(Calendar.DATE, -1);
        return dateFormat.format(cal.getTime());
    }

    public static long getTimeNow() {

        Date date = new Date();
        return date.getTime();

    }

    public static String millis2DateTimeString(long millis) {
        Timestamp stamp = new Timestamp(millis);
        Date date = new Date(stamp.getTime());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return dateFormat.format(date);
    }

    public static String getDateNowyyyy_MM_dd() {
        Date dd = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT0:00"));
        String df = formatter.format(dd);
        return df;
    }

    public static String convertYYYYMMDD2yyyy_MM_dd(String strDate) {
        // 20160115 -> 2016-01-15
        //log.write(Constants.LOG_LEVEL_INFO, "in public static String convertYYYYMMDD2yyyy_MM_dd(String strDate){");
        //log.write(Constants.LOG_LEVEL_INFO, "strDate->" + strDate);
        if (!"".equals(strDate.trim())) {
            String strYear = strDate.substring(0, 4);
            //log.write(Constants.LOG_LEVEL_INFO, "strYear->" + strYear);
            String strMonth = strDate.substring(4, 6);
            //log.write(Constants.LOG_LEVEL_INFO, "strMonth->" + strMonth);
            String strDay = strDate.substring(6, 8);
            //log.write(Constants.LOG_LEVEL_INFO, "strDay->" + strDay);

            //log.write(Constants.LOG_LEVEL_INFO, "out public static String convertYYYYMMDD2yyyy_MM_dd(String strDate){");
            return strYear + "-" + strMonth + "-" + strDay;
        } else {
            return "";
        }
    }

    public static String getGMT0(long c0) {
        Date d = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT0:00"));
        d.setTime(c0 * 1000L);
        String df = formatter.format(d);
        String dateAr[] = df.split("-");
        return df;
    }

}
