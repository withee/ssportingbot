/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.utility;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

/**
 *
 * @author superx
 */
public final class XMLUtility {
    
    public static Element getRoot(String xmlFile){
        
        Document document = null;
        Element rootElement = null;
        Element element = null;
        Elements elements = null;
        List<Node> node = null;
        try {
            
            document = Jsoup.parse(new File(xmlFile), Constants.XML_ENCODE);
            // html->body->f24
            rootElement = document.select(Constants.LIVE_MATCH_XML_ROOT_NODE).first();
            
            //return rootElement;
            // Get root
            //System.out.println(" Document -> " +rootElement.outerHtml());
//            if(0 < rootElement.childNodeSize()){
//                element = rootElement.child(0);
//                System.out.println("Child : " + element.outerHtml());
//            }else{
//               rootElement = null; 
//            }
            //rootElement = document.select("f24").first(); 
            
            //return rootElement;
        } catch (IOException ex) {
            Logger.getLogger(XMLUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return rootElement;
            
    }
    
    public static Element getElementFromXML(String xmlFile,String elementName){
        
        Document document = null;
        Element rootElement = null;
        Element element = null;
        Elements elements = null;
        List<Node> node = null;
        try {
            
            document = Jsoup.parse(new File(xmlFile), Constants.XML_ENCODE);
            
            rootElement = document.select(elementName).first();
            
        } catch (IOException ex) {
            Logger.getLogger(XMLUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return rootElement;
            
    }

    public static Element getElementByName(Element elementRoot,String elementName){
        
        Element element = null;
        
        element = elementRoot.select(elementName).first();
            
        return element;
            
    }

    public static void displayAttribute(Element element){
        
        System.out.println("Element Name : " + element.nodeName());
        Attributes attributes = element.attributes();
        for (Attribute attr : attributes) {
            System.out.print("Name : " + attr.getKey());
            System.out.println(" | Value : " + attr.getValue());
        }

        
    }
    
    public static String displayAttribute2String(Element element){
        
        String str = "Element Name : " + element.nodeName() + "\n";
        Attributes attributes = element.attributes();
        for (Attribute attr : attributes) {
            str += "Name : " + attr.getKey();
            str += " | Value : " + attr.getValue() + "\n";
        }

        return str;
        
    }
    
    public static HashMap<String, String> attribute2HashMap(Element element){
        
        HashMap<String, String> map = new HashMap<String, String>();
        
        if(null != element){
            
            
            Attributes attributes = element.attributes();
            for (Attribute attr : attributes) {
                //System.out.print("Name : " + attr.getKey());
                //System.out.println(" | Value : " + attr.getValue());
                map.put(attr.getKey(), attr.getValue());
            }
            
         }
        
        return map;
    }
    
    public static void displayHMAttribute(HashMap<String, String> map){
        
        Iterator<String> keySetIterator = map.keySet().iterator(); 
        System.out.println("====================================");
        while(keySetIterator.hasNext()){ 
            String key = keySetIterator.next(); 
            System.out.println("key: " + key + " value: " + map.get(key)); 
        } 

        
    }
    
    public static Elements getChildren(Element element){
        
        Elements elements = element.children();
        
        return elements;        
    }
    
    public static void displayChildrenName(Elements elements){
        
        int index = 0;
        for (Element elm : elements){
            System.out.println("Child["+(++index)+"] : " + elm.nodeName());
        }
        
    }
}
