/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssportingbot;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.model.LiveFileM;
import org.controller.*;
import org.manager.LogManager;
import org.manager.ProxyManager;
import org.manager.spider.OddManager;
import org.model.db.LiveFileDAO;
import org.model.db.LiveFileImpl;
import org.utility.Constants;

/**
 *
 * @author superx
 */
public class Tester {

    /**
     * @param args the command line arguments
     */
    private static final LogManager log = new LogManager();

    public static void main(String[] args) {
        // TODO code application logic here
        //System.out.println("Hello World!");

        int ntest = 1;

        if (-5 == ntest) {
            SpiderController spider = new SpiderController();
            //while (true) {
            java.util.Date date = new java.util.Date();
            System.out.println("Run at:" + date.toLocaleString());
            try {

                //new ProxyManager();
                spider.getLeagueResultContent();
                spider.getLeagueFixtureContent();
                spider.getLeagueStatisticContent();
                //TimeUnit.MINUTES.sleep(1);
            } catch (Exception ex) {
                Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
            }
            //}

            // Excuite 1. livefile run for -> every 3 minutes
        } else if (-4 == ntest) {
            SpiderController spider = new SpiderController();
            //while (true) {
            java.util.Date date = new java.util.Date();
            System.out.println("Run at:" + date.toLocaleString());
            try {

                //new ProxyManager();
                spider.getTeamResult();
                //TimeUnit.MINUTES.sleep(1);
            } catch (Exception ex) {
                Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
            }
            //}

            // Excuite 1. livefile run for -> every 3 minutes
        } else if (-3 == ntest) {
            SpiderController spider = new SpiderController();
            //while (true) {
            java.util.Date date = new java.util.Date();
            System.out.println("Run at:" + date.toLocaleString());
            try {

                //new ProxyManager();
                spider.getTeamCompare();
                //TimeUnit.MINUTES.sleep(1);
            } catch (Exception ex) {
                Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
            }
            //}

            // Excuite 1. livefile run for -> every 3 minutes
        } else if (-2 == ntest) {
            SpiderController spider = new SpiderController();
            //while (true) {
            java.util.Date date = new java.util.Date();
            System.out.println("Run at:" + date.toLocaleString());
            try {

                //new ProxyManager();
                spider.getTeamFixture();
                //TimeUnit.MINUTES.sleep(1);
            } catch (Exception ex) {
                Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
            }
            //}

            // Excuite 1. livefile run for -> every 3 minutes
        } else if (-1 == ntest) {
            SpiderController spider = new SpiderController();
            //while (true) {
            java.util.Date date = new java.util.Date();
            System.out.println("Run at:" + date.toLocaleString());
            try {

                //new ProxyManager();
                spider.getTeamPk();
                //TimeUnit.MINUTES.sleep(1);
            } catch (Exception ex) {
                Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
            }
            //}

            // Excuite 1. livefile run for -> every 3 minutes
        } else if (0 == ntest) {
            SpiderController spider = new SpiderController();
            //while (true) {
            java.util.Date date = new java.util.Date();
            System.out.println("Run at:" + date.toLocaleString());
            try {

                //new ProxyManager();
                spider.getLiveMatch();
                spider.getLiveMatchYesterday();
                spider.getLiveEvent();
                spider.getCompetition();
                //TimeUnit.MINUTES.sleep(1);
            } catch (Exception ex) {
                Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
            }
            //}

            // Excuite 1. livefile run for -> every 3 minutes
        } else if (1 == ntest) {

            log.write(Constants.LOG_LEVEL_INFO, "From Main 2: Excuite livematch xml -> }else if(1 == ntest){   ");

            //while (true) {
            try {
                LiveMatchController liveMatchController = new LiveMatchController();
                liveMatchController.excuiteLiveMatch(); // LiveMatchController.getLive() {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException ex) {
                Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
            }
            //}
            //LiveFileDAO liveFileDAO = new LiveFileImpl();
            //LiveFileM liveFileB = liveFileDAO.getLiveFile();
            // Excuite 2. getUpdateInfinity run for -> every 5 minutes 
        } else if (2 == ntest) {

            log.write(Constants.LOG_LEVEL_INFO, "From Main 3: Excuite UpdateLiveMatchInfinity xml -> }else if(2 == ntest){   ");

            LiveMatchController liveMatchController = new LiveMatchController();
            liveMatchController.runLiveMatchUpdateInfinity(); // LiveMatchController.getLive() {

//            LiveFileDAO liveFileDAO = new LiveFileImpl();
//            LiveFileM liveFileB = liveFileDAO.getLiveFile();
            log.write(Constants.LOG_LEVEL_INFO, "From Main 3: Excuite UpdateLiveMatchInfinity xml -> Successfully !!!!!!!!!   ");

            // Excuite 3. liveMatchEventInfinity run for -> every 5 minutes
        } else if (3 == ntest) {

            LiveMatchController liveMatchController = new LiveMatchController();
            liveMatchController.liveMatchEventInfinity(); // LiveMatchController.getLive() {

            log.write(Constants.LOG_LEVEL_INFO, "From Main 3 : Excuite liveMatchEventInfinity -> Successfully !!!!!!!!!   ");

            // Excuite 3. UpdateLiveMatchInfinity run for -> every 5 minutes
        } else if (4 == ntest) {
            log.write(Constants.LOG_LEVEL_INFO, "From Main 4 : Excuite liveMatchEventInfinity2 -> }else if(4 == ntest){");

            LiveMatchController liveMatchController = new LiveMatchController();
            liveMatchController.liveMatchEventInfinity2(); // LiveMatchController.getLive() {

            log.write(Constants.LOG_LEVEL_INFO, "From Main 4 : Excuite liveMatchEventInfinity2 -> Successfully !!!!!!!!!   ");
        } else if (5 == ntest) {
            log.write(Constants.LOG_LEVEL_INFO, "From Main 5 : Excuite liveMatchLastEvent(); -> }else if(4 == ntest){");

            LiveMatchController liveMatchController = new LiveMatchController();
            liveMatchController.liveMatchLastEvent(); // LiveMatchController.getLive() {

            log.write(Constants.LOG_LEVEL_INFO, "From Main 5 : liveMatchLastEvent(); -> Successfully !!!!!!!!!   ");
        } else if (6 == ntest) {
            log.write(Constants.LOG_LEVEL_INFO, "From Main 6 : Excuite liveMatchLastEvent(); -> }else if(4 == ntest){");

            LiveMatchController liveMatchController = new LiveMatchController();
            liveMatchController.liveMatchResultEvent(); // LiveMatchController.getLive() {

            log.write(Constants.LOG_LEVEL_INFO, "From Main 6 : liveMatchLastEvent(); -> Successfully !!!!!!!!!   ");
        } else if (7 == ntest) {
            log.write(Constants.LOG_LEVEL_INFO, "From Main 7: Excuite UpdateLiveMatchInfinity xml -> }else if(2 == ntest){   ");

            LiveMatchController liveMatchController = new LiveMatchController();
            liveMatchController.runLiveYesterday(); // LiveMatchController.getLive() {

//            LiveFileDAO liveFileDAO = new LiveFileImpl();
//            LiveFileM liveFileB = liveFileDAO.getLiveFile();
            log.write(Constants.LOG_LEVEL_INFO, "From Main 7: Excuite UpdateLiveMatchInfinity xml -> Successfully !!!!!!!!!   ");
        } else if (8 == ntest) {
            log.write(Constants.LOG_LEVEL_INFO, "From Main 8: runLiveVS()");

            LiveMatchController liveMatchController = new LiveMatchController();
            liveMatchController.runLiveVS();
            log.write(Constants.LOG_LEVEL_INFO, "From Main 8: runLiveVS()");
        } else if (9 == ntest) {
            log.write(Constants.LOG_LEVEL_INFO, "IN From Main 9: liveLeagueController.getLeagueInfo();");

            //LiveMatchController liveMatchController = new LiveMatchController();
            LiveLeagueController liveLeagueController = new LiveLeagueController();
            liveLeagueController.getLeagueInfo();
            log.write(Constants.LOG_LEVEL_INFO, "OUT From Main 9:  liveLeagueController.getLeagueInfo();");
        } else if (10 == ntest) {
            log.write(Constants.LOG_LEVEL_INFO, "IN From Main 10: Node 7m_top_player_score_write.js ");

            //LiveMatchController liveMatchController = new LiveMatchController();
            OddManager oddManager = new OddManager();
            oddManager.oddsToday();
            log.write(Constants.LOG_LEVEL_INFO, "OUT From Main 10:  Node 7m_top_player_score_write.js ");
        }

        // ***************************** Get content bol******************************************
        // Excuite 0. Get livematch xml from ball24 site 
        //      Read liveNow_15.xml return Document
        // ********************************liveNow_15.xml***************************************
        // Folder for 1(after read move to 2) -> 2  (after read delete it)
        // Excuite 1. livefile run for -> every 3 minutes *** ok doesn't include logo teams
        //      Read liveNow_15.xml 
        //      Write
        //          sumWait.txt
        //          sumLive.txt
        //          sumResult.txt
        //          liveMatchOnly.json
        //          liveMatchWaitOnly.json
        //          liveMatchResultOnly.json
        //          liveMatchLastEvent.json
        //      Database
        //          live_file
        //          live_league
        //          live_match
        //          timelines_game
        // Excuite 2. getUpdateInfinity run for -> every 5 minutes *** ok doesn't include logo teams
        //      Read liveNow_15.xml
        //      Write liveMatchUpdate.json
        //      Database last_update
        // ******************************** Doesn't read from url **************************************
        // Excuite 3. liveMatchEventInfinity run for -> every 5 minutes *** ok doesn't include logo teams
        //      Write liveMatchOnly.json
        //      Database last_update
        // *********************************mid.xml (Delete after 3 days)**************************************
        // Excuite 4. liveMatchEventInfinity2 run for -> every 5 minutes *** ok doesn't test because of no live game avaliable
        //      Read mid.xml
        //      Write mid.json
        //      Database event       
        // Excuite 5. liveMatchLastEvent run for -> every 5 minutes *** ok
        //      Read mid.xml
        //      Write mid.json
        //      Database event   
        // Excuite 6. liveMatchEvent run for -> every 30 minutes
        //      Read mid.xml
        //      Write mid.json
        //      Database event  
        // ***********************************************************************       
        // Excuite 7. sv2Yesterday.jar run for -> every 120 minutes
        //      Read MatchDayXml/day as same as 1,2      
        // Excuite 8. liveVs.jar run for -> every 1 minutes 0-8 hours +7 gmtช่นื
        //      Read fulbal24 teamcompare     
        // Excuite 9. SoccerwaywithProxy.jar getliagueinfo  run for -> every 11 minutes
        // Excuite 10. Node 7m_odds_writefile.js run for -> every 10 minutes
        // Excuite 11. Node 7m_top_player_score_write.js run for ->  13 minutes 6 hours just 1 time
        // Excuite 12. Node 7m_odds_yesterday.js run for -> every 120 minutes
        // Excuite 13. Node futbol24_write_league.js run for -> every 60 minutes *** waitting for test
        // Excuite 14. Node futbol24_write_team.js run for -> every 60 minutes *** waitting for test
        // Excuite 15. SoccerwayWithProxy.jar removeFixtured run for -> 20 minutes 6 hours just 1 time
        // Excuite 16. SoccerwayWithProxy.jar getCompetitions run for -> 38 minutes 6 hours just 1 time
        // Excuite 17. SoccerwayWithProxy.jar getLeagueStat run for -> 45 minutes 4 hours just 1 time
        // Excuite 18. getLeague.jar run for -> 30 minutes 4 hours just 1 time
    }
}
